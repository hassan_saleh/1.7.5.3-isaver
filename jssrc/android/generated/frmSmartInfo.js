function addWidgetsfrmSmartInfo() {
    frmSmartInfo.setDefaultUnit(kony.flex.DP);
    var flxMain = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxMain",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMain.setDefaultUnit(kony.flex.DP);
    var flxSmartHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxSmartHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "skin": "s",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true
    }, {});
    flxSmartHeader.setDefaultUnit(kony.flex.DP);
    var lblSmartTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblSmartTitle",
        "isVisible": true,
        "skin": "CopylblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.common.isavingsAccount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "15dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxSmartBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxSmartBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_ec3b8845276749f7b8c65adcff602030,
        "skin": "slFbox",
        "top": "0%",
        "width": "20%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true,
        "retainFlowHorizontalAlignment": true
    }, {});
    flxSmartBack.setDefaultUnit(kony.flex.DP);
    var CopylblBackIcon0if0488d921b146 = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Back to"
        },
        "centerY": "50%",
        "id": "CopylblBackIcon0if0488d921b146",
        "isVisible": true,
        "left": "0dp",
        "skin": "CopysknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopylblBack0b50c4d5ec5694d = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Login Screen"
        },
        "centerY": "50%",
        "id": "CopylblBack0b50c4d5ec5694d",
        "isVisible": true,
        "left": "0dp",
        "skin": "CopysknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxSmartBack.add(CopylblBackIcon0if0488d921b146, CopylblBack0b50c4d5ec5694d);
    flxSmartHeader.add(lblSmartTitle, flxSmartBack);
    var FlexScrollContainer0c7fbb045130942 = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "FlexScrollContainer0c7fbb045130942",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "-0.03%",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "40dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexScrollContainer0c7fbb045130942.setDefaultUnit(kony.flex.DP);
    var flxContent = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "7%",
        "id": "flxContent",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "CopyslFboxOuterRing0g7d8638973fb41",
        "top": "1%",
        "width": "80%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true,
        "retainFlowHorizontalAlignment": false
    }, {});
    flxContent.setDefaultUnit(kony.flex.DP);
    var Label0j7998c85969042 = new kony.ui.Label({
        "height": "100%",
        "id": "Label0j7998c85969042",
        "isVisible": true,
        "skin": "CopylatoRegular0d4fb860b81944f",
        "text": kony.i18n.getLocalizedString("ii18n.account.newSubCluster"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyLabel0f8052486d28c4e = new kony.ui.Label({
        "height": "100%",
        "id": "CopyLabel0f8052486d28c4e",
        "isVisible": true,
        "skin": "CopylatoRegular0i488d75649d241",
        "text": kony.i18n.getLocalizedString("ii18n.account.newSubInterestRate"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxContent.add(Label0j7998c85969042, CopyLabel0f8052486d28c4e);
    var dgRates = new kony.ui.DataGrid({
        "centerX": "50.00%",
        "columnHeadersConfig": [{
            "columnContentAlignment": constants.CONTENT_ALIGN_CENTER,
            "columnHeaderTemplate": null,
            "columnHeaderText": "Cluster شرائح",
            "columnHeaderType": "text",
            "columnID": "col1",
            "columnText": "Not Defined",
            "columnType": constants.DATAGRID_COLUMN_TYPE_TEXT,
            "columnWidthInPercentage": 50,
            "isColumnSortable": false
        }, {
            "columnContentAlignment": constants.CONTENT_ALIGN_CENTER,
            "columnHeaderTemplate": null,
            "columnHeaderText": "%IR سعرالفائدة",
            "columnHeaderType": "text",
            "columnID": "col2",
            "columnText": "Not Defined",
            "columnType": constants.DATAGRID_COLUMN_TYPE_TEXT,
            "columnWidthInPercentage": 50,
            "isColumnSortable": false
        }, ],
        "data": [{
            "col1": "200 - 999",
            "col2": "0.10%"
        }, {
            "col1": "1,000 - 2,999",
            "col2": "0.20%"
        }, {
            "col1": "3,000 - 4,999",
            "col2": "0.40%"
        }, {
            "col1": "5,000 - 6,999",
            "col2": "0.60%"
        }, {
            "col1": "7,000 - 8,999",
            "col2": "0.80%"
        }, {
            "col1": "9,000 - 11,999",
            "col2": "1.00%"
        }, {
            "col1": "12,000 - 14,999",
            "col2": "1.25%"
        }, {
            "col1": "15,000 - 19,999",
            "col2": "1.50%"
        }, {
            "col1": "20,000 - 29,999",
            "col2": "1.75%"
        }, {
            "col1": "30,000 - 39,999",
            "col2": "2.00%"
        }, {
            "col1": "40,000 - 49,999",
            "col2": "0.50%"
        }, {
            "col1": "50,000 +",
            "col2": "0.50%"
        }],
        "headerSkin": "gridHeaderSkin",
        "height": "70%",
        "id": "dgRates",
        "isMultiSelect": false,
        "isVisible": false,
        "left": "0dp",
        "rowCount": 12,
        "rowNormalSkin": "slDataGridAltRowBold",
        "showColumnHeaders": false,
        "top": "0dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "gridlineColor": "ffffff00"
    });
    var Label0f9f8e101db6c44 = new kony.ui.Label({
        "id": "Label0f9f8e101db6c44",
        "isVisible": false,
        "left": "2%",
        "skin": "latoRegular24px",
        "text": kony.i18n.getLocalizedString("i18n.account.subCurDesc"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "1%",
        "width": "96%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [1, 0, 1, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var FlexContainer0e6dd425320e142 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "70%",
        "id": "FlexContainer0e6dd425320e142",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "CopyslFboxOuterRing0g7d8638973fb41",
        "top": "0%",
        "width": "80%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true,
        "retainContentAlignment": false,
        "retainFlowHorizontalAlignment": false
    }, {});
    FlexContainer0e6dd425320e142.setDefaultUnit(kony.flex.DP);
    var lblSaver1 = new kony.ui.Label({
        "height": "6%",
        "id": "lblSaver1",
        "isVisible": true,
        "left": "0%",
        "right": "0dp",
        "skin": "CopylatoRegular0d4fb860b81944f",
        "text": "200 - 999",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true,
        "retainContentAlignment": false
    }, {
        "textCopyable": false
    });
    var lblSaver2 = new kony.ui.Label({
        "height": "6%",
        "id": "lblSaver2",
        "isVisible": true,
        "left": "50%",
        "right": "50%",
        "skin": "CopylatoRegular0d4fb860b81944f",
        "text": "0.10%",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true,
        "retainContentAlignment": false
    }, {
        "textCopyable": false
    });
    var lblSaver3 = new kony.ui.Label({
        "height": "6%",
        "id": "lblSaver3",
        "isVisible": true,
        "left": "0%",
        "right": "0dp",
        "skin": "CopylatoRegular0d4fb860b81944f",
        "text": "1,000 - 2,999",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "6%",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true,
        "retainContentAlignment": false
    }, {
        "textCopyable": false
    });
    var lblSaver4 = new kony.ui.Label({
        "height": "6%",
        "id": "lblSaver4",
        "isVisible": true,
        "left": "50%",
        "right": "50%",
        "skin": "CopylatoRegular0d4fb860b81944f",
        "text": "0.20%",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "6%",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true,
        "retainContentAlignment": false
    }, {
        "textCopyable": false
    });
    var lblSaver5 = new kony.ui.Label({
        "height": "6%",
        "id": "lblSaver5",
        "isVisible": true,
        "left": "0%",
        "right": "0%",
        "skin": "CopylatoRegular0d4fb860b81944f",
        "text": "3,000 - 4,999",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "12%",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true,
        "retainContentAlignment": false
    }, {
        "textCopyable": false
    });
    var lblSaver6 = new kony.ui.Label({
        "height": "6%",
        "id": "lblSaver6",
        "isVisible": true,
        "left": "50%",
        "right": "50%",
        "skin": "CopylatoRegular0d4fb860b81944f",
        "text": "0.40%",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "12%",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true,
        "retainContentAlignment": false
    }, {
        "textCopyable": false
    });
    var lblSaver7 = new kony.ui.Label({
        "height": "6%",
        "id": "lblSaver7",
        "isVisible": true,
        "left": "0%",
        "right": "0dp",
        "skin": "CopylatoRegular0d4fb860b81944f",
        "text": "5,000 - 6,999",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "18%",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true,
        "retainContentAlignment": false
    }, {
        "textCopyable": false
    });
    var lblSaver8 = new kony.ui.Label({
        "height": "6%",
        "id": "lblSaver8",
        "isVisible": true,
        "left": "50%",
        "right": "50%",
        "skin": "CopylatoRegular0d4fb860b81944f",
        "text": "0.60%",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "18%",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true,
        "retainContentAlignment": false
    }, {
        "textCopyable": false
    });
    var lblSaver9 = new kony.ui.Label({
        "height": "6%",
        "id": "lblSaver9",
        "isVisible": true,
        "left": "0%",
        "right": "0dp",
        "skin": "CopylatoRegular0d4fb860b81944f",
        "text": "7,000 - 8,999",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "24%",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true,
        "retainContentAlignment": false
    }, {
        "textCopyable": false
    });
    var lblSaver10 = new kony.ui.Label({
        "height": "6%",
        "id": "lblSaver10",
        "isVisible": true,
        "left": "50%",
        "right": "50%",
        "skin": "CopylatoRegular0d4fb860b81944f",
        "text": "0.80%",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "24%",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true,
        "retainContentAlignment": false
    }, {
        "textCopyable": false
    });
    var lblSaver11 = new kony.ui.Label({
        "height": "6%",
        "id": "lblSaver11",
        "isVisible": true,
        "left": "0%",
        "right": "0dp",
        "skin": "CopylatoRegular0d4fb860b81944f",
        "text": "9,000 - 11,999",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "30%",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true,
        "retainContentAlignment": false
    }, {
        "textCopyable": false
    });
    var lblSaver12 = new kony.ui.Label({
        "height": "6%",
        "id": "lblSaver12",
        "isVisible": true,
        "left": "50%",
        "right": "50%",
        "skin": "CopylatoRegular0d4fb860b81944f",
        "text": "1.00%",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "30%",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true,
        "retainContentAlignment": false
    }, {
        "textCopyable": false
    });
    var lblSaver13 = new kony.ui.Label({
        "height": "6%",
        "id": "lblSaver13",
        "isVisible": true,
        "left": "0%",
        "right": "0dp",
        "skin": "CopylatoRegular0d4fb860b81944f",
        "text": "12,000 - 14,999",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "36%",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true,
        "retainContentAlignment": false
    }, {
        "textCopyable": false
    });
    var lblSaver14 = new kony.ui.Label({
        "height": "6%",
        "id": "lblSaver14",
        "isVisible": true,
        "left": "50%",
        "right": "50%",
        "skin": "CopylatoRegular0d4fb860b81944f",
        "text": "1.25%",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "36%",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true,
        "retainContentAlignment": false
    }, {
        "textCopyable": false
    });
    var lblSaver15 = new kony.ui.Label({
        "height": "6%",
        "id": "lblSaver15",
        "isVisible": true,
        "left": "0%",
        "right": "0dp",
        "skin": "CopylatoRegular0d4fb860b81944f",
        "text": "15,000 - 19,999",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "42%",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true,
        "retainContentAlignment": false
    }, {
        "textCopyable": false
    });
    var lblSaver16 = new kony.ui.Label({
        "height": "6%",
        "id": "lblSaver16",
        "isVisible": true,
        "left": "50%",
        "right": "50%",
        "skin": "CopylatoRegular0d4fb860b81944f",
        "text": "1.50%",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "42%",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true,
        "retainContentAlignment": false
    }, {
        "textCopyable": false
    });
    var lblSaver17 = new kony.ui.Label({
        "height": "6%",
        "id": "lblSaver17",
        "isVisible": true,
        "left": "50%",
        "right": "50%",
        "skin": "CopylatoRegular0d4fb860b81944f",
        "text": "1.75%",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "48%",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true,
        "retainContentAlignment": false
    }, {
        "textCopyable": false
    });
    var lblSaver18 = new kony.ui.Label({
        "height": "6%",
        "id": "lblSaver18",
        "isVisible": true,
        "left": "0%",
        "right": "0%",
        "skin": "CopylatoRegular0d4fb860b81944f",
        "text": "20,000 - 29,999",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "48%",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true,
        "retainContentAlignment": false
    }, {
        "textCopyable": false
    });
    var lblSaver19 = new kony.ui.Label({
        "height": "6%",
        "id": "lblSaver19",
        "isVisible": true,
        "left": "50%",
        "right": "50%",
        "skin": "CopylatoRegular0d4fb860b81944f",
        "text": "2.00%",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "54%",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true,
        "retainContentAlignment": false
    }, {
        "textCopyable": false
    });
    var lblSaver20 = new kony.ui.Label({
        "height": "6%",
        "id": "lblSaver20",
        "isVisible": true,
        "left": "0%",
        "right": "0%",
        "skin": "CopylatoRegular0d4fb860b81944f",
        "text": "30,000 - 39,999",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "54%",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true,
        "retainContentAlignment": false
    }, {
        "textCopyable": false
    });
    var lblSaver21 = new kony.ui.Label({
        "height": "6%",
        "id": "lblSaver21",
        "isVisible": true,
        "left": "50%",
        "right": "50%",
        "skin": "CopylatoRegular0d4fb860b81944f",
        "text": "0.50%",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "60%",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true,
        "retainContentAlignment": false
    }, {
        "textCopyable": false
    });
    var lblSaver22 = new kony.ui.Label({
        "height": "6%",
        "id": "lblSaver22",
        "isVisible": true,
        "left": "0%",
        "right": "0%",
        "skin": "CopylatoRegular0d4fb860b81944f",
        "text": "40,000 - 49,999",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "60%",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true,
        "retainContentAlignment": false
    }, {
        "textCopyable": false
    });
    var lblSaver23 = new kony.ui.Label({
        "height": "6%",
        "id": "lblSaver23",
        "isVisible": true,
        "left": "50%",
        "right": "50%",
        "skin": "CopylatoRegular0d4fb860b81944f",
        "text": "0.50%",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "66%",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true,
        "retainContentAlignment": false
    }, {
        "textCopyable": false
    });
    var lblSaver24 = new kony.ui.Label({
        "height": "6%",
        "id": "lblSaver24",
        "isVisible": true,
        "left": "0%",
        "right": "0%",
        "skin": "CopylatoRegular0d4fb860b81944f",
        "text": "50,000 +",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "66%",
        "width": "50%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true,
        "retainContentAlignment": false
    }, {
        "textCopyable": false
    });
    var lblSmartDesc = new kony.ui.Label({
        "id": "lblSmartDesc",
        "isVisible": true,
        "left": "0%",
        "skin": "smartSavingDescskn",
        "text": kony.i18n.getLocalizedString("i18n.smartSaver.desc"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "75%",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlexContainer0e6dd425320e142.add(lblSaver1, lblSaver2, lblSaver3, lblSaver4, lblSaver5, lblSaver6, lblSaver7, lblSaver8, lblSaver9, lblSaver10, lblSaver11, lblSaver12, lblSaver13, lblSaver14, lblSaver15, lblSaver16, lblSaver17, lblSaver18, lblSaver19, lblSaver20, lblSaver21, lblSaver22, lblSaver23, lblSaver24, lblSmartDesc);
    FlexScrollContainer0c7fbb045130942.add(flxContent, dgRates, Label0f9f8e101db6c44, FlexContainer0e6dd425320e142);
    flxMain.add(flxSmartHeader, FlexScrollContainer0c7fbb045130942);
    var Button0a37d42ec208c44 = new kony.ui.Button({
        "focusSkin": "slButtonGlossRed",
        "height": "50dp",
        "id": "Button0a37d42ec208c44",
        "isVisible": true,
        "left": "31dp",
        "onClick": AS_Button_d969a035374740f4b943522d3911df1d,
        "skin": "slButtonGlossBlue",
        "text": "Button",
        "top": "614dp",
        "width": "300dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    frmSmartInfo.add(flxMain, Button0a37d42ec208c44);
};

function frmSmartInfoGlobals() {
    frmSmartInfo = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmSmartInfo,
        "bounces": false,
        "enableScrolling": false,
        "enabledForIdleTimeout": true,
        "id": "frmSmartInfo",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_a316b8a48b0e4227993d47f419034596,
        "retainScrollPosition": false,
        "titleBar": false,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};