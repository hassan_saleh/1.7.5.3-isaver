function addWidgetsfrmBrowser() {
    frmBrowser.setDefaultUnit(kony.flex.DP);
    var flxHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0%",
        "isModalContainer": false,
        "skin": "s",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHeader.setDefaultUnit(kony.flex.DP);
    var flxBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_d53d39af6b8842a5be4c1bdcbfea9b55,
        "skin": "slFbox",
        "top": "0%",
        "width": "20%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true
    }, {});
    flxBack.setDefaultUnit(kony.flex.DP);
    var lblBackIcon = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Back to"
        },
        "centerY": "50%",
        "height": "100%",
        "id": "lblBackIcon",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBack = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Login Screen"
        },
        "centerY": "50%",
        "height": "100%",
        "id": "lblBack",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBack.add(lblBackIcon, lblBack);
    var lblBrowserTitle = new kony.ui.Label({
        "centerX": "50%",
        "height": "90%",
        "id": "lblBrowserTitle",
        "isVisible": true,
        "skin": "lblAmountCurrency",
        "text": kony.i18n.getLocalizedString("ii18.available.limit"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxHeader.add(flxBack, lblBrowserTitle);
    var flxTransferAmountHelp = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "90%",
        "horizontalScrollIndicator": true,
        "id": "flxTransferAmountHelp",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "10%",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTransferAmountHelp.setDefaultUnit(kony.flex.DP);
    var flxOwnAccountTransferHint = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxOwnAccountTransferHint",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "5%",
        "isModalContainer": false,
        "skin": "flxBgBlueGradientRound8",
        "top": "0%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxOwnAccountTransferHint.setDefaultUnit(kony.flex.DP);
    var lblOwnTransferHintTitle = new kony.ui.Label({
        "id": "lblOwnTransferHintTitle",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblWhike125",
        "text": kony.i18n.getLocalizedString("i18n.transferHelp.ownCurrencyTransferTitle"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": 0,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblOwnTransferHint = new kony.ui.Label({
        "id": "lblOwnTransferHint",
        "isVisible": true,
        "left": "5.00%",
        "skin": "sknLblWhite100",
        "text": "Label\nsadlfjala;ldfja;l\nsldfgjkld",
        "textStyle": {
            "lineSpacing": 0,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": 0,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyFlexContainer0dcf5f06bdd8947 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10dp",
        "id": "CopyFlexContainer0dcf5f06bdd8947",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0dcf5f06bdd8947.setDefaultUnit(kony.flex.DP);
    CopyFlexContainer0dcf5f06bdd8947.add();
    flxOwnAccountTransferHint.add(lblOwnTransferHintTitle, lblOwnTransferHint, CopyFlexContainer0dcf5f06bdd8947);
    var flxOwnAccountTransferDiffCurrHint = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxOwnAccountTransferDiffCurrHint",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "5%",
        "isModalContainer": false,
        "skin": "flxBgBlueGradientRound8",
        "top": "1%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxOwnAccountTransferDiffCurrHint.setDefaultUnit(kony.flex.DP);
    var lblOwnAccountTransferDiffCurrHintTitle = new kony.ui.Label({
        "id": "lblOwnAccountTransferDiffCurrHintTitle",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblWhike125",
        "text": kony.i18n.getLocalizedString("i18n.transferHelp.ownDiffCurrencyTransferTitle"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": 0,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblOwnAccountTransferDiffCurrHint = new kony.ui.Label({
        "id": "lblOwnAccountTransferDiffCurrHint",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblWhite100",
        "text": "Label\nsadlfjala;ldfja;lLabel\n",
        "textStyle": {
            "lineSpacing": 0,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": 0,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyFlexContainer0g15ed8ba819749 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": 10,
        "id": "CopyFlexContainer0g15ed8ba819749",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0g15ed8ba819749.setDefaultUnit(kony.flex.DP);
    CopyFlexContainer0g15ed8ba819749.add();
    flxOwnAccountTransferDiffCurrHint.add(lblOwnAccountTransferDiffCurrHintTitle, lblOwnAccountTransferDiffCurrHint, CopyFlexContainer0g15ed8ba819749);
    var flxBOJAccountTransferHint = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxBOJAccountTransferHint",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "5%",
        "isModalContainer": false,
        "skin": "flxBgBlueGradientRound8",
        "top": "1%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxBOJAccountTransferHint.setDefaultUnit(kony.flex.DP);
    var lblBOJAccountTransferHintTitle = new kony.ui.Label({
        "id": "lblBOJAccountTransferHintTitle",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblWhike125",
        "text": kony.i18n.getLocalizedString("i18n.transferHelp.bojTransferTitle"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": 0,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblBOJAccountTransferHint = new kony.ui.Label({
        "id": "lblBOJAccountTransferHint",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblWhite100",
        "text": "Label\nsadlfjala;ldfja;l",
        "textStyle": {
            "lineSpacing": 0,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": 0,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyFlexContainer0be3bc952cb0044 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": 10,
        "id": "CopyFlexContainer0be3bc952cb0044",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0be3bc952cb0044.setDefaultUnit(kony.flex.DP);
    CopyFlexContainer0be3bc952cb0044.add();
    flxBOJAccountTransferHint.add(lblBOJAccountTransferHintTitle, lblBOJAccountTransferHint, CopyFlexContainer0be3bc952cb0044);
    var flxNationalAccountTransferHint = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxNationalAccountTransferHint",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "5%",
        "isModalContainer": false,
        "skin": "flxBgBlueGradientRound8",
        "top": "1%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxNationalAccountTransferHint.setDefaultUnit(kony.flex.DP);
    var lblNationalAccountTransferHintTitle = new kony.ui.Label({
        "id": "lblNationalAccountTransferHintTitle",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblWhike125",
        "text": kony.i18n.getLocalizedString("i18n.transferHelp.nationalTransferTitle"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": 0,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblNationalAccountTransferHint = new kony.ui.Label({
        "id": "lblNationalAccountTransferHint",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblWhite100",
        "text": "Label\nsadlfjala;ldfja;l",
        "textStyle": {
            "lineSpacing": 0,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": 0,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyFlexContainer0dcd28deb32724f = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": 10,
        "id": "CopyFlexContainer0dcd28deb32724f",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0dcd28deb32724f.setDefaultUnit(kony.flex.DP);
    CopyFlexContainer0dcd28deb32724f.add();
    flxNationalAccountTransferHint.add(lblNationalAccountTransferHintTitle, lblNationalAccountTransferHint, CopyFlexContainer0dcd28deb32724f);
    var flxInternationalAccountTransferHint = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxInternationalAccountTransferHint",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "5%",
        "isModalContainer": false,
        "skin": "flxBgBlueGradientRound8",
        "top": "1%",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxInternationalAccountTransferHint.setDefaultUnit(kony.flex.DP);
    var lblInternationalAccountTransferHintTitle = new kony.ui.Label({
        "id": "lblInternationalAccountTransferHintTitle",
        "isVisible": true,
        "left": "2%",
        "skin": "sknLblWhike125",
        "text": kony.i18n.getLocalizedString("i18n.transferHelp.internationalTransferTitle"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": 0,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblInternationalAccountTransferHint = new kony.ui.Label({
        "id": "lblInternationalAccountTransferHint",
        "isVisible": true,
        "left": "5%",
        "skin": "sknLblWhite100",
        "text": "Label\nsadlfjala;ldfja;l",
        "textStyle": {
            "lineSpacing": 0,
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": 0,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyFlexContainer0b14691c525714c = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": 10,
        "id": "CopyFlexContainer0b14691c525714c",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "right": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0b14691c525714c.setDefaultUnit(kony.flex.DP);
    CopyFlexContainer0b14691c525714c.add();
    flxInternationalAccountTransferHint.add(lblInternationalAccountTransferHintTitle, lblInternationalAccountTransferHint, CopyFlexContainer0b14691c525714c);
    flxTransferAmountHelp.add(flxOwnAccountTransferHint, flxOwnAccountTransferDiffCurrHint, flxBOJAccountTransferHint, flxNationalAccountTransferHint, flxInternationalAccountTransferHint);
    frmBrowser.add(flxHeader, flxTransferAmountHelp);
};

function frmBrowserGlobals() {
    frmBrowser = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmBrowser,
        "enabledForIdleTimeout": false,
        "id": "frmBrowser",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "sknmainGradient"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};