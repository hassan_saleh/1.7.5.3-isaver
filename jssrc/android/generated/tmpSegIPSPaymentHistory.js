function initializetmpSegIPSPaymentHistory() {
    flxPaymentHistoryIncoming = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "focusSkin": "CopysknflxTransprnt",
        "id": "flxPaymentHistoryIncoming",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox"
    }, {}, {});
    flxPaymentHistoryIncoming.setDefaultUnit(kony.flex.DP);
    var lblNoData = new kony.ui.Label({
        "centerY": 50,
        "id": "lblNoData",
        "isVisible": false,
        "left": "15dp",
        "skin": "CopylblSegName",
        "text": kony.i18n.getLocalizedString("i18n.accounts.noTransaction"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxIncoming = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxIncoming",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": 0,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {}, {});
    flxIncoming.setDefaultUnit(kony.flex.DP);
    var lblTrxRefi = new kony.ui.Label({
        "centerY": "95dp",
        "id": "lblTrxRefi",
        "isVisible": true,
        "left": "35%",
        "skin": "CopysknLblNextDisabled0i0515df3e6be48",
        "text": "Ref",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": false,
        "retainContentAlignment": false
    }, {
        "textCopyable": false
    });
    var lblCreditAccountValue = new kony.ui.Label({
        "centerY": "120dp",
        "id": "lblCreditAccountValue",
        "isVisible": true,
        "left": "35%",
        "skin": "CopysknLblNextDisabled0i0515df3e6be48",
        "text": "Ref",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": false,
        "retainContentAlignment": false
    }, {
        "textCopyable": false
    });
    var lblRefi = new kony.ui.Label({
        "centerY": "95dp",
        "id": "lblRefi",
        "isVisible": true,
        "left": "3.50%",
        "skin": "CopysknLblAccNumBiller0h365d6684ee94b",
        "text": kony.i18n.getLocalizedString("i18n.common.ReferenceNumber"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [1, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblCredited = new kony.ui.Label({
        "centerY": "120dp",
        "id": "lblCredited",
        "isVisible": true,
        "left": "3.50%",
        "skin": "CopysknLblAccNumBiller0h365d6684ee94b",
        "text": kony.i18n.getLocalizedString("ii18n.CLIQ.creditedAccount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [1, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var AliasNamei = new kony.ui.Label({
        "centerY": "45dp",
        "id": "AliasNamei",
        "isVisible": true,
        "left": "35%",
        "skin": "CopysknLblNextDisabled0i0515df3e6be48",
        "text": "ALias",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAliasi = new kony.ui.Label({
        "centerY": "45dp",
        "id": "lblAliasi",
        "isVisible": true,
        "left": "3.50%",
        "skin": "CopysknLblAccNumBiller0h365d6684ee94b",
        "text": kony.i18n.getLocalizedString("ii18.CLIQ.senderName"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [1, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAmti = new kony.ui.Label({
        "centerY": "20dp",
        "id": "lblAmti",
        "isVisible": false,
        "left": "50%",
        "skin": "CopysknLblAccNumBiller",
        "text": kony.i18n.getLocalizedString("i18n.accounts.amount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var transactionDatei = new kony.ui.Label({
        "centerY": "20dp",
        "id": "transactionDatei",
        "isVisible": true,
        "left": "3.50%",
        "skin": "CopysknLblAccNumBiller0h365d6684ee94b",
        "text": "10/10/2098",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [1, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTrxDatei = new kony.ui.Label({
        "centerY": "20dp",
        "id": "lblTrxDatei",
        "isVisible": false,
        "left": "3.50%",
        "skin": "CopysknLblAccNumBiller",
        "text": kony.i18n.getLocalizedString("i18n.CLIQ.TransferDate"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [1, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var transactionAmounti = new kony.ui.Label({
        "centerY": "20dp",
        "id": "transactionAmounti",
        "isVisible": true,
        "left": "35%",
        "skin": "CopysknLblAccNumBiller0h365d6684ee94b",
        "text": "$10.00",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblIBAnDesci = new kony.ui.Label({
        "centerY": "70dp",
        "id": "lblIBAnDesci",
        "isVisible": true,
        "left": "3.50%",
        "skin": "CopysknLblAccNumBiller0h365d6684ee94b",
        "text": kony.i18n.getLocalizedString("ii18n.CLIQ.senderIBAN"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [1, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblIBANi = new kony.ui.Label({
        "centerY": "70dp",
        "id": "lblIBANi",
        "isVisible": true,
        "left": "35%",
        "skin": "CopysknLblNextDisabled0i0515df3e6be48",
        "text": "JO94CBJO0010000000000131000302",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnReturnPayment = new kony.ui.Button({
        "centerY": 115,
        "focusSkin": "BOJFont3skn",
        "height": "25dp",
        "id": "btnReturnPayment",
        "isVisible": false,
        "left": "90%",
        "onClick": AS_Button_f3ba10f76d8f42fbaee7a14f3b954742,
        "skin": "BOJFont3skn",
        "text": "U",
        "top": "0",
        "width": "25dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxIncoming.add(lblTrxRefi, lblCreditAccountValue, lblRefi, lblCredited, AliasNamei, lblAliasi, lblAmti, transactionDatei, lblTrxDatei, transactionAmounti, lblIBAnDesci, lblIBANi, btnReturnPayment);
    var flxSpacer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10dp",
        "id": "flxSpacer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "CopyslFbox0dd727ad8509e46",
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {}, {});
    flxSpacer.setDefaultUnit(kony.flex.DP);
    flxSpacer.add();
    var flxOutgoing = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxOutgoing",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {}, {});
    flxOutgoing.setDefaultUnit(kony.flex.DP);
    var lblTrxRefo = new kony.ui.Label({
        "centerY": "95dp",
        "id": "lblTrxRefo",
        "isVisible": true,
        "left": "35%",
        "skin": "CopysknLblNextDisabled0i0515df3e6be48",
        "text": "Ref",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": false,
        "retainContentAlignment": false
    }, {
        "textCopyable": false
    });
    var lblRefo = new kony.ui.Label({
        "centerY": "95dp",
        "id": "lblRefo",
        "isVisible": true,
        "left": "3.50%",
        "skin": "CopysknLblAccNumBiller0h365d6684ee94b",
        "text": kony.i18n.getLocalizedString("i18n.common.ReferenceNumber"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [1, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var AliasNameo = new kony.ui.Label({
        "centerY": "35dp",
        "id": "AliasNameo",
        "isVisible": false,
        "left": "37.50%",
        "skin": "CopysknLblNextDisabled",
        "text": "ALias",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAliaso = new kony.ui.Label({
        "centerY": "35dp",
        "id": "lblAliaso",
        "isVisible": false,
        "left": "3.50%",
        "skin": "CopysknLblAccNumBiller",
        "text": kony.i18n.getLocalizedString("i18n.CliQ.ALias"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAmto = new kony.ui.Label({
        "centerY": "20dp",
        "id": "lblAmto",
        "isVisible": false,
        "left": "50%",
        "skin": "CopysknLblAccNumBiller",
        "text": kony.i18n.getLocalizedString("i18n.accounts.amount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var transactionDateo = new kony.ui.Label({
        "centerY": "20dp",
        "id": "transactionDateo",
        "isVisible": true,
        "left": "3.50%",
        "skin": "CopysknLblAccNumBiller0h365d6684ee94b",
        "text": "10/10/2098",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [1, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTrxDateo = new kony.ui.Label({
        "centerY": "20dp",
        "id": "lblTrxDateo",
        "isVisible": false,
        "left": "3.50%",
        "skin": "CopysknLblAccNumBiller",
        "text": kony.i18n.getLocalizedString("i18n.CLIQ.TransferDate"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [1, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var transactionAmounto = new kony.ui.Label({
        "centerY": "20dp",
        "id": "transactionAmounto",
        "isVisible": true,
        "left": "35%",
        "skin": "CopysknLblAccNumBiller0h365d6684ee94b",
        "text": "$10.00",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblIBAnDescO = new kony.ui.Label({
        "centerY": "70dp",
        "id": "lblIBAnDescO",
        "isVisible": true,
        "left": "3.50%",
        "skin": "CopysknLblAccNumBiller0h365d6684ee94b",
        "text": kony.i18n.getLocalizedString("i18n.CliQ.ToBene"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [1, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblIBANo = new kony.ui.Label({
        "centerY": "70dp",
        "id": "lblIBANo",
        "isVisible": true,
        "left": "35%",
        "skin": "CopysknLblNextDisabled0i0515df3e6be48",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblFAcco = new kony.ui.Label({
        "centerY": "45dp",
        "id": "lblFAcco",
        "isVisible": true,
        "left": "3.50%",
        "skin": "CopysknLblAccNumBiller0h365d6684ee94b",
        "text": kony.i18n.getLocalizedString("i18n.CLIQ.FromAccount"),
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [1, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblFromAccount = new kony.ui.Label({
        "centerY": "45dp",
        "id": "lblFromAccount",
        "isVisible": true,
        "left": "35%",
        "skin": "CopysknLblNextDisabled0i0515df3e6be48",
        "text": "from",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var CopyflxSpacer0e5be78dde5554d = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": 110,
        "clipBounds": true,
        "height": "10dp",
        "id": "CopyflxSpacer0e5be78dde5554d",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "CopyslFbox0dd727ad8509e46",
        "top": "5dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {}, {});
    CopyflxSpacer0e5be78dde5554d.setDefaultUnit(kony.flex.DP);
    CopyflxSpacer0e5be78dde5554d.add();
    var btnCreditConfirm = new kony.ui.Button({
        "centerY": 95,
        "focusSkin": "sknRDOBlueRNDBRDBLDBOJFont60",
        "height": "25dp",
        "id": "btnCreditConfirm",
        "isVisible": true,
        "left": "90%",
        "onClick": AS_Button_cb607dea768843c98808863dcd85562e,
        "skin": "sknRDOBlueRNDBRDBLDBOJFont60",
        "text": "E",
        "width": "25dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxOutgoing.add(lblTrxRefo, lblRefo, AliasNameo, lblAliaso, lblAmto, transactionDateo, lblTrxDateo, transactionAmounto, lblIBAnDescO, lblIBANo, lblFAcco, lblFromAccount, CopyflxSpacer0e5be78dde5554d, btnCreditConfirm);
    var lblTransType = new kony.ui.Label({
        "centerY": "50dp",
        "id": "lblTransType",
        "isVisible": false,
        "left": "3.50%",
        "minWidth": 0,
        "skin": "CopysknLblAccNumBiller",
        "text": kony.i18n.getLocalizedString("i18n.FilterTransaction.transactiontype"),
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxPaymentHistoryIncoming.add(lblNoData, flxIncoming, flxSpacer, flxOutgoing, lblTransType);
}