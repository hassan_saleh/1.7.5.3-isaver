function initializetmpAllBeneHeader() {
    flxTmpHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "flxTmpHeader",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "isModalContainer": false,
        "skin": "slFboxTemplateHeader"
    }, {}, {});
    flxTmpHeader.setDefaultUnit(kony.flex.DP);
    var lbltmpTitle = new kony.ui.Label({
        "height": "99%",
        "id": "lbltmpTitle",
        "isVisible": true,
        "left": "5%",
        "skin": "slLabelTitle",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 2, 0, 2],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxUnderline = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1%",
        "id": "flxUnderline",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknsegmentDivider",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxUnderline.setDefaultUnit(kony.flex.DP);
    flxUnderline.add();
    flxTmpHeader.add(lbltmpTitle, flxUnderline);
}