function initializeFBox0a8248f803e1745() {
    FBox0a8248f803e1745 = new kony.ui.FlexContainer({
        "clipBounds": false,
        "height": "40dp",
        "id": "FBox0a8248f803e1745",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "width": "100%"
    }, {
        "containerWeight": 100
    }, {});
    FBox0a8248f803e1745.setDefaultUnit(kony.flex.DP);
    var lblData = new kony.ui.Label({
        "id": "lblData",
        "isVisible": true,
        "left": "0dp",
        "skin": "CopysknstandardTextBold0db86a48ecb5f44",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "hExpand": true,
        "margin": [1, 1, 1, 1],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false
    });
    FBox0a8248f803e1745.add(lblData);
}