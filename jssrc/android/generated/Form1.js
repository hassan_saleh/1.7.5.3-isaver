function addWidgetsForm1() {
    Form1.setDefaultUnit(kony.flex.DP);
    var piechart = new com.konymp.piechart({
        "clipBounds": true,
        "height": "100%",
        "id": "piechart",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "skin": "sknComponentFlex",
        "top": "0dp",
        "width": "100%",
        "overrides": {
            "piechart": {
                "right": "viz.val_cleared",
                "bottom": "viz.val_cleared",
                "minWidth": "viz.val_cleared",
                "minHeight": "viz.val_cleared",
                "maxWidth": "viz.val_cleared",
                "maxHeight": "viz.val_cleared",
                "centerX": "viz.val_cleared",
                "centerY": "viz.val_cleared"
            }
        }
    }, {
        "overrides": {}
    }, {
        "overrides": {}
    });
    piechart.chartTitle = "Pie Chart";
    piechart.enableLegend = true;
    piechart.bgColor = "#ffffff";
    piechart.chartData = {
        "data": [{
            "colorCode": "#1B9ED9",
            "label": "Data1",
            "value": "40"
        }, {
            "colorCode": "#E8672B",
            "label": "Data2",
            "value": "20"
        }, {
            "colorCode": "#76C044",
            "label": "Data3",
            "value": "20"
        }, {
            "colorCode": "#FFC522",
            "label": "Data4",
            "value": "10"
        }, {
            "colorCode": "#97CDED",
            "label": "Data5",
            "value": "10"
        }],
        "schema": [{
            "columnHeaderTemplate": null,
            "columnHeaderText": "Label",
            "columnHeaderType": "text",
            "columnID": "label",
            "columnOnClick": null,
            "columnText": "Not Defined",
            "columnType": "text",
            "kuid": "c8434b6cbed443e6a6167d99e8c3bdcb"
        }, {
            "columnHeaderTemplate": null,
            "columnHeaderText": "Value",
            "columnHeaderType": "text",
            "columnID": "value",
            "columnOnClick": null,
            "columnText": "Not Defined",
            "columnType": "text",
            "kuid": "cddb65c1fe8c429bb5a4b5f93bd171ef"
        }, {
            "columnHeaderTemplate": null,
            "columnHeaderText": "Color Code",
            "columnHeaderType": "text",
            "columnID": "colorCode",
            "columnOnClick": null,
            "columnText": "Not Defined",
            "columnType": "text",
            "kuid": "f722303b71a8439eae528d9af9856f30"
        }]
    };
    piechart.titleFontSize = "12";
    piechart.legendFontSize = "8";
    piechart.enableStaticPreview = true;
    piechart.titleFontColor = "#000000";
    piechart.legendFontColor = "#000000";
    Form1.info = {
        "kuid": "b1cd3ce2d80340cabc5ed8448379be89"
    };
    Form1.add(piechart);
};

function Form1Globals() {
    Form1 = new kony.ui.Form2({
        "addWidgets": addWidgetsForm1,
        "enabledForIdleTimeout": false,
        "id": "Form1",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "slForm"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "retainScrollPosition": false,
        "titleBar": true,
        "titleBarSkin": "slTitleBar",
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
};