var logObj = ["", //frmAccount,0
    "", //frmAccountBr,1
    "", //frmAccountCurr,2
    "", //toAccount,3
    "", //toAccountBr,4
    "", //billtype,5
    "", //billerno,6
    "", //billername,7
    "", //servicetype,8
    "", //jomopaytype,9
    "", //jomopaytypeval,10
    "", //transfercurr,11
    "", //transferamt,12
    "", //transferType,13
    "", //recurenceType,14
    "", //recurenceTxnDate,15
    "", // reference number16
    "", //status,17
    "", // statuscomments,18
    "", //description19
];

function loggerCall() {
    try {
        if (kony.sdk.isNetworkAvailable()) {
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
            var scopeObj = this;
            var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
            var options = {
                "access": "online",
                "objectName": "RBObjects"
            };
            var headers = {
                "session_token": kony.retailBanking.globalData.session_token
            };
            var serviceName = "RBObjects";
            var modelObj = INSTANCE.getModel("Ads", serviceName, options);
            var record = {};
            kony.print("logObj :: " + JSON.stringify(logObj));
            var ipAddress = getDeviceIP();
            record["custId"] = custid;
            record["userid"] = user_id;
            record["loggerchannel"] = channel;
            record["ipaddress"] = ipAddress;
            record["deviceid"] = deviceid;
            record["frmAccount"] = logObj[0];
            record["frmAccountBr"] = logObj[1];
            record["frmAccountCurr"] = (isEmpty(gblFromAccCurrency)) ? logObj[2] : gblFromAccCurrency;
            record["toAccount"] = logObj[3];
            record["toAccountBr"] = logObj[4];
            record["billtype"] = logObj[5];
            record["billerno"] = logObj[6];
            record["billername"] = logObj[7];
            record["servicetype"] = logObj[8];
            record["jomopaytype"] = logObj[9];
            record["jomopaytypeval"] = logObj[10];
            record["transfercurr"] = logObj[11];
            record["transferamt"] = logObj[12];
            record["transferType"] = logObj[13];
            record["recurenceType"] = logObj[14];
            record["recurenceTxnDate"] = logObj[15];
            record["refno"] = logObj[16];
            record["status"] = logObj[17];
            record["statuscomments"] = logObj[18];
            if (logObj[19].length > 200) record["description"] = logObj[19].substring(0, 200);
            else record["description"] = logObj[19];
            kony.print("excpetion log :: " + JSON.stringify(record));
            var dataObject = new kony.sdk.dto.DataObject("Ads", record);
            var requestOptions = {
                "dataObject": dataObject,
                "headers": headers
            };
            modelObj.create(requestOptions, loggerCallSuccessCallback, loggerCallErrorCallback);
        } else {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            customAlertPopup("", kony.i18n.getLocalizedString("i18n.errorMsg.deviceConnectionError"), popupCommonAlertDimiss, "");
        }
    } catch (err) {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        kony.print("Exception in Logger Service" + err);
        exceptionLogCall("BojLogger", "UI ERROR", "UI", err);
    }
}

function loggerCallSuccessCallback(resObj) {
    clearLog();
    kony.print("In success of loggerCallSuccessCallback ::  " + JSON.stringify(resObj));
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
}

function loggerCallErrorCallback(errObj) {
    clearLog();
    kony.print("In Error of loggerCallSuccessCallback ::  " + JSON.stringify(errObj));
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
}

function clearLog() {
    logObj = ["", //frmAccount,
        "", //frmAccountBr,
        "", //frmAccountCurr,
        "", //toAccount,
        "", //toAccountBr,
        "", //billtype,
        "", //billerno,
        "", //billername,
        "", //servicetype,
        "", //jomopaytype,
        "", //jomopaytypeval,
        "", //transfercurr,
        "", //transferamt,
        "", //transferType,
        "", //recurenceType,
        "", //recurenceTxnDate,
        "", // reference number
        "", //status,
        "", // statuscomments,
        "", //description
    ];
}