function accountsScreenPreshow() {
    var data = [{
        "AccountType": "Salary Account",
        "AccountNumber": "1111111234567890",
        "Amount": "25433"
    }, {
        "AccountType": "Credit card Account",
        "AccountNumber": "1111111890234567",
        "Amount": "25433"
    }, {
        "AccountType": "Savings Account",
        "AccountNumber": "1111111234905678",
        "Amount": "25433"
    }, {
        "AccountType": "Term deposit Account",
        "AccountNumber": "1111111289034467",
        "Amount": "25433"
    }, {
        "AccountType": "Current Account",
        "AccountNumber": "3458901267",
        "Amount": "25433"
    }];
    for (var i in data) {
        var x = data[i].AccountNumber;
        x = "***" + ((parseInt(x) % 100000).toString());
        data[i].hiddenAccountNumber = x;
        data[i].Amount = data[i].Amount + " JOD";
    }
    frmAccountDetailsScreen.segFre.widgetDataMap = {
        lblAccountName: "AccountType",
        lblAccountNumber: "hiddenAccountNumber",
        lblAmount: "Amount"
    };
    frmAccountDetailsScreen.segFre.setData(data);
}

function backFromAccDetails() {
    if (gblTModule === "BillPayNewBillKA") frmNewBillKA.show();
    else if (gblTModule === "BillPayBillers") frmBills.show();
    else if (gblTModule === "BillPay") frmNewBillDetails.show();
    else if (gblTModule == "jomo") frmJoMoPay.show();
    else if (gblTModule == "jomoReg") frmJoMoPayRegistration.show();
    else if (gblTModule == "web") {
        frmWebCharge.show();
    } else if (gblTModule == "cards") {
        frmCreditCardPayment.show();
    } else frmNewTransferKA.show();
}