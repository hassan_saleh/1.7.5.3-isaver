kony = kony || {};
kony.rb = kony.rb || {};
kony.rb.frmAccountDetailKAViewController = function() {};
kony.rb.frmAccountDetailKAViewController.prototype.onSelectCashWithdraw = function(acntId) {
    var cardlessPC = applicationManager.getCardlessWithdrawalPresentationController();
    cardlessPC.showCardlessCashLandingForm(acntId);
};