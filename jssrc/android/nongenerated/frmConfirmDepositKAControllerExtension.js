/*
 * Controller Extension class for frmConfirmDepositKA
 * Developer can edit the existing methods or can add new methods if required
 *
 */
kony = kony || {};
kony.sdk = kony.sdk || {};
kony.sdk.mvvm = kony.sdk.mvvm || {};
kony.sdk.mvvm.v2 = kony.sdk.mvvm.v2 || {};
/**
 * Creates a new Form Controller Extension.
 * @class frmConfirmDepositKAControllerExtension
 * @param {Object} controllerObj - Form Controller.
 */
kony.sdk.mvvm.frmConfirmDepositKAControllerExtension = Class(kony.sdk.mvvm.BankingAppControllerExtension, {
    constructor: function(controllerObj) {
        this.$class.$super.call(this, controllerObj);
    },
    /** 
     * This method is an entry point for all fetch related flows. Developer can edit.
     * Default implementation fetches data for the form based on form config 
     * @memberof frmConfirmDepositKAControllerExtension#
     */
    fetchData: function() {
        try {
            var scopeObj = this;
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen(" ");
            this.$class.$superp.fetchData.call(this, success, error);
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("Error in fetchData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_FETCH_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_FETCH_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

        function success(response) {
            kony.sdk.mvvm.log.info("success fetching data ", response);
            scopeObj.getController().processData(response);
        }

        function error(err) {
            //Error fetching data
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("In fetchData errorcallback in controller extension ", err);
            var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_FETCH_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_FETCH_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    /** 
     * This method processes fetched data. Developer can edit.
     * Default implementation processes the provided data to required format for bind.
     * @param {Object} data - fetched data. (Default : data map, group id as key and records array as value)
     * @memberof frmConfirmDepositKAControllerExtension#
     * @returns {Object} - processed data
     */
    processData: function(data) {
        try {
            var scopeObj = this;
            var processedData = this.$class.$superp.processData.call(this, data);
            this.getController().bindData(processedData);
            return processedData;
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("Error in processData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_PROCESSDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_PROCESSDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    /** 
     * This method binds the processed data to the form. Developer can edit.
     * Default implementation binds the data to widgets in the form.
     * @param {Object} data - processed data.(Default : data map for each group, widget id as key and widget data as value)
     * @memberof frmConfirmDepositKAControllerExtension#
     */
    bindData: function(data) {
        try {
            var formmodel = this.getController().getFormModel();
            formmodel.clear();
            //this.$class.$superp.bindData.call(this, data);
            //this.getController().getFormModel().formatUI();
            var controllerContextData = this.getController().getContextData();
            var customInfo = controllerContextData.getCustomInfo("dataDeposit");
            formmodel.setViewAttributeByProperty("lblCurrencytype", "text", kony.retailBanking.util.formatingAmount.appendingCurrencyCodeToAmount(customInfo.amount, kony.retailBanking.globalData.globals.CurrencyCode));
            formmodel.setWidgetData("transactionAmount", customInfo.amount);
            formmodel.setViewAttributeByProperty("transactionName", "text", customInfo.toAccountName);
            formmodel.setWidgetData("toAccountNumberKA", customInfo.toAccount);
            formmodel.setWidgetData("lblfrontKA", customInfo.checkImgFront);
            formmodel.setWidgetData("lblbackKA", customInfo.checkImgBack);
            if (customInfo.notes) {
                formmodel.setWidgetData("transactionNotes", customInfo.notes);
                formmodel.performActionOnView("flxNotesKA", "setVisibility", [true]);
            } else formmodel.performActionOnView("flxNotesKA", "setVisibility", [false]);
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            this.getController().showForm();
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("Error in bindData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_BINDDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_BINDDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    /** 
     * This method is entry point for save flow. Developer can edit.
     * Default implementation saves the entity record from the data of widgets defined in form config 
     * @memberof frmConfirmDepositKAControllerExtension#
     */
    saveData: function() {
        var onConfirmDeposit = function(response) {
            if (response["status"] === "success") {
                try {
                    var scopeObj = this;
                    var formModel = this.getController() && this.getController().getFormModel();
                    var amount = formModel.getViewAttributeByProperty("transactionAmount", "text");
                    //formModel.setViewAttributeByProperty("lbltransactionAmount","text",amount);
                    formModel.setViewAttributeByProperty("transactionAmount", "text", amount);
                    formModel.setViewAttributeByProperty("depositsType", "text", "Deposit");
                    var date = new Date();
                    date = kony.retailBanking.util.formatingDate.getApplicationFormattedDateTime(date, kony.retailBanking.util.BACKEND_DATE_FORMAT);
                    formModel.setViewAttributeByProperty("createdDate", "text", date);
                    depositSuccessFormPreShow();
                    frmDepositSuccessKA.show();
                    depositsuccessanimationShow();
                    this.$class.$superp.saveData.call(this, success, error);
                } catch (err) {
                    var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, err);
                    kony.sdk.mvvm.log.error(exception.toString());
                }
            } else {
                alert(response["status"] + ": " + response["msg"]);
            }

            function success(res) {
                //Successfully created record
                // navigateToSuccess(res);
                var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
                var listController = INSTANCE.getFormController("frmDepositSuccessKA");
                var navObject = new kony.sdk.mvvm.NavigationObject();
                if (res.referenceId) navObject.setCustomInfo("TransactionID", i18n_referenceId + res.referenceId);
                else navObject.setCustomInfo("TransactionID", "");
                listController.loadDataAndShowForm(navObject);
                var amount = ""; //frmNewDepositKA.amountTextField.text;
                KNYMetricsService.sendCustomMetrics("frmSuccessFormKA", {
                    "ChequeDepositStatus": "Success",
                    "ChequeDepositAverageAmount": parseInt(amount)
                });
            }

            function error(err) {
                //Handle error case
                frmDepositSuccessKA.successText.text = i18n_transactionFailed;
                frmDepositSuccessKA.successTitle.text = i18n_sorryForinconvenience;
                frmDepositSuccessKA.processing.text = i18n_processingTransaction;
                depositerrorFormPostShow();
                kony.sdk.mvvm.log.error("In saveData errorcallback in controller extension ", err);
                var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, err);
                kony.sdk.mvvm.log.error(exception.toString());
            }
        };
        //part of extention
        serviceCharges(this, onConfirmDeposit);
    },
    /** 
     * This method is entry point for delete/remove flow. Developer can edit.
     * Default implementation deletes the entity record displayed in form (primary keys are needed)
     * @memberof frmConfirmDepositKAControllerExtension#
     */
    deleteData: function() {
        try {
            var scopeObj = this;
            this.$class.$superp.deleteData.call(this, success, error);
        } catch (err) {
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

        function success(res) {
            //Successfully deleting record
            kony.sdk.mvvm.log.info("success deleting record " + JSON.stringify(res));
        }

        function error(err) {
            //Handle error case
            kony.sdk.mvvm.log.error("In deleteData errorcallback in controller extension ", err);
            var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    /** 
     * This method shows form.
     * @memberof frmConfirmDepositKAControllerExtension#
     */
    showForm: function() {
        try {
            var formmodel = this.getController().getFormModel();
            formmodel.showView();
        } catch (e) {
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SHOWFORM_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SHOWFORM_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    }
});