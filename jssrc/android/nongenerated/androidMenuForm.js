var CurrForm;
var OldForm;
// onClick action for hamburger icons on Android
function activeAndroidNav() {
    //     CurrForm  = kony.application.getCurrentForm();
    //   	if (CurrForm.navigationWrapper){
    //       CurrForm.remove(CurrForm.navigationWrapper);
    //     } 
    //     CurrForm.add(frmAndroidMenuKA.navigationWrapper);
    //   	CurrForm.navigationDrawer.left = "-85%";
    //   	CurrForm.navigationDrawerBkg.isVisible = false;
    //     CurrForm.navigationDrawerBkg.opacity = 0;
    //   	setAndroidPrimaryMenuSegmentData();
    //   	animateAndroidNav();
    kony.print("Commented due to Removal of RB Forms");
}
// Show Android Nav
function animateAndroidNav() {
    CurrForm.navigationDrawer.animate(kony.ui.createAnimation({
        "100": {
            "left": "0%",
            "stepConfig": {
                "timingFunction": easeIn
            }
        }
    }), {
        "fillMode": forwards,
        "duration": duration / 1.3,
        "delay": 0.3
    }, {
        "animationEnd": function() {}
    });
    CurrForm.navigationDrawerBkg.isVisible = true;
    CurrForm.navigationDrawerBkg.animate(kony.ui.createAnimation({
        "100": {
            "opacity": 1,
            "stepConfig": {
                "timingFunction": easeIn
            }
        }
    }), {
        "fillMode": forwards,
        "duration": duration
    }, {
        "animationEnd": function() {}
    });
}
// Close Android Nav and remove it from the form
function closeAndroidNav() {
    if (CurrForm.navigationWrapper) {
        CurrForm.navigationDrawer.animate(kony.ui.createAnimation({
            "100": {
                "left": "-85%",
                "stepConfig": {
                    "timingFunction": easeIn
                }
            }
        }), {
            "fillMode": forwards,
            "duration": duration / 1.3
        }, {
            "animationEnd": function() {}
        });
        CurrForm.navigationDrawerBkg.animate(kony.ui.createAnimation({
            "100": {
                "opacity": 0,
                "stepConfig": {
                    "timingFunction": easeIn
                }
            }
        }), {
            "fillMode": forwards,
            "duration": duration
        }, {
            "animationEnd": function() {
                //CurrForm.navigationDrawerBkg.isVisible = false;
                // CurrForm.remove(CurrForm.navigationWrapper);
            }
        });
        CurrForm.remove(CurrForm.navigationWrapper);
    }
}

function androidPrimaryNavigationClick() {
    //    closeAndroidNav();
    //   var selectedRow = frmAndroidMenuKA.androidPrimaryNavigation.selectedRowIndex[1];
    //     data = frmAndroidMenuKA.androidPrimaryNavigation.data;
    //   	var selectedRow_value = data[selectedRow]["Label01f864cb479e14f"]; 
    //   	 gblfrmName = "";
    //   //kony.print("entering into the gblfrmName"+gblfrmName);
    //   switch(selectedRow_value){
    //       case i18n_accountOverview:
    //         	showFormOrderList();
    //         	break;
    //        case i18n_deposits:
    //     		additionalValidationRdc(this,gotoDeposits);
    //         	break;
    //       case i18n_messages:
    //             onMessageInboxSelected();
    //         	showMessagesList();
    //             break;
    //        case i18n_locateUs:
    //     		//	onClickLocator();
    //       		//frmLocatorKA.show();
    //       		onClickLocateUS(true);
    //         	break;
    //        case i18n_contactUs:
    //             frmContactUsKA.btnback.setVisibility(false);
    //             frmContactUsKA.hamburgerButton.setVisibility(true);
    //             contactUsPreshow();
    //         	break;
    //        case i18n_resources: 
    //       		frmMoreLandingKA.show();
    //             break;
    //        case 'ChatBot':
    //            var currentForm = kony.application.getCurrentForm();
    //             kony.chatbots.bot.showBotSplash(currentForm);
    //             break;
    //       default:
    //         	frmAndroidMenuKA.show();
    //     }
    kony.print("Commented Due to Removal of RB Forms");
}

function androidSecondaryNavigationClick() {
    //   closeAndroidNav();
    //   var selectedRow = frmAndroidMenuKA.androidSecondaryNavigation.selectedRowIndex[1];
    // 	data = frmAndroidMenuKA.androidSecondaryNavigation.data;
    //   	var selectedRow_value = data[selectedRow]["Label01f864cb479e14f"];
    //   switch(selectedRow){
    //       case i18n_messages:
    //             onMessageInboxSelected();
    //         	showMessagesList();
    //             break;
    //        case i18n_locateUs:
    //     		//	onClickLocator();
    //       		//frmLocatorKA.show();
    //       		onClickLocateUS(true);
    //         	break;
    //        case i18n_contactUs:
    //             frmContactUsKA.btnback.setVisibility(false);
    //             frmContactUsKA.hamburgerButton.setVisibility(true);
    //     		frmContactUsKA.show();
    //         	break;
    //        case i18n_settings:
    //             frmMoreLandingKA.show();
    //             break;
    //       default:
    //         	frmAndroidMenuKA.show();
    //     }
    kony.print("Commented due to Removal of RB Forms");
}