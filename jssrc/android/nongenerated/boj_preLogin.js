//Type your code here
function initialisePreLoginForms() {
    var appContext = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    try {
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        //frmRegisterUser
        var frmRegisterUserModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmRegisterUserConfig);
        var frmRegisterUserControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmRegisterUserController", appContext, frmRegisterUserModelConfigObj);
        var frmRegisterUserControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmRegisterUserControllerExtension", frmRegisterUserControllerObj);
        frmRegisterUserControllerObj.setControllerExtensionObject(frmRegisterUserControllerExtObj);
        var frmRegisterUserFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmRegisterUserFormModel", frmRegisterUserControllerObj);
        var frmRegisterUserFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmRegisterUserFormModelExtension", frmRegisterUserFormModelObj);
        frmRegisterUserFormModelObj.setFormModelExtensionObj(frmRegisterUserFormModelExtObj);
        appContext.setFormController("frmRegisterUser", frmRegisterUserControllerObj);
        //frmEnrolluserLandingKA
        var frmEnrolluserLandingKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmEnrolluserLandingKAConfig);
        var frmEnrolluserLandingKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmEnrolluserLandingKAController", appContext, frmEnrolluserLandingKAModelConfigObj);
        var frmEnrolluserLandingKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmEnrolluserLandingKAControllerExtension", frmEnrolluserLandingKAControllerObj);
        frmEnrolluserLandingKAControllerObj.setControllerExtensionObject(frmEnrolluserLandingKAControllerExtObj);
        var frmEnrolluserLandingKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmEnrolluserLandingKAFormModel", frmEnrolluserLandingKAControllerObj);
        var frmEnrolluserLandingKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmEnrolluserLandingKAFormModelExtension", frmEnrolluserLandingKAFormModelObj);
        frmEnrolluserLandingKAFormModelObj.setFormModelExtensionObj(frmEnrolluserLandingKAFormModelExtObj);
        appContext.setFormController("frmEnrolluserLandingKA", frmEnrolluserLandingKAControllerObj);
        var frmDeviceDeRegistrationKAModelConfigObj = appContext.getFactorySharedInstance().createConfigClassObject(frmDeviceDeRegistrationKAConfig);
        var frmDeviceDeRegistrationKAControllerObj = appContext.getFactorySharedInstance().createFormControllerObject("kony.sdk.mvvm.frmDeviceDeRegistrationKAController", appContext, frmDeviceDeRegistrationKAModelConfigObj);
        var frmDeviceDeRegistrationKAControllerExtObj = appContext.getFactorySharedInstance().createFormControllerExtObject("kony.sdk.mvvm.frmDeviceDeRegistrationKAControllerExtension", frmDeviceDeRegistrationKAControllerObj);
        frmDeviceDeRegistrationKAControllerObj.setControllerExtensionObject(frmDeviceDeRegistrationKAControllerExtObj);
        var frmDeviceDeRegistrationKAFormModelObj = appContext.getFactorySharedInstance().createFormModelObject("kony.sdk.mvvm.frmDeviceDeRegistrationKAFormModel", frmDeviceDeRegistrationKAControllerObj);
        var frmDeviceDeRegistrationKAFormModelExtObj = appContext.getFactorySharedInstance().createFormModelExtObject("kony.sdk.mvvm.frmDeviceDeRegistrationKAFormModelExtension", frmDeviceDeRegistrationKAFormModelObj);
        frmDeviceDeRegistrationKAFormModelObj.setFormModelExtensionObj(frmDeviceDeRegistrationKAFormModelExtObj);
        appContext.setFormController("frmDeviceDeRegistrationKA", frmDeviceDeRegistrationKAControllerObj);
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    } catch (err) {
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        exceptionLogCall("initialisePreLoginForms", "UI ERROR", "UI", err);
    }
}