//Type your code here
function navigate_LOAN_POSTPONE() {
    try {
        var index2 = parseInt(DealsLoansCurrIndex) || 0;
        assin_DATA_LOAN_POSTPONE_SCREEN(kony.retailBanking.globalData.accountsDashboardData.loansData[index2]);
        frmLoanPostpone.show();
    } catch (e) {
        kony.print("Exception_navigate_LOAN_POSTPONE ::" + e);
    }
}

function assin_DATA_LOAN_POSTPONE_SCREEN(data) {
    try {
        frmLoanPostpone.lblLoanPostponeTitle.text = geti18Value("i18n.loanpostpone.title");
        frmLoanPostpone.lblLoanAccountNumber.text = data.accountID;
        frmLoanPostpone.lblLoanAccNum.text = data.accountID;
        frmLoanPostpone.lblRemainingBalance.text = data.currentBalance + " " + data.currencyCode;
        frmLoanPostpone.lblRemainingBal.text = data.currentBalance + " " + data.currencyCode;
        frmLoanPostpone.lblNextPaymentDate.text = frmAccountInfoKA.lblNextPayment1.text;
        frmLoanPostpone.lblNxtPaymntDate.text = frmAccountInfoKA.lblNextPayment1.text;
        frmLoanPostpone.lblPaymentAmount.text = frmAccountInfoKA.CopyaccountNumberLabel014bda555f8c040.text;
        frmLoanPostpone.lblPaymentAmount1.text = frmAccountInfoKA.CopyaccountNumberLabel014bda555f8c040.text;
        frmLoanPostpone.flxLoanPostponeConfirmation.setVisibility(false);
        frmLoanPostpone.flxLoanPostpone.setVisibility(true);
        frmLoanPostpone.btnEndOfLoan.text = "t";
        frmLoanPostpone.btnCurLoanInst.text = "s";
        frmLoanPostpone.lblTermsandConditionsCheckBox.text = "q";
        frmLoanPostpone.lblIntPayment.text = frmLoanPostpone.lblEndOfLoan.text;
        frmLoanPostpone.btnNext.setVisibility(true);
        frmLoanPostpone.btnNext.skin = "jomopaynextEnabled";
    } catch (e) {
        kony.print("Exception_assin_DATA_LOAN_POSTPONE_SCREEN ::" + e);
    }
}

function radio_SELECTION_INTEREST_PAYMENT(widget1Ref, widget2Ref, flow) {
    try {
        if (widget1Ref.text === "t" && widget2Ref.text === "s" && flow == 2) {
            widget1Ref.text = "s";
            widget2Ref.text = "t";
            frmLoanPostpone.lblIntPayment.text = frmLoanPostpone.lblCurLoanInst.text;
        } else if (widget1Ref.text === "s" && widget2Ref.text === "t" && flow == 1) {
            widget1Ref.text = "t";
            widget2Ref.text = "s";
            frmLoanPostpone.lblIntPayment.text = frmLoanPostpone.lblEndOfLoan.text;
        }
    } catch (e) {
        kony.print("Exception_radio_SELECTION_INTEREST_PAYMENT ::" + e);
    }
}

function onClick_BACK_LOAN_POSTPONE() {
    try {
        if (frmLoanPostpone.flxLoanPostponeConfirmation.isVisible === true) {
            frmLoanPostpone.lblLoanPostponeTitle.text = geti18Value("i18n.loanpostpone.title");
            frmLoanPostpone.flxLoanPostponeConfirmation.setVisibility(false);
            frmLoanPostpone.lblNumofInsPostpone.text = "";
            frmLoanPostpone.btnNext.setVisibility(true);
            frmLoanPostpone.flxLoanPostpone.setVisibility(true);
        } else if (frmLoanPostpone.flxLoanPostpone.isVisible === true) {
            frmAccountInfoKA.show();
            frmLoanPostpone.destroy();
        }
    } catch (e) {
        kony.print("Exception_onClick_BACK_LOAN_POSTPONE ::" + e);
    }
}

function onClick_NEXT_LOAN_POSTPONE() {
    try {
        if (frmLoanPostpone.btnNext.skin === "jomopaynextEnabled") {
            frmLoanPostpone.lblLoanPostponeTitle.text = geti18Value("i18n.Transfer.ConfirmDet");
            frmLoanPostpone.lblNumofInsPostpone.text = frmLoanPostpone.lblMonths.text;
            frmLoanPostpone.flxLoanPostpone.setVisibility(false);
            frmLoanPostpone.btnNext.setVisibility(false);
            frmLoanPostpone.flxLoanPostponeConfirmation.setVisibility(true);
            frmLoanPostpone.lblTermsandConditionsCheckBox.text = "q";
            frmLoanPostpone.richtxtTermsandCondition.text = "<U>" + geti18Value("i18n.tnc.cardrequest") + "</U>";
        }
    } catch (e) {
        kony.print("Exception_onClick_NEXT_LOAN_POSTPONE ::" + e);
    }
}

function serv_LOAN_POSTPONE() {
    try {
        var index2 = parseInt(DealsLoansCurrIndex) || 0;
        var loanData = kony.retailBanking.globalData.accountsDashboardData.loansData[index2];
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var options = {
            "access": "online",
            "objectName": "RBObjects"
        };
        var headers = {};
        var serviceName = "RBObjects";
        var modelObj = INSTANCE.getModel("Accounts", serviceName, options);
        var dataObject = new kony.sdk.dto.DataObject("Accounts");
        dataObject.addField("custId", custid);
        dataObject.addField("accNumber", loanData.accountID);
        dataObject.addField("accBranch", loanData.branchNumber);
        dataObject.addField("loanSequence", loanData.reference_no);
        dataObject.addField("loanBalance", loanData.currentBalance.replace(/,/g, ""));
        kony.print("frmLoanPostpone.lblNumofInsPostpone.text ::" + frmLoanPostpone.lblNumofInsPostpone.text);
        // hassan BMA-1927
        //dataObject.addField("billMaturityDate",frmLoanPostpone.lblNumofInsPostpone.text);
        dataObject.addField("billMaturityDate", 1);
        // hassan BMA-1927
        dataObject.addField("interestCommCond", frmLoanPostpone.lblIntPayment.text);
        dataObject.addField("p_channel", "MOBILE");
        dataObject.addField("p_user_id", "BOJMOB");
        var serviceOptions = {
            "dataObject": dataObject,
            "headers": headers
        };
        kony.print("dataobject:: " + JSON.stringify(serviceOptions));
        if (kony.sdk.isNetworkAvailable()) {
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
            modelObj.customVerb("LoanPostpone", serviceOptions, success_serv_LOAN_POSTPONE, error_serv_LOAN_POSTPONE);
        }
    } catch (e) {
        kony.application.dismissLoadingScreen();
        kony.print("Exception_serv_LOAN_POSTPONE ::" + e);
    }
}

function success_serv_LOAN_POSTPONE(res) {
    try {
        kony.print("reponse ::" + JSON.stringify(res)); //res.refNumber
        if (res.ErrorCode === "00000") {
            kony.boj.populateSuccessScreen("success.png", geti18Value("i18n.common.success"), res.refNumber + ": " + geti18Value("i18n.loanpostpone.successmsg"), geti18Value("i18n.Bene.GotoAccDash"), geti18nkey("i18n.common.AccountDashboard"), "", "", "", "", "");
        } else {
            var errMsg = getErrorMessage(res.ErrorCode);
            kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), errMsg, geti18Value("i18n.Bene.GotoAccDash"), geti18nkey("i18n.common.AccountDashboard"), "", "", "", "");
        }
        kony.application.dismissLoadingScreen();
    } catch (e) {
        kony.application.dismissLoadingScreen();
        kony.print("Exception_success_serv_LOAN_POSTPONE ::" + e);
    }
}

function error_serv_LOAN_POSTPONE(err) {
    try {
        kony.boj.populateSuccessScreen("failure.png", geti18Value("i18n.Bene.Failed"), geti18Value("i18n.common.somethingwentwrong"), geti18Value("i18n.Bene.GotoAccDash"), geti18nkey("i18n.common.AccountDashboard"), "", "", "", "");
        kony.application.dismissLoadingScreen();
    } catch (e) {
        kony.application.dismissLoadingScreen();
        kony.print("Exception_error_serv_LOAN_POSTPONE ::" + e);
    }
}

function assign_MNTH_LIST() {
    var limit = 3,
        temp = [];
    if (parseInt(frmAccountInfoKA.lblReducedInterestRate.text) < 3) limit = parseInt(frmAccountInfoKA.lblReducedInterestRate.text);
    frmCurrency.segCurrency.widgetDataMap = {
        lblMonth: "lblMonth"
    };
    for (var i = 1; i <= limit; i++) {
        temp.push({
            "lblMonth": i + "",
            template: flxMonthsTemplate
        });
    }
    frmCurrency.segCurrency.setData(temp);
}