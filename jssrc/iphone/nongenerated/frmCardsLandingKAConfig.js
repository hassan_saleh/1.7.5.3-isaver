var frmCardsLandingKAConfig = {
    "formid": "frmCardsLandingKA",
    "frmCardsLandingKA": {
        "entity": "Cards",
        "objectServiceName": "RBObjects",
        "objectServiceOptions": {
            "access": "online"
        },
    },
    "segCardsLanding": {
        "fieldprops": {
            "widgettype": "Segment",
            "entity": "Cards",
            "additionalFields": ["card_type_desc", "card_category", "card_category_desc", "issue_date", "last_used_date", "bill_cycle", "card_limit", "installment_limit", "spending_limit", "cash_limit", "daily_cash_limit", "currency", "id_statement", "available_credit_limit", "available_cash_limit", "total_billed_amount", "total_unbilled_amount", "auto_pay", "recovery_account", "statement_date", "last_payment_date", "last_payment_amount", "total_due_amt", "current_authorization", "primary_card_no", "card_point", "nick_name", "card_code", "prefNumber", "showHideFlag", "nickname", "card_mobile_number"],
            "field": {
                "cardType": {
                    "widgettype": "Label",
                    "field": "card_type"
                },
                "cardNumber": {
                    "widgettype": "Label",
                    "field": "card_num"
                },
                "CardHolder": {
                    "widgettype": "Label",
                    "field": "name_on_card"
                },
                "ValidThru": {
                    "widgettype": "Label",
                    "field": "card_exp_date"
                },
                "cardImage": {
                    "widgettype": "Image2",
                    "field": "cardImage"
                }
            }
        }
    }
};