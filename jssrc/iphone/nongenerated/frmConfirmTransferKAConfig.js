var frmConfirmTransferKAConfig = {
    "formid": "frmConfirmTransferKA",
    "frmConfirmTransferKA": {
        "entity": "Transactions",
        "objectServiceName": "RBObjects",
        "objectServiceOptions": {
            "access": "online"
        },
    },
    "transactionAmount": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "amount"
        }
    },
    "transactionNotes": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "transactionsNotes"
        }
    },
    "fromAccountNumberKA": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "fromAccountNumber"
        }
    },
    "toAccountNumberKA": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "toAccountNumber"
        }
    },
    "frmAccBranchCode": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "SourceBranchCode"
        }
    },
    "toAccBranchCode": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "BenBranchCode" //Not needed for INT and DOM transfers
        }
    },
    "hiddenlblCommRate": { //pcom
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "p_comm" //Not needed for INT and DOM transfers
        }
    },
    "hiddenlblExchComm": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "p_exc_comm" //Not needed for INT and DOM transfers
        }
    },
    "hiddenlblCrAmt": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "p_cr_amount" //Not needed for INT and DOM transfers
        }
    },
    "hiddenlblLclAmt": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "p_lc_amount" //Not needed for INT and DOM transfers
        }
    },
    "payMode": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "p_pay_mode"
        }
    },
    "transFlag": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "TransferFlag"
        }
    },
    "lblFreType": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "frequencyType"
        }
    },
    "hiddenLblFromDate": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "frequencyStartDate"
        }
    },
    "hiddenLblToDate": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "frequencyEndDate"
        }
    },
    "lblReccuureceFreq": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "numberOfRecurrences"
        }
    },
    "lblBenName": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "toAccountName"
        }
    },
    "lblRemmit": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "p_remmit_purpose"
        }
    },
    "lblBenAdr1": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "BenAddress1"
        }
    },
    "lblBenAdr2": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "BenAddress2"
        }
    },
    "lblBenAdr3": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "BenAddress3"
        }
    },
    "lblBenCity": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "BenCity"
        }
    },
    "lblBenCountry": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "BenCountry"
        }
    },
    "lblCrearCode": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "p_national_crear_code"
        }
    },
    "lblCrearType": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "p_national_crear_code_type"
        }
    },
    "lblBenBankName": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "p_ben_bank_name"
        }
    },
    "lblBenBankAdr1": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "p_ben_bank_addr1"
        }
    },
    "lblBenBankAdr2": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "p_ben_bank_addr2"
        }
    },
    "lblBenBankCity": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "p_ben_bank_city"
        }
    },
    "lblBenBankCountry": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "p_ben_bank_country"
        }
    },
    "lblTransCur": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "p_trans_amt_ccy"
        }
    },
    "lblPmt1": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "p_pmt_details1"
        }
    },
    "lblPmt2": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "p_pmt_details2"
        }
    },
    /*  
      "lblNarr":{
        "fieldprops": {
          "entity":"Transactions",
          "constrained":true,
          "widgettype":"Label",
          "field":"p_narrative"
        }		  
      },*/
    "lblSwift": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "SwiftCode"
        }
    },
    "lblDealRefNo": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "p_deal_ref_no"
        }
    },
    "lblImbSC": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "p_imb_swift_code"
        }
    },
    "lblImbCoun": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "p_imb_country"
        }
    },
    "lblImbAdr1": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "p_imb_addr1"
        }
    },
    "lblCorCharges": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "p_corresp_charges"
        }
    },
    "lblImbAdr2": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "p_imb_addr2"
        }
    },
    "lblImbCity": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "p_imb_city"
        }
    },
    "lblCustomerID": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "custId"
        }
    },
    "lblBenBranchCode": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "p_ben_branch_code"
        }
    },
    "lblIBAN": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "p_iban"
        }
    },
    "lblRefID": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "referenceId"
        }
    },
    "lblPmtDetails3": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "p_pmt_details3"
        }
    },
    "lblPmtDetails4": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "p_pmt_details4"
        }
    },
    "lblToAccType": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "toAccountType"
        }
    },
    "lblImbName": {
        "fieldprops": {
            "entity": "Transactions",
            "constrained": true,
            "widgettype": "Label",
            "field": "p_imb_name"
        }
    }
    // "transactionName":{
    //         "fieldprops": {
    //           "entity":"Transactions",
    //           "constrained":true,
    //           "widgettype":"Label",
    //           "field":"toAccountName"
    // 		}		  
    //       }, 
    // "transactionFrom":{
    //         "fieldprops": {
    //           "entity":"Transactions",
    //           "constrained":true,
    //           "widgettype":"Label",
    //           "field":"fromAccountName"
    // 		}		  
    //       }, 
    // "lblScheduledDate":{
    //         "fieldprops": {
    //           "entity":"Transactions",
    //           "constrained":true,
    //           "widgettype":"Label",
    //           "field":"scheduledDate"
    // 		}		  
    //       }, 
};