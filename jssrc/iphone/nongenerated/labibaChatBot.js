function setLabibaLoginProcess() {
    frmLoginKA.flxIconMove.onTouchStart = chatbuttonTouchStart;
    frmLoginKA.flxIconMove.onTouchMove = chatbuttonTouchMove;
    frmLoginKA.flxIconMove.left = "75%";
    frmLoginKA.flxIconMove.top = "4%";
    if (kony.store.getItem("cameFromLabiba") === true) {
        frmLoginKA.flxRegisterMobileBanking.setVisibility(false);
        frmLoginKA.flxForgotP.setVisibility(false);
        frmLoginKA.flxSwitchLanguage.setVisibility(false);
        frmLoginKA.flxPayAppLink.setVisibility(false);
        frmLoginKA.contactLink.setVisibility(false);
        frmLoginKA.atmLink.setVisibility(false);
        frmLoginKA.btnOnboarding.setVisibility(false);
        frmLoginKA.lblInformationSuppIcon.setVisibility(false);
        frmLoginKA.lblAtmBranchicon.setVisibility(false);
        frmLoginKA.lblNewCusticon.setVisibility(false);
        frmLoginKA.btnCancelLabibaLogin.setVisibility(true);
        //     frmLoginKA.btnPinLogin.left = "55%"; Ahmad 12-8-2020
        //     frmLoginKA.btnTouchID.right = "3%";  Ahmad 12-8-2020
        //     frmLoginKA.btnFaceID.right = "3%";  Ahmad 12-8-2020
    } else {
        frmLoginKA.flxRegisterMobileBanking.setVisibility(true);
        frmLoginKA.flxForgotP.setVisibility(true);
        frmLoginKA.flxSwitchLanguage.setVisibility(true);
        //     frmLoginKA.flxPayAppLink.setVisibility(true);
        frmLoginKA.contactLink.setVisibility(true);
        frmLoginKA.btnOnboarding.setVisibility(true);
        frmLoginKA.atmLink.setVisibility(true);
        frmLoginKA.lblInformationSuppIcon.setVisibility(true);
        frmLoginKA.lblAtmBranchicon.setVisibility(true);
        frmLoginKA.lblNewCusticon.setVisibility(true);
        frmLoginKA.btnCancelLabibaLogin.setVisibility(false);
        //     frmLoginKA.btnPinLogin.left = "55%"; Ahmad 12-8-2020
        //     frmLoginKA.btnTouchID.right = "3%"; Ahmad 12-8-2020
        //     frmLoginKA.btnFaceID.right = "3%"; Ahmad 12-8-2020
    }
}

function cancelLabibaLoginProcess() {
    if (kony.store.getItem("cameFromLabiba") === true) {
        kony.store.setItem("cameFromLabiba", false);
        frmLoginKA.flxRegisterMobileBanking.setVisibility(true);
        frmLoginKA.flxForgotP.setVisibility(true);
        frmLoginKA.flxSwitchLanguage.setVisibility(true);
        //     frmLoginKA.flxPayAppLink.setVisibility(true);
        frmLoginKA.contactLink.setVisibility(true);
        frmLoginKA.btnOnboarding.setVisibility(true);
        frmLoginKA.atmLink.setVisibility(true);
        frmLoginKA.btnCancelLabibaLogin.setVisibility(false);
        //     frmLoginKA.btnPinLogin.left = "55%"; Ahmad 12-8-2020
        //     frmLoginKA.btnTouchID.right = "3%"; Ahmad 12-8-2020
        //     frmLoginKA.btnFaceID.right = "3%";  Ahmad 12-8-2020
    }
}

function startChatbotBeforeLogin() {
    frmLoginKA.flxIconMove.left = "75%";
    frmLoginKA.flxIconMove.top = "4%";
    var langL = kony.store.getItem("langPrefObj");
    if (langL === "ar") langL = "Arabic";
    else if (langL === "en") langL = "English";
    kony.store.setItem("cameFromLabiba", false);

    function labibaCallBack2() {
        frmLoginKA.show();
        kony.store.setItem("cameFromLabiba", true);
    }
    kony.print("Starting Labiba");
    try {
        var Labiba2Object = new LabibaConfigIOS.Labiba2();
        Labiba2Object.startLabiba(langL, "0", labibaCallBack2);
    } catch (ex) {
        kony.print("error calling labiba FFI " + JSON.stringify(ex) + "   " + ex);
    }
}

function startChatbotAfterLogin(custID, cameFromInside) {
    function labibaCallBack2() {}
    if (kony.store.getItem("cameFromLabiba") === true || cameFromInside === true) {
        var langL = kony.store.getItem("langPrefObj");
        if (langL === "ar") langL = "Arabic";
        else if (langL === "en") langL = "English";
        kony.store.setItem("cameFromLabiba", false);
        var returnedValue;
        try {
            var Labiba2Object = new LabibaConfigIOS.Labiba2();
            Labiba2Object.startLabiba(langL, custID, labibaCallBack2);
        } catch (ex) {
            kony.print("error calling labiba FFI " + JSON.stringify(ex));
        }
    }
}
var btnStartX = 0,
    btnStartY = 0,
    btnLeft = 0,
    btnTop = 0;

function chatbuttonTouchStart(source, x, y) {
    btnStartX = x;
    btnStartY = y;
}

function chatbuttonTouchMove(source, x, y) {
    btnLeft = parseInt(source.frame.x);
    btnTop = parseInt(source.frame.y);
    frmLoginKA.flxIconMove.left = btnLeft + (x - btnStartX);
    frmLoginKA.flxIconMove.top = btnTop + (y - btnStartY);
    frmLoginKA.apScrollEnable.forceLayout();
}

function chatbuttonDashboardTouchMove(source, x, y) {
    btnLeft = parseInt(source.frame.x);
    btnTop = parseInt(source.frame.y);
    frmAccountsLandingKA.flxIconMove1.left = btnLeft + (x - btnStartX);
    frmAccountsLandingKA.flxIconMove1.top = btnTop + (y - btnStartY);
    frmAccountsLandingKA.forceLayout();
}

function startOnboardingActivity() {
    var langOnboarding = kony.store.getItem("langPrefObj");
    if (langOnboarding === "ar") langOnboarding = true;
    else if (langOnboarding === "en") langOnboarding = false;
    var OnBoardingIOSObject = new IOSonboardingConfig.OnBoardingIOS();
    OnBoardingIOSObject.startOnboarding(langOnboarding);
}