var hasTouchId;
var accountsPreviewStatus;
var deviceRegStatus;
var touchIDStatus;
var defaultPage;
var defaultTransferAcc = "";
var defaultDepositAcc = "";
var defaultPaymentsAcc = "";
var defaultCardlessAcc = "";
var profSettings;
var isPinEnabled;
var alertsFlg;
var defaultLogin;
var isFaceIDEnabled;
var tempSettingsFlag = 0;

function getAccountNameForAccID(accID) {
    var data = kony.retailBanking.globalData.accounts.getAccountsData();
    var accountName = "";
    for (var i = 0; i < data.length; i++) {
        if (accID === data[i].accountID) {
            accountName = data[i].nickName;
            return accountName;
        }
    }
}

function getSettingsFeaturesStatus() {
    var settingsData = kony.store.getItem("settingsflagsObject");
    if (isSettingsFlagEnabled("isFacialAuthEnabledFlag")) {
        isFaceIDEnabled = i18n_onLabel;
    } else {
        isFaceIDEnabled = i18n_offLabel;
    }
    var tempSettingsData = kony.store.getItem("settingsflagsObject");
    if (tempSettingsData.defaultLoginMode == "touchid") defaultLogin = "Touch Id";
    else if (tempSettingsData.defaultLoginMode == "faceid") defaultLogin = "Face Id";
    else if (tempSettingsData.defaultLoginMode == "pin") defaultLogin = "Pin";
    else if (tempSettingsData.defaultLoginMode == "password") defaultLogin = "Password";
    //     if(kony.retailBanking.globalData.deviceInfo.isTouchIDSupported())
    //     {
    //           if(settingsData.touchIDEnabledFlag===true )
    //           {
    //            touchIDStatus=i18n_onLabel;
    //            if(kony.retailBanking.globalData.deviceInfo.isIphone())
    //            		frmUserSettingsTouchIdKA.touchIdSwitch.selectedIndex = 0;
    //           else
    //            		frmUserSettingsTouchIdKA.touchIdCheckBox.selectedKeys = ["touchId"];
    //       	  }
    //          else
    //          {
    //            touchIDStatus=i18n_offLabel;
    //            if(kony.retailBanking.globalData.deviceInfo.isIphone())
    //            	   frmUserSettingsTouchIdKA.touchIdSwitch.selectedIndex = 1;
    //            else
    //            		frmUserSettingsTouchIdKA.touchIdCheckBox.selectedKeys = null;
    //          }
    //     }
    getPinSettingsStatus();
    if (settingsData.deviceRegisterFlag !== null && settingsData.deviceRegisterFlag === true) deviceRegStatus = i18n_onLabel;
    else deviceRegStatus = i18n_offLabel;
    if (settingsData.DefaultTransferAcctNo !== null) defaultTransferAcc = getAccountNameForAccID(settingsData.DefaultTransferAcctNo);
    if (settingsData.DefaultDepositAcctNo !== null) defaultDepositAcc = getAccountNameForAccID(settingsData.DefaultDepositAcctNo);
    if (settingsData.DefaultPaymentAcctNo !== null) defaultPaymentsAcc = getAccountNameForAccID(settingsData.DefaultPaymentAcctNo);
    if (settingsData.DefaultCardlessAcctNo !== null) defaultCardlessAcc = getAccountNameForAccID(settingsData.DefaultCardlessAcctNo);
    if (settingsData.alerts !== null && settingsData.alerts === true) {
        alertsFlg = i18n_onLabel;
        if (!kony.retailBanking.globalData.deviceInfo.isIphone()) {
            frmAlertsKA.infoImg.src = "checkbox_on.png";
        } else {
            frmAlertsKA.SwitchAlert.selectedIndex = 0;
        }
        frmAlertsKA.alertsTypes.isVisible = true;
    } else {
        alertsFlg = i18n_offLabel;
        if (!kony.retailBanking.globalData.deviceInfo.isIphone()) {
            frmAlertsKA.infoImg.src = "checkbox_off.png";
        } else {
            frmAlertsKA.SwitchAlert.selectedIndex = 1;
        }
        frmAlertsKA.alertsTypes.isVisible = false;
    }
}

function onClickSettingsSeg() {
    tempSettingsFlag = 1;
    var userSettingsData = kony.store.getItem("settingsflagsObject");
    var selectedSections = 0;
    switch (selectedSections) {
        case 0:
            gotoLoginSettings();
            break;
        case 1:
            gotoProfileSettings();
            break;
        case 2:
            gotoAccountSettings();
            break;
    }
}

function gotoLoginSettings() {
    var selectedRows = 1;
    switch (selectedRows) {
        case 1:
            gotoDevRegistration();
            break;
        case 2:
            // frmDefaultLoginMethodKA.show();
            break;
        case 3:
            gotoPinSettings();
            break;
        case 4:
            gotoFaceIdSettings();
            break;
        case 5:
            if (hasTouchId) frmUserSettingsTouchIdKA.show();
            break;
    }
}

function gotoDevRegistration() {
    var settingsData = kony.store.getItem("settingsflagsObject");
    if (settingsData.deviceRegisterFlag !== null && settingsData.deviceRegisterFlag === false) {
        deviceRegFrom = "settings";
        frmDeviceRegistrationOptionsKA.show();
    } else {
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        deviceRegController = INSTANCE.getFormController("frmDeviceDeRegistrationKA");
        deviceRegNavObject = new kony.sdk.mvvm.NavigationObject();
        deviceRegNavObject.setRequestOptions("form", {
            "headers": {
                "session_token": kony.retailBanking.globalData.session_token
            }
        });
        deviceRegController.performAction("loadDataAndShowForm", [deviceRegNavObject]);
    }
}

function gotoProfileSettings() {
    var selectedRows = 0;
    switch (selectedRows) {
        case 0:
            onClickProfile();
            break;
        case 1:
            showAlertsForm();
            break;
    }
}

function onClickProfile() {
    frmUserSettingsMyProfileKA.show();
}

function setSettingsData() {
    getSettingsFeaturesStatus();
    data = [
        [{
                "lblHeaderKA": i18n_LoginSettings
            },
            [{
                "lblSettingsNameKA": i18n_AccountPreview,
                "lblSettingsStatusKA": accountsPreviewStatus,
                "imgProgressKey": "right_chevron_icon.png"
            }, {
                "lblSettingsNameKA": i18n_devRegistration,
                "lblSettingsStatusKA": deviceRegStatus,
                "imgProgressKey": "right_chevron_icon.png"
            }, {
                "lblSettingsNameKA": "Default Login",
                "lblSettingsStatusKA": defaultLogin,
                "imgProgressKey": "right_chevron_icon.png"
            }, {
                "lblSettingsNameKA": "PIN Login",
                "lblSettingsStatusKA": isPinEnabled,
                "imgProgressKey": "right_chevron_icon.png"
            }, {
                "lblSettingsNameKA": "Face ID",
                "lblSettingsStatusKA": isFaceIDEnabled,
                "imgProgressKey": "right_chevron_icon.png"
            }, {
                "lblSettingsNameKA": i18n_TouchID,
                "lblSettingsStatusKA": touchIDStatus,
                "imgProgressKey": "right_chevron_icon.png"
            }]
        ],
        [{
                "lblHeaderKA": i18n_ProfilenAlerts
            },
            [{
                "lblSettingsNameKA": i18n_MyProfile,
                "lblSettingsStatusKA": "",
                "imgProgressKey": "right_chevron_icon.png"
            }, {
                "lblSettingsNameKA": i18n_Alerts,
                "lblSettingsStatusKA": alertsFlg,
                "imgProgressKey": "right_chevron_icon.png"
            }]
        ],
        [{
                "lblHeaderKA": i18n_GeneralSettings
            },
            [{
                "lblSettingsNameKA": i18n_ForTransfers,
                "lblSettingsStatusKA": defaultTransferAcc,
                "imgProgressKey": "right_chevron_icon.png"
            }, {
                "lblSettingsNameKA": i18n_ForDeposits,
                "lblSettingsStatusKA": defaultDepositAcc,
                "imgProgressKey": "right_chevron_icon.png"
            }, {
                "lblSettingsNameKA": i18n_ForPayments,
                "lblSettingsStatusKA": defaultPaymentsAcc,
                "imgProgressKey": "right_chevron_icon.png"
            }, {
                "lblSettingsNameKA": i18n_forCardless,
                "lblSettingsStatusKA": defaultCardlessAcc,
                "imgProgressKey": "right_chevron_icon.png"
            }]
        ]
    ];
}

function gotoAccountSettings() {
    getPreferredAccounts();
    var selectedAcntRow = 0;
    switch (selectedAcntRow) {
        case 0:
            accountType = i18n_ForTransfers;
            rowIndex = 0;
            selectedAcntType = "default_account_transfers";
            setDefaultAcntsData(transferAcc);
            break;
        case 1:
            accountType = i18n_ForDeposits;
            rowIndex = 1;
            selectedAcntType = "default_account_deposit";
            setDefaultAcntsData(depositAcc);
            break;
        case 2:
            accountType = i18n_ForPayments;
            rowIndex = 2;
            selectedAcntType = "default_account_payments";
            setDefaultAcntsData(paymentAcc);
            break;
        case 3:
            accountType = i18n_forCardless;
            rowIndex = 3;
            selectedAcntType = "default_account_cardless";
            setDefaultAcntsData(cardlessAcc);
            break;
    }
    frmPreferredAccountsKA.lbltransferHeader.text = accountType;
    frmPreferredAccountsKA.show();
}

function frmFaceAuthEnablePreshow() {
    var previousForm = kony.application.getPreviousForm();
    if (previousForm.id === "frmSetupFaceIdSettingsKA") {
        frmFacialAuthEnable.flxHeaderContainerKA.setVisibility(false);
        frmFacialAuthEnable.flxHeaderText.setVisibility(true);
    } else {
        frmFacialAuthEnable.flxHeaderContainerKA.setVisibility(true);
        frmFacialAuthEnable.flxHeaderText.setVisibility(false);
    }
}