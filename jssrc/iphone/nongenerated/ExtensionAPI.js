kony.sdk.ClassExtensionUtility = {
    constructor: function(arg) {},
    /**
     * This function overrides an existing member behaviour with a new behaviour.
     *
     * @param {class} claz   - The class for which a member has to be updated.
     * @param {JSON}  method - The member which has to be updated in the class.
     */
    updateMember: function(claz, method) {
        if (method === undefined) {
            method = claz;
            for (var key in method) {
                eval(key + "=eval(method[key])");
            }
        } else {
            for (var key in method) {
                if (claz.prototype) {
                    var refClass = claz.prototype;
                }
                if (refClass[key]) {
                    refClass[key] = method[key];
                }
            }
        }
    }
}