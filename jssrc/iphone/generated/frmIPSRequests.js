function addWidgetsfrmIPSRequests() {
    frmIPSRequests.setDefaultUnit(kony.flex.DP);
    var flxRequestToPay = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxRequestToPay",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0",
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    flxRequestToPay.setDefaultUnit(kony.flex.DP);
    var flxRPRHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxRPRHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "skin": "Copys",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true
    }, {});
    flxRPRHeader.setDefaultUnit(kony.flex.DP);
    var lblRTRTitleHeader = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblRTRTitleHeader",
        "isVisible": true,
        "skin": "CopylblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.CLIQ.RequestToPay"),
        "textStyle": {},
        "top": "15dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxRPRNext = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "31dp",
        "id": "flxRPRNext",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_i67dc43b04484b6490db2098bf2cff07,
        "right": "2.04%",
        "skin": "slFbox",
        "top": "11.75%",
        "width": "17.33%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true
    }, {});
    flxRPRNext.setDefaultUnit(kony.flex.DP);
    var lblRPRNext = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Next Page"
        },
        "centerY": "50%",
        "height": "100%",
        "id": "lblRPRNext",
        "isVisible": true,
        "left": "0dp",
        "skin": "CopysknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.login.next"),
        "textStyle": {},
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxRPRNext.add(lblRPRNext);
    var flxRPRBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxRPRBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "20%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true,
        "retainFlowHorizontalAlignment": true
    }, {});
    flxRPRBack.setDefaultUnit(kony.flex.DP);
    var lblRPRBAckIcn = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Back to"
        },
        "centerY": "50%",
        "id": "lblRPRBAckIcn",
        "isVisible": true,
        "left": "0dp",
        "skin": "CopysknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {},
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblRPRBAck = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Login Screen"
        },
        "centerY": "50%",
        "id": "lblRPRBAck",
        "isVisible": true,
        "left": "0dp",
        "skin": "CopysknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {},
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxRPRBack.add(lblRPRBAckIcn, lblRPRBAck);
    flxRPRHeader.add(lblRTRTitleHeader, flxRPRNext, flxRPRBack);
    var flxRPRBody = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "91%",
        "id": "flxRPRBody",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxRPRBody.setDefaultUnit(kony.flex.DP);
    var flxRPRAccounFrom = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "20%",
        "id": "flxRPRAccounFrom",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    flxRPRAccounFrom.setDefaultUnit(kony.flex.DP);
    var lblRPRFrom = new kony.ui.Label({
        "height": "35%",
        "id": "lblRPRFrom",
        "isVisible": true,
        "left": "20dp",
        "skin": "CopysknLblWhike",
        "text": kony.i18n.getLocalizedString("i18n.cashWithdraw.from"),
        "textStyle": {},
        "top": "-5%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblRPRSplitACC = new kony.ui.Label({
        "height": "1dp",
        "id": "lblRPRSplitACC",
        "isVisible": true,
        "left": "0dp",
        "skin": "CopylblsknToandFromAccLine",
        "textStyle": {},
        "top": "0dp",
        "width": "100%",
        "zIndex": 10
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxRPRAcc = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70%",
        "id": "flxRPRAcc",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "-0.09%",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0.00%",
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    flxRPRAcc.setDefaultUnit(kony.flex.DP);
    var flxRPRIcon = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "45dp",
        "id": "flxRPRIcon",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "6%",
        "isModalContainer": false,
        "skin": "CopyCopysknFlxToIcon",
        "top": "10%",
        "width": "45dp",
        "zIndex": 100
    }, {}, {});
    flxRPRIcon.setDefaultUnit(kony.flex.DP);
    var lblIcon0 = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "80%",
        "id": "lblIcon0",
        "isVisible": true,
        "left": "19dp",
        "skin": "CopysknLblFromIcon",
        "text": "BH",
        "textStyle": {},
        "top": "13dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxRPRIcon.add(lblIcon0);
    var lblRPRAccountName = new kony.ui.Label({
        "bottom": "0%",
        "height": "40%",
        "id": "lblRPRAccountName",
        "isVisible": true,
        "left": "21%",
        "skin": "CopysknLblAccNum",
        "textStyle": {},
        "top": "10%",
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblRPRAccountNumber = new kony.ui.Label({
        "height": "35%",
        "id": "lblRPRAccountNumber",
        "isVisible": true,
        "left": "21%",
        "skin": "CopysknSmallForIban",
        "textStyle": {},
        "top": "50%",
        "width": "72%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnRPRForward = new kony.ui.Button({
        "focusSkin": "CopysknBtnForwardDimmed",
        "height": "100%",
        "id": "btnRPRForward",
        "isVisible": true,
        "left": "5%",
        "onClick": AS_Button_fadfabddb5ed4cd594865aca819d02c6,
        "right": 0,
        "skin": "CopysknBtnForwardDimmed",
        "text": kony.i18n.getLocalizedString("i18n.common.reverseback"),
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    flxRPRAcc.add(flxRPRIcon, lblRPRAccountName, lblRPRAccountNumber, btnRPRForward);
    var lblRPRSelectanAccount = new kony.ui.Label({
        "centerY": "-35%",
        "id": "lblRPRSelectanAccount",
        "isVisible": true,
        "left": "8%",
        "skin": "CopysknLblWhike",
        "text": kony.i18n.getLocalizedString("i18n.Transfer.selectAcc"),
        "textStyle": {},
        "top": "19dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblRPRFromAccCurr = new kony.ui.Label({
        "id": "lblRPRFromAccCurr",
        "isVisible": true,
        "left": "0dp",
        "skin": "CopysknTrans",
        "textStyle": {},
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxRPRAccounFrom.add(lblRPRFrom, lblRPRSplitACC, flxRPRAcc, lblRPRSelectanAccount, lblRPRFromAccCurr);
    var lblrprSplit = new kony.ui.Label({
        "height": "1dp",
        "id": "lblrprSplit",
        "isVisible": true,
        "left": "0dp",
        "skin": "CopylblsknToandFromAccLine",
        "textStyle": {},
        "top": "0dp",
        "width": "100%",
        "zIndex": 10
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxRPRDetailsIBAN = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "flxRPRDetailsIBAN",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxRPRDetailsIBAN.setDefaultUnit(kony.flex.DP);
    var flxIRPRbanDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": false,
        "height": "50dp",
        "id": "flxIRPRbanDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxIRPRbanDetails.setDefaultUnit(kony.flex.DP);
    var lblRPRIBANAlias = new kony.ui.Label({
        "id": "lblRPRIBANAlias",
        "isVisible": true,
        "left": "2%",
        "skin": "CopysknLblAccNumBiller",
        "text": kony.i18n.getLocalizedString("i18.CLIQ.IBAN"),
        "textStyle": {},
        "top": "15%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblRPRIBAN = new kony.ui.Label({
        "id": "lblRPRIBAN",
        "isVisible": true,
        "left": "37%",
        "skin": "CopysknLblNextDisabled",
        "text": "JO1234433",
        "textStyle": {},
        "top": "10%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxIRPRbanDetails.add(lblRPRIBANAlias, lblRPRIBAN);
    var flxRPRAmountIBAN = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": false,
        "height": "50dp",
        "id": "flxRPRAmountIBAN",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxRPRAmountIBAN.setDefaultUnit(kony.flex.DP);
    var lblRPRAmount = new kony.ui.Label({
        "id": "lblRPRAmount",
        "isVisible": true,
        "left": "2.00%",
        "skin": "CopysknLblAccNumBiller",
        "text": kony.i18n.getLocalizedString("i18n.loans.paymentAmount"),
        "textStyle": {},
        "top": 0,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 10
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblRPRCurrCode = new kony.ui.Label({
        "id": "lblRPRCurrCode",
        "isVisible": true,
        "left": "88%",
        "skin": "CopylblAccountStaticText",
        "text": "JOD",
        "textStyle": {},
        "top": "10%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 5
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblRPRAmountToPay = new kony.ui.Label({
        "id": "lblRPRAmountToPay",
        "isVisible": true,
        "left": "37%",
        "skin": "CopysknLblNextDisabled",
        "text": "10",
        "textStyle": {},
        "top": "10%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxRPRAmountIBAN.add(lblRPRAmount, lblRPRCurrCode, lblRPRAmountToPay);
    flxRPRDetailsIBAN.add(flxIRPRbanDetails, flxRPRAmountIBAN);
    var flxRPRRequestReason = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "51%",
        "clipBounds": true,
        "height": "50dp",
        "id": "flxRPRRequestReason",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxRPRRequestReason.setDefaultUnit(kony.flex.DP);
    var lblRPRReason = new kony.ui.Label({
        "id": "lblRPRReason",
        "isVisible": true,
        "left": "2%",
        "maxNumberOfLines": 1,
        "skin": "CopysknLblAccNumBiller",
        "text": kony.i18n.getLocalizedString("i18n.CLIQ.Reason"),
        "textStyle": {},
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": 0,
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblRPRReasonToPay = new kony.ui.Label({
        "id": "lblRPRReasonToPay",
        "isVisible": true,
        "left": "37%",
        "skin": "CopysknLblNextDisabled",
        "text": "10",
        "textStyle": {},
        "top": "10%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxRPRRequestReason.add(lblRPRReason, lblRPRReasonToPay);
    var btnReject = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "15%",
        "focusSkin": "CopyslButtonWhiteFocus",
        "height": "9%",
        "id": "btnReject",
        "isVisible": true,
        "onClick": AS_Button_b2f79ee87e8c408598d3550bb626ac57,
        "skin": "CopyslButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.CLIQ.Reject"),
        "width": "75%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var CopybtnReject0bd54b13e669d47 = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "7%",
        "focusSkin": "CopyslButtonWhiteFocus",
        "height": "9%",
        "id": "CopybtnReject0bd54b13e669d47",
        "isVisible": true,
        "onClick": AS_Button_e22825f55b8a49f98d32b146fe8aabaf,
        "skin": "CopyslButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.CLIQ.Approve"),
        "width": "75%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    flxRPRBody.add(flxRPRAccounFrom, lblrprSplit, flxRPRDetailsIBAN, flxRPRRequestReason, btnReject, CopybtnReject0bd54b13e669d47);
    flxRequestToPay.add(flxRPRHeader, flxRPRBody);
    var flxReturnPayment = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "99%",
        "id": "flxReturnPayment",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0",
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    flxReturnPayment.setDefaultUnit(kony.flex.DP);
    var flxRequestPaymentHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxRequestPaymentHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "skin": "Copys",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true
    }, {});
    flxRequestPaymentHeader.setDefaultUnit(kony.flex.DP);
    var lblRPTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblRPTitle",
        "isVisible": true,
        "skin": "CopylblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.CLIQ.ReturnPayment"),
        "textStyle": {},
        "top": "15dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxNextRP = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "31dp",
        "id": "flxNextRP",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_f9dd2a4fa0424d289d5610b6e2c54657,
        "right": "2.04%",
        "skin": "slFbox",
        "top": "11.75%",
        "width": "17.33%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true
    }, {});
    flxNextRP.setDefaultUnit(kony.flex.DP);
    var lblNextRP = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Next Page"
        },
        "centerY": "50.00%",
        "height": "100%",
        "id": "lblNextRP",
        "isVisible": true,
        "left": "0dp",
        "skin": "CopysknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.login.next"),
        "textStyle": {},
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxNextRP.add(lblNextRP);
    var flxRPBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxRPBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_a5e271f14bcc439e9a72cde792ebd091,
        "skin": "slFbox",
        "top": "0%",
        "width": "20%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true,
        "retainFlowHorizontalAlignment": true
    }, {});
    flxRPBack.setDefaultUnit(kony.flex.DP);
    var CopylblBackIcon0if0488d921b146 = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Back to"
        },
        "centerY": "50%",
        "id": "CopylblBackIcon0if0488d921b146",
        "isVisible": true,
        "left": "0dp",
        "skin": "CopysknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {},
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopylblBack0b50c4d5ec5694d = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Login Screen"
        },
        "centerY": "50%",
        "id": "CopylblBack0b50c4d5ec5694d",
        "isVisible": true,
        "left": "0dp",
        "skin": "CopysknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {},
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxRPBack.add(CopylblBackIcon0if0488d921b146, CopylblBack0b50c4d5ec5694d);
    flxRequestPaymentHeader.add(lblRPTitle, flxNextRP, flxRPBack);
    var flxReturnPaymentBody = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "67%",
        "id": "flxReturnPaymentBody",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "isModalContainer": false,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxReturnPaymentBody.setDefaultUnit(kony.flex.DP);
    var flxRef = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": false,
        "height": "70dp",
        "id": "flxRef",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": 0,
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxRef.setDefaultUnit(kony.flex.DP);
    var lblRefTitleRP = new kony.ui.Label({
        "id": "lblRefTitleRP",
        "isVisible": true,
        "left": "2%",
        "skin": "CopysknLblAccNumBiller",
        "text": kony.i18n.getLocalizedString("i18n.common.ReferenceNumber"),
        "textStyle": {},
        "top": "15%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxrRPUnderlineRef = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxrRPUnderlineRef",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "CopysknFlxGreyLine",
        "top": "94%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxrRPUnderlineRef.setDefaultUnit(kony.flex.DP);
    flxrRPUnderlineRef.add();
    var lblReferenceRPR = new kony.ui.Label({
        "bottom": "4%",
        "height": "50%",
        "id": "lblReferenceRPR",
        "isVisible": true,
        "left": "2%",
        "skin": "CopysknLblNextDisabled",
        "textStyle": {},
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxRef.add(lblRefTitleRP, flxrRPUnderlineRef, lblReferenceRPR);
    var flxAliasRPR = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": false,
        "height": "70dp",
        "id": "flxAliasRPR",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": 0,
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxAliasRPR.setDefaultUnit(kony.flex.DP);
    var lblAliasRPRTitle = new kony.ui.Label({
        "id": "lblAliasRPRTitle",
        "isVisible": true,
        "left": "2%",
        "skin": "CopysknLblAccNumBiller",
        "text": kony.i18n.getLocalizedString("i18n.CliQ.ALias"),
        "textStyle": {},
        "top": "15%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxrRPUnderlineAlias = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxrRPUnderlineAlias",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "CopysknFlxGreyLine",
        "top": "94%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxrRPUnderlineAlias.setDefaultUnit(kony.flex.DP);
    flxrRPUnderlineAlias.add();
    var lblAliasRP = new kony.ui.Label({
        "bottom": "4%",
        "height": "50%",
        "id": "lblAliasRP",
        "isVisible": true,
        "left": "2%",
        "skin": "CopysknLblNextDisabled",
        "textStyle": {},
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxAliasRPR.add(lblAliasRPRTitle, flxrRPUnderlineAlias, lblAliasRP);
    var flxRPIbanDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": false,
        "height": "70dp",
        "id": "flxRPIbanDetails",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": 0,
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxRPIbanDetails.setDefaultUnit(kony.flex.DP);
    var lblIBANTitleRP = new kony.ui.Label({
        "id": "lblIBANTitleRP",
        "isVisible": true,
        "left": "2%",
        "skin": "CopysknLblAccNumBiller",
        "text": kony.i18n.getLocalizedString("i18.CLIQ.IBAN"),
        "textStyle": {},
        "top": "15%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxrRPUnderlineDate = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxrRPUnderlineDate",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "CopysknFlxGreyLine",
        "top": "94%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxrRPUnderlineDate.setDefaultUnit(kony.flex.DP);
    flxrRPUnderlineDate.add();
    var lblIBANRP = new kony.ui.Label({
        "bottom": "4%",
        "height": "50%",
        "id": "lblIBANRP",
        "isVisible": true,
        "left": "2%",
        "skin": "CopysknLblNextDisabled",
        "textStyle": {},
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxRPIbanDetails.add(lblIBANTitleRP, flxrRPUnderlineDate, lblIBANRP);
    var flxTransferDate = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": false,
        "height": "70dp",
        "id": "flxTransferDate",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxTransferDate.setDefaultUnit(kony.flex.DP);
    var lblTransferDateTitle = new kony.ui.Label({
        "id": "lblTransferDateTitle",
        "isVisible": true,
        "left": "2%",
        "skin": "CopysknLblAccNumBiller",
        "text": kony.i18n.getLocalizedString("i18n.common.transactiondate"),
        "textStyle": {},
        "top": "15%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxUnderliineTrxDate = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderliineTrxDate",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "CopysknFlxGreyLine",
        "top": "94%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxUnderliineTrxDate.setDefaultUnit(kony.flex.DP);
    flxUnderliineTrxDate.add();
    var lblRPTransactionDate = new kony.ui.Label({
        "bottom": "4%",
        "height": "50%",
        "id": "lblRPTransactionDate",
        "isVisible": true,
        "left": "2%",
        "skin": "CopysknLblNextDisabled",
        "textStyle": {},
        "width": "100%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxTransferDate.add(lblTransferDateTitle, flxUnderliineTrxDate, lblRPTransactionDate);
    var flxRPAmount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": false,
        "height": "70dp",
        "id": "flxRPAmount",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxRPAmount.setDefaultUnit(kony.flex.DP);
    var lblAmountRP = new kony.ui.Label({
        "id": "lblAmountRP",
        "isVisible": true,
        "left": "2%",
        "skin": "lblAccountStaticText",
        "text": kony.i18n.getLocalizedString("i18n.accounts.amount"),
        "textStyle": {},
        "top": "15%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 10
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblCurrCodeRP = new kony.ui.Label({
        "centerY": "70%",
        "id": "lblCurrCodeRP",
        "isVisible": true,
        "left": "88%",
        "skin": "CopylblAccountStaticText",
        "text": "JOD",
        "textStyle": {},
        "top": "42%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 5
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxUnderlineAmountRP = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderlineAmountRP",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "CopysknFlxGreyLine",
        "top": "95.30%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxUnderlineAmountRP.setDefaultUnit(kony.flex.DP);
    flxUnderlineAmountRP.add();
    var txtAmountRP = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "4%",
        "focusSkin": "CopysknTxtBox",
        "height": "50%",
        "id": "txtAmountRP",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DECIMAL,
        "left": "0%",
        "onDone": AS_TextField_b9007188ec284e4e9bd0284d2a401e45,
        "onTextChange": AS_TextField_d8bf3f576c664f3db4f78a43e163637e,
        "placeholder": "0.000",
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var txtAmountRPOriginal = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "bottom": "-6%",
        "focusSkin": "CopysknTxtBox",
        "height": "50%",
        "id": "txtAmountRPOriginal",
        "isVisible": false,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DECIMAL,
        "left": "10%",
        "onDone": AS_TextField_daa0a20d476643679e5be84812be43e4,
        "onTextChange": AS_TextField_a14225524b8b455f991bc6c0b1e4426b,
        "placeholder": "0.000",
        "secureTextEntry": false,
        "skin": "sknTxtBox",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "width": "100%",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoCorrect": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_NEXT,
        "showClearButton": true,
        "showCloseButton": true,
        "showProgressIndicator": true,
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    flxRPAmount.add(lblAmountRP, lblCurrCodeRP, flxUnderlineAmountRP, txtAmountRP, txtAmountRPOriginal);
    var flxRPRequestReason = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "70dp",
        "id": "flxRPRequestReason",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0",
        "width": "90%",
        "zIndex": 1
    }, {}, {});
    flxRPRequestReason.setDefaultUnit(kony.flex.DP);
    var lblReasonStatRP = new kony.ui.Label({
        "id": "lblReasonStatRP",
        "isVisible": false,
        "left": "2%",
        "maxNumberOfLines": 1,
        "skin": "lblAccountStaticText",
        "text": kony.i18n.getLocalizedString("i18n.CLIQ.Reason"),
        "textStyle": {},
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "14%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var tbxReasonRP = new kony.ui.Label({
        "bottom": "4%",
        "centerX": "50%",
        "height": "60%",
        "id": "tbxReasonRP",
        "isVisible": true,
        "skin": "CopysknlblWhitecarioRegular",
        "textStyle": {},
        "width": "100%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxUnderlineReasonRP = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "2%",
        "id": "flxUnderlineReasonRP",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "CopysknFlxGreyLine",
        "top": "95%",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxUnderlineReasonRP.setDefaultUnit(kony.flex.DP);
    flxUnderlineReasonRP.add();
    var flxReasonInsideRP = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "7%",
        "centerX": "49%",
        "clipBounds": true,
        "height": "60%",
        "id": "flxReasonInsideRP",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_gf50d2155e164e8db41593924a353d52,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxReasonInsideRP.setDefaultUnit(kony.flex.DP);
    var lblReasonRP = new kony.ui.Label({
        "centerY": "50%",
        "height": "100%",
        "id": "lblReasonRP",
        "isVisible": true,
        "left": "3%",
        "maxNumberOfLines": 1,
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.CLIQ.Reason"),
        "textStyle": {},
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "90%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblArrowReasonRP = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblArrowReasonRP",
        "isVisible": true,
        "left": "93%",
        "skin": "CopysknBackIconDisabled",
        "text": kony.i18n.getLocalizedString("i18n.common.reverseback"),
        "textStyle": {},
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxReasonInsideRP.add(lblReasonRP, lblArrowReasonRP);
    flxRPRequestReason.add(lblReasonStatRP, tbxReasonRP, flxUnderlineReasonRP, flxReasonInsideRP);
    flxReturnPaymentBody.add(flxRef, flxAliasRPR, flxRPIbanDetails, flxTransferDate, flxRPAmount, flxRPRequestReason);
    flxReturnPayment.add(flxRequestPaymentHeader, flxReturnPaymentBody);
    var flxPaymentHistory = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxPaymentHistory",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": 0,
        "isModalContainer": false,
        "skin": "CopyslFbox0j647f88e757942",
        "top": "0dp",
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    flxPaymentHistory.setDefaultUnit(kony.flex.DP);
    var flxHdrPH = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "23%",
        "id": "flxHdrPH",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "masterType": constants.MASTER_TYPE_USERWIDGET,
        "isModalContainer": false,
        "skin": "Copys",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true,
        "retainFlowHorizontalAlignment": false
    }, {});
    flxHdrPH.setDefaultUnit(kony.flex.DP);
    var flxHdrMain = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "35%",
        "id": "flxHdrMain",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": 0,
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxHdrMain.setDefaultUnit(kony.flex.DP);
    var lblHistoryHeader = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblHistoryHeader",
        "isVisible": true,
        "skin": "CopylblAmountCurrency",
        "text": kony.i18n.getLocalizedString("i18n.CLIQ.PaymentHistory"),
        "textStyle": {},
        "top": "15dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var flxNextHistory = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "31dp",
        "id": "flxNextHistory",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "onClick": AS_FlexContainer_a3643f46ad6e42cba56cd990ac96779c,
        "right": "2.04%",
        "skin": "slFbox",
        "top": "11.75%",
        "width": "17.33%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true
    }, {});
    flxNextHistory.setDefaultUnit(kony.flex.DP);
    var lblNextHistory = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Next Page"
        },
        "centerY": "50%",
        "height": "100%",
        "id": "lblNextHistory",
        "isVisible": false,
        "left": "0dp",
        "skin": "CopysknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.login.next"),
        "textStyle": {},
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxNextHistory.add(lblNextHistory);
    var flxBackHistory = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxBackHistory",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_b07c48a28eeb4c1e89635dc3eaa69155,
        "skin": "slFbox",
        "top": "0%",
        "width": "20%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true,
        "retainFlowHorizontalAlignment": true
    }, {});
    flxBackHistory.setDefaultUnit(kony.flex.DP);
    var CopylblBackIcon0bed99bb0a62d4b = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Back to"
        },
        "centerY": "50%",
        "id": "CopylblBackIcon0bed99bb0a62d4b",
        "isVisible": true,
        "left": "0dp",
        "skin": "CopysknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {},
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopylblBack0j735e1c57c4645 = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yLabel": "Login Screen"
        },
        "centerY": "50.12%",
        "id": "CopylblBack0j735e1c57c4645",
        "isVisible": true,
        "left": "0dp",
        "skin": "CopysknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {},
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxBackHistory.add(CopylblBackIcon0bed99bb0a62d4b, CopylblBack0j735e1c57c4645);
    flxHdrMain.add(lblHistoryHeader, flxNextHistory, flxBackHistory);
    var flxTab = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "28%",
        "id": "flxTab",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxTab.setDefaultUnit(kony.flex.DP);
    var flxContent = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "65%",
        "id": "flxContent",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "CopyslFboxOuterRing",
        "top": "0dp",
        "width": "96%",
        "zIndex": 1
    }, {}, {});
    flxContent.setDefaultUnit(kony.flex.DP);
    var btnAll = new kony.ui.Button({
        "centerY": "50.01%",
        "focusSkin": "CopyslButtonWhiteTabFocus",
        "height": "100%",
        "id": "btnAll",
        "isVisible": true,
        "left": "0dp",
        "onClick": AS_Button_c5ba055f0b3c430294bf8ffd11eebe00,
        "skin": "CopyslButtonWhiteTab",
        "text": kony.i18n.getLocalizedString("i18n.Bene.All"),
        "top": "0dp",
        "width": "33%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var btnIncoming = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "CopyslButtonWhiteTabFocus",
        "height": "100%",
        "id": "btnIncoming",
        "isVisible": true,
        "left": "33%",
        "onClick": AS_Button_h7cb1349585c4927ae00890163092a84,
        "skin": "CopyslButtonWhiteTabDisabled",
        "text": kony.i18n.getLocalizedString("i18n.CLIQ.Incoming"),
        "top": "0dp",
        "width": "32%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var btnOutgoing = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "CopyslButtonWhiteTabFocus",
        "height": "98%",
        "id": "btnOutgoing",
        "isVisible": true,
        "left": "65%",
        "onClick": AS_Button_aa531eb1a81a4f639fb8acfc78046e57,
        "skin": "CopyslButtonWhiteTabDisabled",
        "text": kony.i18n.getLocalizedString("i18n.CLIQ.Outgoing"),
        "width": "34%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var btnRequestToPay = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "CopyslButtonWhiteTabFocus",
        "height": "100%",
        "id": "btnRequestToPay",
        "isVisible": false,
        "left": "69%",
        "onClick": AS_Button_c7c5c2d47d1c4ef78db8777e0b657b56,
        "skin": "CopyslButtonWhiteTabDisabled",
        "text": kony.i18n.getLocalizedString("i18n.CLIQ.RequestToPay"),
        "top": "0dp",
        "width": "30%",
        "zIndex": 2
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    flxContent.add(btnAll, btnIncoming, btnOutgoing, btnRequestToPay);
    flxTab.add(flxContent);
    flxHdrPH.add(flxHdrMain, flxTab);
    var flxIPSHistoryBody = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "77%",
        "horizontalScrollIndicator": true,
        "id": "flxIPSHistoryBody",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0%",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "slFSbox",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    flxIPSHistoryBody.setDefaultUnit(kony.flex.DP);
    var flxNote = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxNote",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxNote.setDefaultUnit(kony.flex.DP);
    var lblNotice = new kony.ui.Label({
        "id": "lblNotice",
        "isVisible": true,
        "left": "3%",
        "skin": "lblorangeSKN",
        "text": kony.i18n.getLocalizedString("i18n.CLIQ.ReturnNotice"),
        "textStyle": {},
        "top": 0,
        "width": "94%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxNote.add(lblNotice);
    var segIPSHistory = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "data": [{
            "AliasNamei": "$10.00",
            "AliasNameo": "ALias",
            "btnCreditConfirm": "Button",
            "btnReturnPayment": kony.i18n.getLocalizedString("i18n.CLIQ.ReturnPayment"),
            "lblAliasi": kony.i18n.getLocalizedString("i18n.accounts.amount"),
            "lblAliaso": kony.i18n.getLocalizedString("i18n.CliQ.ALias"),
            "lblAmti": kony.i18n.getLocalizedString("i18n.accounts.amount"),
            "lblAmto": kony.i18n.getLocalizedString("i18n.accounts.amount"),
            "lblCreditAccountValue": "Ref",
            "lblCredited": kony.i18n.getLocalizedString("i18n.common.ReferenceNumber"),
            "lblFAcco": kony.i18n.getLocalizedString("i18n.CLIQ.Reason"),
            "lblFromAccount": "Label",
            "lblIBANi": "JOARAB76544567887323",
            "lblIBANo": "Label",
            "lblIBAnDescO": kony.i18n.getLocalizedString("i18.CLIQ.IBAN"),
            "lblIBAnDesci": "Label",
            "lblNoData": kony.i18n.getLocalizedString("i18n.accounts.noTransaction"),
            "lblRefi": kony.i18n.getLocalizedString("i18n.jomopay.transactionRef"),
            "lblRefo": kony.i18n.getLocalizedString("i18n.jomopay.transactionRef"),
            "lblTransType": kony.i18n.getLocalizedString("i18n.FilterTransaction.transactiontype"),
            "lblTrxDatei": kony.i18n.getLocalizedString("i18n.common.transactiondate"),
            "lblTrxDateo": kony.i18n.getLocalizedString("i18n.common.transactiondate"),
            "lblTrxRefi": "Label",
            "lblTrxRefo": "Ref",
            "transactionAmounti": "10",
            "transactionAmounto": "$10.00",
            "transactionDatei": "15/12/2020",
            "transactionDateo": "10/10/2098"
        }, {
            "AliasNamei": "$10.00",
            "AliasNameo": "ALias",
            "btnCreditConfirm": "Button",
            "btnReturnPayment": kony.i18n.getLocalizedString("i18n.CLIQ.ReturnPayment"),
            "lblAliasi": kony.i18n.getLocalizedString("i18n.accounts.amount"),
            "lblAliaso": kony.i18n.getLocalizedString("i18n.CliQ.ALias"),
            "lblAmti": kony.i18n.getLocalizedString("i18n.accounts.amount"),
            "lblAmto": kony.i18n.getLocalizedString("i18n.accounts.amount"),
            "lblCreditAccountValue": "Ref",
            "lblCredited": kony.i18n.getLocalizedString("i18n.common.ReferenceNumber"),
            "lblFAcco": kony.i18n.getLocalizedString("i18n.CLIQ.Reason"),
            "lblFromAccount": "Label",
            "lblIBANi": "JOUBI56789876567890",
            "lblIBANo": "Label",
            "lblIBAnDescO": kony.i18n.getLocalizedString("i18.CLIQ.IBAN"),
            "lblIBAnDesci": "Label",
            "lblNoData": kony.i18n.getLocalizedString("i18n.accounts.noTransaction"),
            "lblRefi": kony.i18n.getLocalizedString("i18n.jomopay.transactionRef"),
            "lblRefo": kony.i18n.getLocalizedString("i18n.jomopay.transactionRef"),
            "lblTransType": kony.i18n.getLocalizedString("i18n.FilterTransaction.transactiontype"),
            "lblTrxDatei": kony.i18n.getLocalizedString("i18n.common.transactiondate"),
            "lblTrxDateo": kony.i18n.getLocalizedString("i18n.common.transactiondate"),
            "lblTrxRefi": "Label",
            "lblTrxRefo": "Ref",
            "transactionAmounti": "5",
            "transactionAmounto": "$10.00",
            "transactionDatei": "5/12/2020",
            "transactionDateo": "10/10/2098"
        }, {
            "AliasNamei": "$10.00",
            "AliasNameo": "ALias",
            "btnCreditConfirm": "Button",
            "btnReturnPayment": kony.i18n.getLocalizedString("i18n.CLIQ.ReturnPayment"),
            "lblAliasi": kony.i18n.getLocalizedString("i18n.accounts.amount"),
            "lblAliaso": kony.i18n.getLocalizedString("i18n.CliQ.ALias"),
            "lblAmti": kony.i18n.getLocalizedString("i18n.accounts.amount"),
            "lblAmto": kony.i18n.getLocalizedString("i18n.accounts.amount"),
            "lblCreditAccountValue": "Ref",
            "lblCredited": kony.i18n.getLocalizedString("i18n.common.ReferenceNumber"),
            "lblFAcco": kony.i18n.getLocalizedString("i18n.CLIQ.Reason"),
            "lblFromAccount": "Label",
            "lblIBANi": "JOrty4567890987656789",
            "lblIBANo": "Label",
            "lblIBAnDescO": kony.i18n.getLocalizedString("i18.CLIQ.IBAN"),
            "lblIBAnDesci": "Label",
            "lblNoData": kony.i18n.getLocalizedString("i18n.accounts.noTransaction"),
            "lblRefi": kony.i18n.getLocalizedString("i18n.jomopay.transactionRef"),
            "lblRefo": kony.i18n.getLocalizedString("i18n.jomopay.transactionRef"),
            "lblTransType": kony.i18n.getLocalizedString("i18n.FilterTransaction.transactiontype"),
            "lblTrxDatei": kony.i18n.getLocalizedString("i18n.common.transactiondate"),
            "lblTrxDateo": kony.i18n.getLocalizedString("i18n.common.transactiondate"),
            "lblTrxRefi": "Label",
            "lblTrxRefo": "Ref",
            "transactionAmounti": "6",
            "transactionAmounto": "$10.00",
            "transactionDatei": "10/12/2020",
            "transactionDateo": "10/10/2098"
        }, {
            "AliasNamei": "$10.00",
            "AliasNameo": "ALias",
            "btnCreditConfirm": "Button",
            "btnReturnPayment": kony.i18n.getLocalizedString("i18n.CLIQ.ReturnPayment"),
            "lblAliasi": kony.i18n.getLocalizedString("i18n.accounts.amount"),
            "lblAliaso": kony.i18n.getLocalizedString("i18n.CliQ.ALias"),
            "lblAmti": kony.i18n.getLocalizedString("i18n.accounts.amount"),
            "lblAmto": kony.i18n.getLocalizedString("i18n.accounts.amount"),
            "lblCreditAccountValue": "Ref",
            "lblCredited": kony.i18n.getLocalizedString("i18n.common.ReferenceNumber"),
            "lblFAcco": kony.i18n.getLocalizedString("i18n.CLIQ.Reason"),
            "lblFromAccount": "Label",
            "lblIBANi": "JOwer3456789098765323",
            "lblIBANo": "Label",
            "lblIBAnDescO": kony.i18n.getLocalizedString("i18.CLIQ.IBAN"),
            "lblIBAnDesci": "Label",
            "lblNoData": kony.i18n.getLocalizedString("i18n.accounts.noTransaction"),
            "lblRefi": kony.i18n.getLocalizedString("i18n.jomopay.transactionRef"),
            "lblRefo": kony.i18n.getLocalizedString("i18n.jomopay.transactionRef"),
            "lblTransType": kony.i18n.getLocalizedString("i18n.FilterTransaction.transactiontype"),
            "lblTrxDatei": kony.i18n.getLocalizedString("i18n.common.transactiondate"),
            "lblTrxDateo": kony.i18n.getLocalizedString("i18n.common.transactiondate"),
            "lblTrxRefi": "Label",
            "lblTrxRefo": "Ref",
            "transactionAmounti": "7",
            "transactionAmounto": "$10.00",
            "transactionDatei": "02/12/2020",
            "transactionDateo": "10/10/2098"
        }, {
            "AliasNamei": "$10.00",
            "AliasNameo": "ALias",
            "btnCreditConfirm": "Button",
            "btnReturnPayment": kony.i18n.getLocalizedString("i18n.CLIQ.ReturnPayment"),
            "lblAliasi": kony.i18n.getLocalizedString("i18n.accounts.amount"),
            "lblAliaso": kony.i18n.getLocalizedString("i18n.CliQ.ALias"),
            "lblAmti": kony.i18n.getLocalizedString("i18n.accounts.amount"),
            "lblAmto": kony.i18n.getLocalizedString("i18n.accounts.amount"),
            "lblCreditAccountValue": "Ref",
            "lblCredited": kony.i18n.getLocalizedString("i18n.common.ReferenceNumber"),
            "lblFAcco": kony.i18n.getLocalizedString("i18n.CLIQ.Reason"),
            "lblFromAccount": "Label",
            "lblIBANi": "BH4567876567876567",
            "lblIBANo": "Label",
            "lblIBAnDescO": kony.i18n.getLocalizedString("i18.CLIQ.IBAN"),
            "lblIBAnDesci": "Label",
            "lblNoData": kony.i18n.getLocalizedString("i18n.accounts.noTransaction"),
            "lblRefi": kony.i18n.getLocalizedString("i18n.jomopay.transactionRef"),
            "lblRefo": kony.i18n.getLocalizedString("i18n.jomopay.transactionRef"),
            "lblTransType": kony.i18n.getLocalizedString("i18n.FilterTransaction.transactiontype"),
            "lblTrxDatei": kony.i18n.getLocalizedString("i18n.common.transactiondate"),
            "lblTrxDateo": kony.i18n.getLocalizedString("i18n.common.transactiondate"),
            "lblTrxRefi": "Label",
            "lblTrxRefo": "Ref",
            "transactionAmounti": "8",
            "transactionAmounto": "$10.00",
            "transactionDatei": "01/12/2020",
            "transactionDateo": "10/10/2098"
        }, {
            "AliasNamei": "$10.00",
            "AliasNameo": "ALias",
            "btnCreditConfirm": "Button",
            "btnReturnPayment": kony.i18n.getLocalizedString("i18n.CLIQ.ReturnPayment"),
            "lblAliasi": kony.i18n.getLocalizedString("i18n.accounts.amount"),
            "lblAliaso": kony.i18n.getLocalizedString("i18n.CliQ.ALias"),
            "lblAmti": kony.i18n.getLocalizedString("i18n.accounts.amount"),
            "lblAmto": kony.i18n.getLocalizedString("i18n.accounts.amount"),
            "lblCreditAccountValue": "Ref",
            "lblCredited": kony.i18n.getLocalizedString("i18n.common.ReferenceNumber"),
            "lblFAcco": kony.i18n.getLocalizedString("i18n.CLIQ.Reason"),
            "lblFromAccount": "Label",
            "lblIBANi": "JO4567890976543456789",
            "lblIBANo": "Label",
            "lblIBAnDescO": kony.i18n.getLocalizedString("i18.CLIQ.IBAN"),
            "lblIBAnDesci": "Label",
            "lblNoData": kony.i18n.getLocalizedString("i18n.accounts.noTransaction"),
            "lblRefi": kony.i18n.getLocalizedString("i18n.jomopay.transactionRef"),
            "lblRefo": kony.i18n.getLocalizedString("i18n.jomopay.transactionRef"),
            "lblTransType": kony.i18n.getLocalizedString("i18n.FilterTransaction.transactiontype"),
            "lblTrxDatei": kony.i18n.getLocalizedString("i18n.common.transactiondate"),
            "lblTrxDateo": kony.i18n.getLocalizedString("i18n.common.transactiondate"),
            "lblTrxRefi": "Label",
            "lblTrxRefo": "Ref",
            "transactionAmounti": "9",
            "transactionAmounto": "$10.00",
            "transactionDatei": "12/10/2020",
            "transactionDateo": "10/10/2098"
        }, {
            "AliasNamei": "$10.00",
            "AliasNameo": "ALias",
            "btnCreditConfirm": "Button",
            "btnReturnPayment": kony.i18n.getLocalizedString("i18n.CLIQ.ReturnPayment"),
            "lblAliasi": kony.i18n.getLocalizedString("i18n.accounts.amount"),
            "lblAliaso": kony.i18n.getLocalizedString("i18n.CliQ.ALias"),
            "lblAmti": kony.i18n.getLocalizedString("i18n.accounts.amount"),
            "lblAmto": kony.i18n.getLocalizedString("i18n.accounts.amount"),
            "lblCreditAccountValue": "Ref",
            "lblCredited": kony.i18n.getLocalizedString("i18n.common.ReferenceNumber"),
            "lblFAcco": kony.i18n.getLocalizedString("i18n.CLIQ.Reason"),
            "lblFromAccount": "Label",
            "lblIBANi": "JOsdf09872349845",
            "lblIBANo": "Label",
            "lblIBAnDescO": kony.i18n.getLocalizedString("i18.CLIQ.IBAN"),
            "lblIBAnDesci": "Label",
            "lblNoData": kony.i18n.getLocalizedString("i18n.accounts.noTransaction"),
            "lblRefi": kony.i18n.getLocalizedString("i18n.jomopay.transactionRef"),
            "lblRefo": kony.i18n.getLocalizedString("i18n.jomopay.transactionRef"),
            "lblTransType": kony.i18n.getLocalizedString("i18n.FilterTransaction.transactiontype"),
            "lblTrxDatei": kony.i18n.getLocalizedString("i18n.common.transactiondate"),
            "lblTrxDateo": kony.i18n.getLocalizedString("i18n.common.transactiondate"),
            "lblTrxRefi": "Label",
            "lblTrxRefo": "Ref",
            "transactionAmounti": "10.5",
            "transactionAmounto": "$10.00",
            "transactionDatei": "03/12/2020",
            "transactionDateo": "10/10/2098"
        }],
        "groupCells": false,
        "height": "100%",
        "id": "segIPSHistory",
        "isVisible": true,
        "left": 0,
        "needPageIndicator": true,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowFocusSkin": "seg2Focus",
        "rowSkin": "CopyslSegSendMoney",
        "rowTemplate": flxPaymentHistoryIncoming,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "64646424",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": 0,
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "AliasNamei": "AliasNamei",
            "AliasNameo": "AliasNameo",
            "CopyflxSpacer0e5be78dde5554d": "CopyflxSpacer0e5be78dde5554d",
            "btnCreditConfirm": "btnCreditConfirm",
            "btnReturnPayment": "btnReturnPayment",
            "flxIncoming": "flxIncoming",
            "flxOutgoing": "flxOutgoing",
            "flxPaymentHistoryIncoming": "flxPaymentHistoryIncoming",
            "flxSpacer": "flxSpacer",
            "lblAliasi": "lblAliasi",
            "lblAliaso": "lblAliaso",
            "lblAmti": "lblAmti",
            "lblAmto": "lblAmto",
            "lblCreditAccountValue": "lblCreditAccountValue",
            "lblCredited": "lblCredited",
            "lblFAcco": "lblFAcco",
            "lblFromAccount": "lblFromAccount",
            "lblIBANi": "lblIBANi",
            "lblIBANo": "lblIBANo",
            "lblIBAnDescO": "lblIBAnDescO",
            "lblIBAnDesci": "lblIBAnDesci",
            "lblNoData": "lblNoData",
            "lblRefi": "lblRefi",
            "lblRefo": "lblRefo",
            "lblTransType": "lblTransType",
            "lblTrxDatei": "lblTrxDatei",
            "lblTrxDateo": "lblTrxDateo",
            "lblTrxRefi": "lblTrxRefi",
            "lblTrxRefo": "lblTrxRefo",
            "transactionAmounti": "transactionAmounti",
            "transactionAmounto": "transactionAmounto",
            "transactionDatei": "transactionDatei",
            "transactionDateo": "transactionDateo"
        },
        "width": "100%",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "bounces": true,
        "editStyle": constants.SEGUI_EDITING_STYLE_NONE,
        "enableDictionary": false,
        "indicator": constants.SEGUI_NONE,
        "progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
        "showProgressIndicator": true
    });
    flxIPSHistoryBody.add(flxNote, segIPSHistory);
    flxPaymentHistory.add(flxHdrPH, flxIPSHistoryBody);
    var flxConfirmEPS = new kony.ui.FlexScrollContainer({
        "allowHorizontalBounce": false,
        "allowVerticalBounce": true,
        "bounces": true,
        "clipBounds": true,
        "enableScrolling": true,
        "height": "100%",
        "horizontalScrollIndicator": true,
        "id": "flxConfirmEPS",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "pagingEnabled": false,
        "scrollDirection": kony.flex.SCROLL_VERTICAL,
        "skin": "IPSCpnfirmSkin",
        "top": "0dp",
        "verticalScrollIndicator": true,
        "width": "100%",
        "zIndex": 10
    }, {}, {});
    flxConfirmEPS.setDefaultUnit(kony.flex.DP);
    var flxConfirmHeader = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "9%",
        "id": "flxConfirmHeader",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFlxHeaderImg",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true
    }, {});
    flxConfirmHeader.setDefaultUnit(kony.flex.DP);
    var flxHeaderBack = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "90%",
        "id": "flxHeaderBack",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "2%",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_ea493460bd8b42eaaa9ec7a0e15eaeef,
        "skin": "slFbox",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {
        "retainFlexPositionProperties": true,
        "retainFlowHorizontalAlignment": true
    }, {});
    flxHeaderBack.setDefaultUnit(kony.flex.DP);
    var lblHeaderBackIcon = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblHeaderBackIcon",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknBackIcon",
        "text": kony.i18n.getLocalizedString("i18n.common.back"),
        "textStyle": {},
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true,
        "retainContentAlignment": true
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblHeaderBack = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblHeaderBack",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.deposit.back"),
        "textStyle": {},
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true,
        "retainContentAlignment": true
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxHeaderBack.add(lblHeaderBackIcon, lblHeaderBack);
    var lblHeaderTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblHeaderTitle",
        "isVisible": true,
        "skin": "sknLblBack",
        "text": kony.i18n.getLocalizedString("i18n.Transfer.ConfirmDet"),
        "textStyle": {},
        "width": "40%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnClose = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "sknDeleteFocus",
        "height": "80%",
        "id": "btnClose",
        "isVisible": true,
        "left": "85%",
        "onClick": AS_Button_d044b43b14ef449a84a6f85703af71f7,
        "skin": "sknBtnBack",
        "text": "O",
        "width": "15%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true,
        "retainContentAlignment": true
    }, {
        "showProgressIndicator": true
    });
    flxConfirmHeader.add(flxHeaderBack, lblHeaderTitle, btnClose);
    var flxImpDetail = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "23%",
        "id": "flxImpDetail",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxImpDetail.setDefaultUnit(kony.flex.DP);
    var CopyflxIcon0bd2e3679c4ec42 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "50dp",
        "id": "CopyflxIcon0bd2e3679c4ec42",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknFlxToIcon",
        "top": "7%",
        "width": "50dp",
        "zIndex": 1
    }, {}, {});
    CopyflxIcon0bd2e3679c4ec42.setDefaultUnit(kony.flex.DP);
    var lblInitial = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "height": "80%",
        "id": "lblInitial",
        "isVisible": true,
        "skin": "sknLblFromIcon",
        "text": "C N",
        "textStyle": {},
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    CopyflxIcon0bd2e3679c4ec42.add(lblInitial);
    var lblCustomerNameConfirmation = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblCustomerNameConfirmation",
        "isVisible": true,
        "skin": "sknBeneTitle",
        "text": "Customer Name",
        "textStyle": {},
        "top": "3%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblBeneAccNum = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblBeneAccNum",
        "isVisible": true,
        "left": "0dp",
        "skin": "sknLblNextDisabled",
        "text": "Account number 7705152",
        "textStyle": {},
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxImpDetail.add(CopyflxIcon0bd2e3679c4ec42, lblCustomerNameConfirmation, lblBeneAccNum);
    var flxOtherDetails = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flxOtherDetails",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxOtherDetails.setDefaultUnit(kony.flex.DP);
    var flxConfirmEmail = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "20%",
        "id": "flxConfirmEmail",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxConfirmEmail.setDefaultUnit(kony.flex.DP);
    var lblConfirmEmailTitle = new kony.ui.Label({
        "height": "50%",
        "id": "lblConfirmEmailTitle",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.Bene.Beneficiaryemail"),
        "textStyle": {},
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblConfirmEmail = new kony.ui.Label({
        "height": "50%",
        "id": "lblConfirmEmail",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "text": "someone@something.somecom",
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxConfirmEmail.add(lblConfirmEmailTitle, lblConfirmEmail);
    var flxConfirmCountry = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "20%",
        "id": "flxConfirmCountry",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxConfirmCountry.setDefaultUnit(kony.flex.DP);
    var lblConfirmCountryTitle = new kony.ui.Label({
        "height": "50%",
        "id": "lblConfirmCountryTitle",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.NUO.Country"),
        "textStyle": {},
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblConfirmCountry = new kony.ui.Label({
        "height": "50%",
        "id": "lblConfirmCountry",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "text": "Sweden",
        "textStyle": {},
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxConfirmCountry.add(lblConfirmCountryTitle, lblConfirmCountry);
    flxOtherDetails.add(flxConfirmEmail, flxConfirmCountry);
    var flxOtherDetailScroll = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "75%",
        "id": "flxOtherDetailScroll",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "2%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxOtherDetailScroll.setDefaultUnit(kony.flex.DP);
    var flxFromAccountsConf = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxFromAccountsConf",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "-1%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxFromAccountsConf.setDefaultUnit(kony.flex.DP);
    var lblFromAccConfTitle = new kony.ui.Label({
        "height": "50%",
        "id": "lblFromAccConfTitle",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.common.ReferenceNumber"),
        "textStyle": {},
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblRefNoConfText = new kony.ui.Label({
        "height": "50%",
        "id": "lblRefNoConfText",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "textStyle": {},
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxFromAccountsConf.add(lblFromAccConfTitle, lblRefNoConfText);
    var ToAccConfirmation = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "ToAccConfirmation",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    ToAccConfirmation.setDefaultUnit(kony.flex.DP);
    var ToAccConfirmationTitle = new kony.ui.Label({
        "height": "50%",
        "id": "ToAccConfirmationTitle",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.CliQ.ALias"),
        "textStyle": {},
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var ToAccConfirmationText = new kony.ui.Label({
        "height": "50%",
        "id": "ToAccConfirmationText",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "textStyle": {},
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    ToAccConfirmation.add(ToAccConfirmationTitle, ToAccConfirmationText);
    var flxIBAN = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "flxIBAN",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "-1%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxIBAN.setDefaultUnit(kony.flex.DP);
    var lblIBANConfTitle = new kony.ui.Label({
        "height": "50%",
        "id": "lblIBANConfTitle",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": "IBAN",
        "textStyle": {},
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblIBANConftext = new kony.ui.Label({
        "height": "50%",
        "id": "lblIBANConftext",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "textStyle": {},
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxIBAN.add(lblIBANConfTitle, lblIBANConftext);
    var flxConfirmBankName = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "flxConfirmBankName",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxConfirmBankName.setDefaultUnit(kony.flex.DP);
    var lblConfirmBankNameTitle = new kony.ui.Label({
        "height": "50%",
        "id": "lblConfirmBankNameTitle",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.Bene.Bankname"),
        "textStyle": {},
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblConfirmBankName = new kony.ui.Label({
        "height": "50%",
        "id": "lblConfirmBankName",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "textStyle": {},
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxConfirmBankName.add(lblConfirmBankNameTitle, lblConfirmBankName);
    var flxConfirmAddress = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "flxConfirmAddress",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxConfirmAddress.setDefaultUnit(kony.flex.DP);
    var lblConfirmAddTitle = new kony.ui.Label({
        "height": "50%",
        "id": "lblConfirmAddTitle",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.common.address"),
        "textStyle": {},
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblConfirmAddr = new kony.ui.Label({
        "height": "50%",
        "id": "lblConfirmAddr",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "textStyle": {},
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxConfirmAddress.add(lblConfirmAddTitle, lblConfirmAddr);
    var flxConfirmIBANBeneName = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "flxConfirmIBANBeneName",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxConfirmIBANBeneName.setDefaultUnit(kony.flex.DP);
    var lblConfirmIBANBeneNameTitle = new kony.ui.Label({
        "height": "50%",
        "id": "lblConfirmIBANBeneNameTitle",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.transfer.BeneficiaryName"),
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblConfirmIBANBeneName = new kony.ui.Label({
        "height": "50%",
        "id": "lblConfirmIBANBeneName",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxConfirmIBANBeneName.add(lblConfirmIBANBeneNameTitle, lblConfirmIBANBeneName);
    var flxAmount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "flxAmount",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "-1%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxAmount.setDefaultUnit(kony.flex.DP);
    var lblAmountConfirm = new kony.ui.Label({
        "height": "50%",
        "id": "lblAmountConfirm",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.CliQ.TransactionAmount"),
        "textStyle": {},
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblAmountConfirmValue = new kony.ui.Label({
        "height": "50%",
        "id": "lblAmountConfirmValue",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "textStyle": {},
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxAmount.add(lblAmountConfirm, lblAmountConfirmValue);
    var flxRetunredAmount = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "flxRetunredAmount",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "-1%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxRetunredAmount.setDefaultUnit(kony.flex.DP);
    var lblAmountReturnedConfirm = new kony.ui.Label({
        "height": "50%",
        "id": "lblAmountReturnedConfirm",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.CLIQ.ReturnedAmount"),
        "textStyle": {},
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblRetunredAmountConfirmValue = new kony.ui.Label({
        "height": "50%",
        "id": "lblRetunredAmountConfirmValue",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "textStyle": {},
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxRetunredAmount.add(lblAmountReturnedConfirm, lblRetunredAmountConfirmValue);
    var flxConfirmFees = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "flxConfirmFees",
        "isVisible": false,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxConfirmFees.setDefaultUnit(kony.flex.DP);
    var lblConfirmFeesTitle = new kony.ui.Label({
        "height": "50%",
        "id": "lblConfirmFeesTitle",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.bills.FeeAmount"),
        "textStyle": {},
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblConfirmFees = new kony.ui.Label({
        "height": "50%",
        "id": "lblConfirmFees",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "text": "0.000",
        "textStyle": {},
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxConfirmFees.add(lblConfirmFeesTitle, lblConfirmFees);
    var flxConfirmReason = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "clipBounds": true,
        "height": "60dp",
        "id": "flxConfirmReason",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": 0,
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "-1%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxConfirmReason.setDefaultUnit(kony.flex.DP);
    var lblConfirmReasonTitle = new kony.ui.Label({
        "height": "50%",
        "id": "lblConfirmReasonTitle",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblNextDisabled",
        "text": kony.i18n.getLocalizedString("i18n.CLIQ.Reason"),
        "textStyle": {},
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblConfirmReason = new kony.ui.Label({
        "height": "50%",
        "id": "lblConfirmReason",
        "isVisible": true,
        "left": "8%",
        "skin": "sknLblBack",
        "text": "NA",
        "textStyle": {},
        "top": "0dp",
        "width": "90%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxConfirmReason.add(lblConfirmReasonTitle, lblConfirmReason);
    flxOtherDetailScroll.add(flxFromAccountsConf, ToAccConfirmation, flxIBAN, flxConfirmBankName, flxConfirmAddress, flxConfirmIBANBeneName, flxAmount, flxRetunredAmount, flxConfirmFees, flxConfirmReason);
    var flxButtonHolder = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxButtonHolder",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxButtonHolder.setDefaultUnit(kony.flex.DP);
    var btnConfirm = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "slButtonWhiteFocus",
        "height": "50dp",
        "id": "btnConfirm",
        "isVisible": true,
        "onClick": AS_Button_c9e189ea1b6841269e0fa84129573efe,
        "skin": "slButtonWhite",
        "text": kony.i18n.getLocalizedString("i18n.Bene.Confirm"),
        "width": "70%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    flxButtonHolder.add(btnConfirm);
    var lblCurrency = new kony.ui.Label({
        "id": "lblCurrency",
        "isVisible": false,
        "left": "-120dp",
        "skin": "slLabel",
        "text": "USD",
        "textStyle": {},
        "top": "-680dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblBranchNumber = new kony.ui.Label({
        "id": "lblBranchNumber",
        "isVisible": false,
        "left": "-120dp",
        "skin": "slLabel",
        "text": "1232435",
        "textStyle": {},
        "top": "-680dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblFavourite = new kony.ui.Label({
        "id": "lblFavourite",
        "isVisible": false,
        "left": "-130dp",
        "skin": "slLabel",
        "text": "true",
        "textStyle": {},
        "top": "-690dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblLanguage = new kony.ui.Label({
        "id": "lblLanguage",
        "isVisible": false,
        "left": "-140dp",
        "skin": "slLabel",
        "text": "EN",
        "textStyle": {},
        "top": "-700dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxConfirmEPS.add(flxConfirmHeader, flxImpDetail, flxOtherDetails, flxOtherDetailScroll, flxButtonHolder, lblCurrency, lblBranchNumber, lblFavourite, lblLanguage);
    frmIPSRequests.add(flxRequestToPay, flxReturnPayment, flxPaymentHistory, flxConfirmEPS);
};

function frmIPSRequestsGlobals() {
    frmIPSRequests = new kony.ui.Form2({
        "addWidgets": addWidgetsfrmIPSRequests,
        "enabledForIdleTimeout": false,
        "id": "frmIPSRequests",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "preShow": function(eventobject) {
            AS_Form_g65cb29f4ee146479b361ccf0a87dcf6(eventobject);
        },
        "skin": "CopyslFormCommon0aaa3928efb3d48"
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "configureExtendBottom": false,
        "configureExtendTop": false,
        "configureStatusBarStyle": false,
        "footerOverlap": false,
        "formTransparencyDuringPostShow": "100",
        "headerOverlap": false,
        "inputAccessoryViewType": constants.FORM_INPUTACCESSORYVIEW_CANCEL,
        "needsIndicatorDuringPostShow": false,
        "retainScrollPosition": false,
        "titleBar": false,
        "titleBarSkin": "slTitleBar"
    });
};