function initializesegacntstatementstmplt() {
    CopyFlexContainer035bb5165f25443 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "CopyFlexContainer035bb5165f25443",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknCopyslFbox07d05709853a74d"
    }, {}, {});
    CopyFlexContainer035bb5165f25443.setDefaultUnit(kony.flex.DP);
    var transactionDate = new kony.ui.Label({
        "centerY": "33.64%",
        "height": "20dp",
        "id": "transactionDate",
        "isVisible": true,
        "left": "5%",
        "skin": "sknRegisterMobileBank",
        "top": "0dp",
        "width": "65%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var transactionName = new kony.ui.Label({
        "centerY": "66.67%",
        "height": "20dp",
        "id": "transactionName",
        "isVisible": true,
        "left": "5%",
        "skin": "skn383838LatoRegular107KA",
        "width": "65%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var contactListDivider = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "1dp",
        "id": "contactListDivider",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "right": 0,
        "skin": "skntextFieldDivider",
        "top": "59dp",
        "width": "95%",
        "zIndex": 1
    }, {}, {});
    contactListDivider.setDefaultUnit(kony.flex.DP);
    contactListDivider.add();
    var lblStstementLinkKA = new kony.ui.Label({
        "id": "lblStstementLinkKA",
        "isVisible": false,
        "left": "250dp",
        "skin": "slLabel",
        "top": "14dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    CopyFlexContainer035bb5165f25443.add(transactionDate, transactionName, contactListDivider, lblStstementLinkKA);
}