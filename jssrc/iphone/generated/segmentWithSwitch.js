function initializesegmentWithSwitch() {
    Copycontainer0743583703d9f4f = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "Copycontainer0743583703d9f4f",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "isModalContainer": false,
        "skin": "sknCopyslFbox07d05709853a74d"
    }, {}, {});
    Copycontainer0743583703d9f4f.setDefaultUnit(kony.flex.DP);
    var Label01f864cb479e14f = new kony.ui.Label({
        "centerY": "50%",
        "id": "Label01f864cb479e14f",
        "isVisible": true,
        "left": "5%",
        "skin": "skn",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var Switchon = new kony.ui.Switch({
        "centerY": "50%",
        "height": "32dp",
        "id": "Switchon",
        "isVisible": true,
        "leftSideText": "ON",
        "right": "5dp",
        "rightSideText": "OFF",
        "selectedIndex": 0,
        "skin": "sknCopyslSwitch060ce78731a4840",
        "top": "6dp",
        "width": "60dp",
        "zIndex": 1
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    Copycontainer0743583703d9f4f.add(Label01f864cb479e14f, Switchon);
}