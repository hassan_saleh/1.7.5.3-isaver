function initializeiphoneFooterAr() {
    footerBackAr = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "7%",
        "id": "footerBackAr",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0",
        "isModalContainer": false,
        "skin": "skncontainerBkgWhite",
        "top": "0",
        "width": "100%"
    }, {}, {});
    footerBackAr.setDefaultUnit(kony.flex.DP);
    var footerBackground = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": false,
        "height": "100%",
        "id": "footerBackground",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "0",
        "isModalContainer": false,
        "skin": "menu",
        "top": "0",
        "width": "100%"
    }, {}, {});
    footerBackground.setDefaultUnit(kony.flex.DP);
    var FlxMore = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "FlxMore",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_ge948f6d76fd489c88a8861222222e65,
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {}, {});
    FlxMore.setDefaultUnit(kony.flex.DP);
    var img4 = new kony.ui.Image2({
        "centerX": "50%",
        "height": "28dp",
        "id": "img4",
        "isVisible": false,
        "left": "23dp",
        "skin": "sknslImage",
        "src": "tab_more_icon_inactive.png",
        "top": "4dp",
        "width": "28dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var Label0e5331028c2ef41 = new kony.ui.Label({
        "accessibilityConfig": {
            "a11yHidden": false
        },
        "centerX": "51%",
        "id": "Label0e5331028c2ef41",
        "isVisible": true,
        "skin": "sknlblFootertitle",
        "text": "المزيد",
        "textStyle": {},
        "top": "55%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopybtnAccounts0ec4d23080f0146 = new kony.ui.Button({
        "accessibilityConfig": {
            "a11yValue": "More"
        },
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "btnCard",
        "height": "50dp",
        "id": "CopybtnAccounts0ec4d23080f0146",
        "isVisible": true,
        "onClick": AS_Button_df5cbcc73f1045a9885eba03369c3c99,
        "skin": "btnCard",
        "text": "K",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 14],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    FlxMore.add(img4, Label0e5331028c2ef41, CopybtnAccounts0ec4d23080f0146);
    var flxEfawaterccom = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "flxEfawaterccom",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "23%",
        "zIndex": 2
    }, {}, {});
    flxEfawaterccom.setDefaultUnit(kony.flex.DP);
    var btnEfawatercoom = new kony.ui.Button({
        "accessibilityConfig": {
            "a11yValue": "E fawateercom"
        },
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "btnCardFoc",
        "height": "50dp",
        "id": "btnEfawatercoom",
        "isVisible": true,
        "left": "0dp",
        "onClick": AS_Button_g4a37c66a8094964a560870fe3f7a267,
        "skin": "btnCard",
        "text": "x",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 14],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var lblEfawatercoom = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblEfawatercoom",
        "isVisible": true,
        "skin": "sknlblFootertitle",
        "text": "إي فواتيركم",
        "textStyle": {},
        "top": "55%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxEfawaterccom.add(btnEfawatercoom, lblEfawatercoom);
    var FlxDeposits = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "FlxDeposits",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_gaa469fd793f43b6ad86e7aa511fbaa0,
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {}, {});
    FlxDeposits.setDefaultUnit(kony.flex.DP);
    var img3 = new kony.ui.Image2({
        "centerX": "50%",
        "height": "28dp",
        "id": "img3",
        "isVisible": false,
        "left": "23dp",
        "skin": "sknslImage",
        "src": "tab_deposits_icon_inactive.png",
        "top": "4dp",
        "width": "28dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var Label04221a71494e848 = new kony.ui.Label({
        "centerX": "50%",
        "id": "Label04221a71494e848",
        "isVisible": false,
        "skin": "sknlblmenu",
        "text": kony.i18n.getLocalizedString("i18n.common.deposits"),
        "textStyle": {},
        "top": "34dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopybtnAccounts0bc68a97c14fb49 = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "btnCard",
        "height": "50dp",
        "id": "CopybtnAccounts0bc68a97c14fb49",
        "isVisible": true,
        "skin": "btnCard",
        "text": "J",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    FlxDeposits.add(img3, Label04221a71494e848, CopybtnAccounts0bc68a97c14fb49);
    var FlxBot = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "FlxBot",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_gae364c3256e43af8b7fe6f2fdb3e9e8,
        "skin": "slFbox",
        "top": "0dp",
        "width": "20%",
        "zIndex": 2
    }, {}, {});
    FlxBot.setDefaultUnit(kony.flex.DP);
    var imgBot = new kony.ui.Image2({
        "centerX": "50%",
        "height": "40dp",
        "id": "imgBot",
        "isVisible": false,
        "left": "13dp",
        "skin": "slImage",
        "src": "chaticonactive.png",
        "top": "4dp",
        "width": "40dp",
        "zIndex": 1
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var CopybtnAccounts0ff48f9feb0aa42 = new kony.ui.Button({
        "accessibilityConfig": {
            "a11yValue": "Transfer"
        },
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "btnCardFoc",
        "height": "50dp",
        "id": "CopybtnAccounts0ff48f9feb0aa42",
        "isVisible": true,
        "onClick": AS_Button_b76c39ef427143f18d933073c05028af,
        "skin": "btnCard",
        "text": "i",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 14],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    var CopyLabel0i584dcaf2e8546 = new kony.ui.Label({
        "centerX": "50%",
        "id": "CopyLabel0i584dcaf2e8546",
        "isVisible": true,
        "skin": "sknlblFootertitle",
        "text": "التحويلات",
        "textStyle": {},
        "top": "55%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    FlxBot.add(imgBot, CopybtnAccounts0ff48f9feb0aa42, CopyLabel0i584dcaf2e8546);
    var FlxTranfers = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "FlxTranfers",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "onClick": AS_FlexContainer_dbc66761cc274f4e90044dc3e88de0b6,
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {}, {});
    FlxTranfers.setDefaultUnit(kony.flex.DP);
    var img2 = new kony.ui.Image2({
        "centerX": "50%",
        "height": "28dp",
        "id": "img2",
        "isVisible": false,
        "left": "23dp",
        "skin": "sknslImage",
        "src": "tab_t_and_p_icon_inactive.png",
        "top": "4dp",
        "width": "28dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var Label02bec01fd5baf4c = new kony.ui.Label({
        "centerX": "50%",
        "id": "Label02bec01fd5baf4c",
        "isVisible": true,
        "skin": "sknlblFootertitle",
        "text": "بطاقات",
        "textStyle": {},
        "top": "55%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CopybtnAccounts0ia3b1ff37c304b = new kony.ui.Button({
        "accessibilityConfig": {
            "a11yValue": "Cards"
        },
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "btnFooterDisablefocusskin",
        "height": "50dp",
        "id": "CopybtnAccounts0ia3b1ff37c304b",
        "isVisible": true,
        "onClick": AS_Button_d31de18e65a24ea093e249b248f73316,
        "skin": "btnFooterDisable",
        "text": "G",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 14],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    FlxTranfers.add(img2, Label02bec01fd5baf4c, CopybtnAccounts0ia3b1ff37c304b);
    var FlxAccounts = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "FlxAccounts",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "slFbox",
        "top": "0dp",
        "width": "20%",
        "zIndex": 1
    }, {}, {});
    FlxAccounts.setDefaultUnit(kony.flex.DP);
    var img1 = new kony.ui.Image2({
        "centerX": "50%",
        "height": "28dp",
        "id": "img1",
        "isVisible": false,
        "left": "23dp",
        "skin": "sknslImage",
        "src": "tab_accounts_icon_inactive.png",
        "top": "4dp",
        "width": "28dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var Label03174bff69bb54c = new kony.ui.Label({
        "centerX": "50%",
        "id": "Label03174bff69bb54c",
        "isVisible": true,
        "skin": "sknlblFootertitleFocus",
        "text": kony.i18n.getLocalizedString("i18n.my_money.accounts"),
        "textStyle": {},
        "top": "55%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "retainFlexPositionProperties": true
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var btnAccounts = new kony.ui.Button({
        "accessibilityConfig": {
            "a11yValue": "Accounts"
        },
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "btnfooterenablefont",
        "height": "50dp",
        "id": "btnAccounts",
        "isVisible": true,
        "onClick": AS_Button_g4a16aaff6494bdab98a870e65bf71ad,
        "skin": "btnFooterDisableaccount",
        "text": "H",
        "width": "100%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 12],
        "paddingInPixel": false
    }, {
        "showProgressIndicator": true
    });
    FlxAccounts.add(img1, Label03174bff69bb54c, btnAccounts);
    footerBackground.add(FlxMore, flxEfawaterccom, FlxDeposits, FlxBot, FlxTranfers, FlxAccounts);
    footerBackAr.add(footerBackground);
}