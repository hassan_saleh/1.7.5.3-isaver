function initializetmpAccountType() {
    CopyyourAccount01dfa1aa140424b = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "focusSkin": "sknyourAccountCardFocus",
        "height": "62dp",
        "id": "CopyyourAccount01dfa1aa140424b",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "5%",
        "isModalContainer": false,
        "skin": "sknyourAccountCard",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    CopyyourAccount01dfa1aa140424b.setDefaultUnit(kony.flex.DP);
    var nameContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100%",
        "id": "nameContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "isModalContainer": false,
        "skin": "sknslFbox",
        "top": "0dp",
        "width": "60%",
        "zIndex": 1
    }, {}, {});
    nameContainer.setDefaultUnit(kony.flex.DP);
    var nameAccount1 = new kony.ui.Label({
        "centerY": "50%",
        "id": "nameAccount1",
        "isVisible": true,
        "left": "15dp",
        "maxWidth": "90%",
        "skin": "sknaccountName",
        "top": "12dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblColorKA = new kony.ui.Label({
        "height": "100%",
        "id": "lblColorKA",
        "isVisible": true,
        "left": "0dp",
        "skin": "slLabel",
        "top": "0dp",
        "width": "6dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    nameContainer.add(nameAccount1, lblColorKA);
    var typeKA = new kony.ui.Label({
        "bottom": "2dp",
        "centerX": "50%",
        "id": "typeKA",
        "isVisible": false,
        "right": "0dp",
        "skin": "sknaccountAvailableBalanceLabel",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    CopyyourAccount01dfa1aa140424b.add(nameContainer, typeKA);
}