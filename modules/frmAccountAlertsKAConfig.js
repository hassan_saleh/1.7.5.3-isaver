var frmAccountAlertsKAConfig = {
    "formid": "frmAccountAlertsKA",
    "frmAccountAlertsKA": {
        "entity": "UserAccountAlerts",
        "objectServiceName": "RBObjects",
        "objectServiceOptions": {
            "access": "online"
        }
    },
    "minBalance": {
        "fieldprops": {
            "entity": "UserAccountAlerts",
            "field": "minimumBalance",
            "widgettype": "Label"
        }
    },
    "balance": {
        "fieldprops": {
            "widgettype": "Label",
            "entity": "UserAccountAlerts",
            "field": "balanceUpdate"
        }
    },
    "debit": {
        "fieldprops": {
            "widgettype": "Label",
            "entity": "UserAccountAlerts",
            "field": "debitLimit"
        }
    },
    "credit": {
        "fieldprops": {
            "widgettype": "Label",
            "entity": "UserAccountAlerts",
            "field": "creditLimit"
        }
    },
    "payment": {
        "fieldprops": {
            "widgettype": "Label",
            "entity": "UserAccountAlerts",
            "field": "paymentDueReminder"
        }
    },
    "deposit": {
            "fieldprops": {
                "widgettype": "Label",
                "entity": "UserAccountAlerts",
                "field": "depositDueReminder"
            }
        },
    "HiddenAlertsReq": {
        "fieldprops": {
            "widgettype": "Label",
            "entity": "UserAccountAlerts",
            "field": "isEnabled"
        }
    },
    "HiddenSuccessTransfer": {
        "fieldprops": {
            "widgettype": "Label",
            "entity": "UserAccountAlerts",
            "field": "interestRate"
        }
    },
    "HiddenCheckClear": {
        "fieldprops": {
            "widgettype": "Label",
            "entity": "UserAccountAlerts",
            "field": "checkClearance"
        }
    },
    "HiddenName": {
        "fieldprops": {
            "widgettype": "Label",
            "entity": "UserAccountAlerts",
            "field": "accountType"
        }
    },
  
  "HiddenAccNum": {
        "fieldprops": {
            "widgettype": "Label",
            "entity": "UserAccountAlerts",
            "field": "accountNumber"
        }
    },
  "HiddenAlertId": {
        "fieldprops": {
            "widgettype": "Label",
            "entity": "UserAccountAlerts",
            "field": "alertId"
        }
    }
};