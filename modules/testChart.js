function textChart()
{
  var pieChart = new com.konymp.piechart(

    {

      "clipBounds": true,

      "id": "pieChart",

      "height": "100%",

      "width": "100%",

      "top": "0%",

      "left": "0%",

      "skin": "CopyslFbox2",

      "layoutType": kony.flex.FREE_FORM,

      "masterType": constants.MASTER_TYPE_USERWIDGET,

      "isVisible": true,

      "zIndex": 1

    }, {}, {});



  /* Setting the component s properties */

  pieChart.bgColor = "#FFFFFF";

  pieChart.chartData =

    {

    "data":

    [

      {"colorCode": "#1b9ed9", "label": "data1", "value": 40}, 

      {"colorCode": "#e8672b", "label": "data2", "value": 20}, 

      {"colorCode": "#76c044", "label": "data3", "value": 10}

    ]

  };

  pieChart.enableStaticPreview = true;

  pieChart.enableLegend = true;

  pieChart.chartTitle = "Pie Chart";

  pieChart.titleFontColor = "#000000";

  pieChart.legendFontColor = "#000000";

  pieChart.titleFontSize = "12";

  pieChart.legendFontSize = "8";



  /* Adding the component to the form */

}