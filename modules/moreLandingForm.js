var greetingName = "John";

// frmMoreLandingKA becomes "Resources" on android
function moreLandingInit(){
  	userAgent = kony.os.userAgent();
    if (userAgent !== "iPhone"){
      //	frmMoreLandingKA.bottomContainer.top = "0dp";
      	//frmMoreLandingKA.moreScrollContainer.top = "50dp";
      //frmMoreLandingKA.moreResourcesSegment.top= "0%";
      frmMoreLandingKA.flxOuterBottonContainerKA.top="50dp";
      frmMoreLandingKA.flxOuterBottonContainerKA.height="92%";
    }
}

// moreLandingPreShow - sets user greeting text
function moreLandingPreShow() {
  deviceRegFrom = "moreLanding";
   if (userAgent === "iPhone"){
  		//frmMoreLandingKA.greetingName.text = "Hi, " + greetingName;
  		//frmMoreLandingKA.moreLandingTitle.text = "Hi, " + greetingName;
  }
   userAgent = kony.os.userAgent();
  
  setMoreLandingSegmentData();
  
}

function setAndroidPrimaryMenuSegmentData(){
    var data=[];
    data = getAndroidPrimaryMenuMasterData();
    
    //frmAndroidMenuKA.androidPrimaryNavigation.setData(data);
}

function getAndroidPrimaryMenuMasterData(){
  var arrData = [];
 
  /* arrData.push(new segAndroidPrimaryMenuData(kony.i18n.getLocalizedString("i18n.overview.accountOverview"),"tab_accounts_icon_inactive.png"));
  arrData.push(new segAndroidPrimaryMenuData(kony.i18n.getLocalizedString("i18n.common.transferAndPay"),"tab_t_and_p_icon_inactive.png"));
  if(kony.retailBanking.globalData.globals.configPreferences.rdc==="ON"){
  arrData.push(new segAndroidPrimaryMenuData(kony.i18n.getLocalizedString("i18n.common.deposits"),"tab_deposits_icon_inactive.png"));  
  }
  arrData.push(new segAndroidPrimaryMenuData(kony.i18n.getLocalizedString("i18n.more.messages"),"message_icon.png"));
  arrData.push(new segAndroidPrimaryMenuData(kony.i18n.getLocalizedString("i18n.common.locateUs"),"locator_pin_inactive.png"));
  arrData.push(new segAndroidPrimaryMenuData(kony.i18n.getLocalizedString("i18n.common.contactUs"),"phone_icon_inactive.png"));
  arrData.push(new segAndroidPrimaryMenuData(kony.i18n.getLocalizedString("i18n.settings.settings"),"settings_icon_inactive.png"));
  arrData.push(new segAndroidPrimaryMenuData(kony.i18n.getLocalizedString("i18n.more.resources"),"tab_more_icon_inactive.png"));
  arrData.push(new segAndroidPrimaryMenuData('ChatBot','chaticon.png')); */
  return arrData;
}

 
 function segAndroidPrimaryMenuData(lblPageNameKA,imgSegIcon) {
  this.Label01f864cb479e14f = lblPageNameKA;
  this.Image02e20b01dcc2e40 = {"src":imgSegIcon};
  
}

 
function setAndroidSecondaryMenuSegmentData(){
    var data=[];
    data = getAndroidSecondaryMenuMasterData();
    
    //frmAndroidMenuKA.androidPrimaryNavigation.setData(data);
}

function getAndroidSecondaryMenuMasterData(){
  var arrData = [];
 
  arrData.push(new segAndroidSecondaryMenuData(kony.i18n.getLocalizedString("i18n.more.messages"),"message_icon.png"));
  arrData.push(new segAndroidSecondaryMenuData(kony.i18n.getLocalizedString("i18n.common.locateUs"),"locator_pin_inactive.png"));
  arrData.push(new segAndroidSecondaryMenuData(kony.i18n.getLocalizedString("i18n.common.contactUs"),"phone_icon_inactive.png"));
  arrData.push(new segAndroidSecondaryMenuData(kony.i18n.getLocalizedString("i18n.settings.settings"),"settings_icon_inactive.png"));
  arrData.push(new segAndroidSecondaryMenuData(kony.i18n.getLocalizedString("i18n.more.resources"),"tab_more_icon_inactive.png"));
  
  return arrData;
}

 
 function segAndroidSecondaryMenuData(lblPageNameKA,imgSegIcon) {
  this.Label01f864cb479e14f = lblPageNameKA;
  this.Image02e20b01dcc2e40 = {"src":imgSegIcon};
  
}



function setMoreLandingSegmentData(){
    var data=[];
    data = getMasterData();
    
    frmMoreLandingKA.moreResourcesSegment.setData(data);
}
function getMasterData(){
  var arrData = [];
  
  arrData.push(new segData(kony.i18n.getLocalizedString("i18n.cards.ManageCards"),"right_chevron_icon.png"));
  arrData.push(new segData(kony.i18n.getLocalizedString("i18n.my_money.myMoney"),"right_chevron_icon.png"));
  if(kony.retailBanking.globalData.globals.configPreferences.newAccount==="ON"){
 // arrData.push(new segData(kony.i18n.getLocalizedString("i18n.resources.OpenNewAccount"),"right_chevron_icon.png"));  
  }
  arrData.push(new segData(kony.i18n.getLocalizedString("i18n.resources.ManageBillPayees"),"right_chevron_icon.png"));
  arrData.push(new segData(kony.i18n.getLocalizedString("i18n.more.checkReOrderName"),"right_chevron_icon.png"));
  arrData.push(new segData(kony.i18n.getLocalizedString("i18n.more.faqS"),"right_chevron_icon.png"));
  arrData.push(new segData(kony.i18n.getLocalizedString("i18n.more.privacyPolicy"),"right_chevron_icon.png"));
  arrData.push(new segData(kony.i18n.getLocalizedString("i18n.common.termsAndConditions"),"right_chevron_icon.png"));
  arrData.push(new segData(kony.i18n.getLocalizedString("i18n.more.foreignExchangeRates"),"right_chevron_icon.png"));
  arrData.push(new segData(kony.i18n.getLocalizedString("i18n.resources.InterestRates"),"right_chevron_icon.png"));
  
 
  return arrData;
}

 function segData(lblPageNameKA,  imgicon) {
  this.lblPageNameKA = lblPageNameKA;
  
  this.imgicontick ={"src":imgicon};
}

function hide_openNewAccount(value){
  frmAccountsLandingKA.segAccountOverViewNav1KA.setVisibility(false);
//   var segmentData={};
//   segmentData = frmAccountsLandingKA.segAccountOverViewNav1KA.data;
//   var data=[];
//   data = frmAccountsLandingKA.segAccountOverViewNav1KA.data;
//   var i=0, index=0;
//   while((i<segmentData.length)){
//     if(segmentData[i]["Label01206abc40b8c42"] === value){
//       data.length -=1;
//       break;                           
//     }
//     i++;
//   }
 
//   for(index = i;index<(segmentData.length-1); index++){
//     i++;
//     data[index] = segmentData[i];
//   }
  
//   frmAccountsLandingKA.segAccountOverViewNav1KA.setData(data);
 
}


// Segment Row Actions for frmMoreLandingKA Form
function moreResourcesSegmentClick(){
	var selectedRow = frmMoreLandingKA.moreResourcesSegment.selectedRowIndex[1];
    data = frmMoreLandingKA.moreResourcesSegment.data;
  	var selectedRow_value = data[selectedRow]["lblPageNameKA"];  
    if(ColumnChart){
    frmMyMoneyListKA.flxSpendingOverviewKAinner.remove(ColumnChart);
    ColumnChart = "";
  }
  	switch(selectedRow_value){
      case i18n_ManageCards:
        	 var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
			 var controller = INSTANCE.getFormController("frmManageCardsKA");
			 var navigationObject = new kony.sdk.mvvm.NavigationObject();
			 navigationObject.setRequestOptions("segCardsKA",{"headers":{"session_token":kony.retailBanking.globalData.session_token}});
			 controller.performAction("navigateTo",["frmManageCardsKA",navigationObject]);
				break;
      case i18n_myMoney:
          from = "MyMoney";
             kony.retailBanking.fromAppMenu = true;
             kony.retailBanking.columnChartGenereted = false;
             showFormMyMoneyAccountsList();
//         var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
// var listController = INSTANCE.getFormController("frmMyMoneyListKA");
// var navObject = new kony.sdk.mvvm.NavigationObject();
// navObject.setRequestOptions("segAccountsKA",{"headers":{"session_token":kony.retailBanking.globalData.session_token}});
// listController.performAction("navigateTo",["frmMyMoneyListKA",navObject]); 
             //frmMyMoneyListKA.show();
        	break;
      case i18n_openNewAccount:
            //frmPickAProductKA.show();
			navigateToPickAProduct();
        	break;
      case "Manage Bill Payees":
        	//frmManagePayeeKA.show();
            showFormManagePayeeList();
    		break;
      case "FAQs":
            //frmMoreFaqKA.show();
        	onClickFaqs();
    		break;
      case "Privacy Policy":
        	//frmMorePrivacyPolicyKA.show();
        	onClickPrivacyPolicy();
    		break;
      case "Terms & Conditions":
        	//frmMoreTermsAndConditionsKA.show();
        	onClickTandC();
    		break;
      case "Foreign Exchange Rates":
        //	frmMoreForeignExchangeRatesKA.show();
			onClickForeinExchangeRates();
    		break;
      case "Interest Rates":
       		//frmMoreInterestRatesKA.show();
        	onClickInterestRates();
        	break;
      case "Order Checks":
       		navigateToCheckReOrder();
          break;
      default:
        	frmMoreLandingKA.show();
    }
}

// Called on frmMoreLandingKA.moreScrollContainer onScrolling action
function moreScroll(){
  if (userAgent === "iPhone"){
  	var outerScrollY = frmMoreLandingKA.moreScrollContainer.contentOffsetMeasured.y;
  	var outerScrollYAbs=Math.abs(outerScrollY);
  	
  	// Display accountLabelScroll element
  	if (outerScrollY > 200){
    	frmMoreLandingKA.accountLabelScroll.isVisible = true;
    } else if (outerScrollY > 0 && outerScrollY < 200) {
          frmMoreLandingKA.accountLabelScroll.isVisible = false;
    }
	
    // If user scrolls upwards
    if (outerScrollY > 0){
       	frmMoreLandingKA.topContainer.opacity= (1-(outerScrollYAbs*0.006));
       	frmMoreLandingKA.topContainer.top=(0+(outerScrollYAbs*0.7)); 
    }
    // Move moreLandingTitle into view
    if (outerScrollY > 20 && outerScrollY < 135){
      	frmMoreLandingKA.moreLandingTitle.top = (55-(outerScrollYAbs*0.3));
    }

    // Stick moreLandingTitle top to 15dp
    if (outerScrollY > 136){
      	frmMoreLandingKA.moreLandingTitle.top = "15dp";
    }

    // if user scrolls downwards  
    if (outerScrollY < 0){
     	frmMoreLandingKA.topContainer.opacity=(1+(outerScrollYAbs*0.02));
     	frmMoreLandingKA.topContainer.top=(0-(outerScrollYAbs*0.5));

    	 // Fallback to animate moreLandingTitle to default
    	 hideMoreLandingTitle();
 	}
  }
}

// Fallback animation for moreLandingTitle called in moreScroll()
function hideMoreLandingTitle(){
	frmMoreLandingKA.moreLandingTitle.animate(
        kony.ui.createAnimation({"100":{ "top": "55dp",
                  "stepConfig":{"timingFunction": easeIn}}}),
        {"fillMode": forwards, "duration": 0.2},
        {"animationEnd": function () {}}
    );
}