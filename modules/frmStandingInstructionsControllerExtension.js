/*
 * Controller Extension class for frmStandingInstructions
 * Developer can edit the existing methods or can add new methods if required
 *
 */
kony = kony || {};
kony.sdk = kony.sdk || {};
kony.sdk.mvvm = kony.sdk.mvvm || {};
kony.sdk.mvvm.v2 = kony.sdk.mvvm.v2 || {};

/**
 * Creates a new Form Controller Extension.
 * @class frmStandingInstructionsControllerExtension
 * @param {Object} controllerObj - Form Controller.
 */
kony.sdk.mvvm.frmStandingInstructionsControllerExtension = Class(kony.sdk.mvvm.BankingAppControllerExtension, {
    constructor: function(controllerObj) {
        this.$class.$super.call(this, controllerObj);
    },
    /** 
     * This method is an entry point for all fetch related flows. Developer can edit.
     * Default implementation fetches data for the form based on form config 
     * @memberof frmStandingInstructionsControllerExtension#
     */
    fetchData: function() {
        try {
            var scopeObj = this;
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen(" ");
            this.$class.$superp.fetchData.call(this, success, error);
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("Error in fetchData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_FETCH_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_FETCH_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
            customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18nkey("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
        }

        function success(response) {
            kony.sdk.mvvm.log.info("success fetching data ", response);
            var navigationObject = this.getController() && this.getController().getContextData();
            navigationObject.setCustomInfo("segSIList", response.segSIList);

            var formattedResponse = scopeObj.getController().processData(response);
            scopeObj.bindData(formattedResponse);
        }

        function error(err) {
            //Error fetching data
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("In fetchData errorcallback in controller extension ", err);
            var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_FETCH_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_FETCH_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    /** 
     * This method processes fetched data. Developer can edit.
     * Default implementation processes the provided data to required format for bind.
     * @param {Object} data - fetched data. (Default : data map, group id as key and records array as value)
     * @memberof frmStandingInstructionsControllerExtension#
     * @returns {Object} - processed data
     */
    processData: function(data) {
        try {
            var scopeObj = this;

            var x = data._raw_response_.segSIList.records[0].SiDetailsResponse;
            for (var i in x) {
            	if(x[i].beneficiary_name.trim() !== ""){
                	x[i].initial = x[i].beneficiary_name.substring(0, 2).toUpperCase();  
                }else{
                	x[i].initial = x[i].credit_acc.substring(0, 2).toUpperCase();
                }
                x[i].initial = x[i].beneficiary_name.substring(0, 2).toUpperCase();
                x[i].formattedDate = formatDateBill(x[i].start_date);
                x[i].formattedAmount = setDecimal(x[i].amount, 3) + " " + x[i].ccy_code;

            }
            kony.boj.SIData = x;
            var processedData = {
                "segSIList": {
                    "segSIList": x
                }
            };
            kony.print("Standing Instructions Processed Data" + JSON.stringify(processedData));

            return processedData;
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("Error in processData of controllerExtension");
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_PROCESSDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_PROCESSDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
            customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18nkey("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
        };
    },
    /** 
     * This method binds the processed data to the form. Developer can edit.
     * Default implementation binds the data to widgets in the form.
     * @param {Object} data - processed data.(Default : data map for each group, widget id as key and widget data as value)
     * @memberof frmStandingInstructionsControllerExtension#
     */
    bindData: function(data) {
        try {
            kony.print("data123::" + JSON.stringify(data));
            var formmodel = this.getController().getFormModel();
            formmodel.clear();

            if (data.segSIList.segSIList !== null && data.segSIList.segSIList !== undefined && data.segSIList.segSIList !== "") {
                var widgetDataMap = formmodel.getViewAttributeByProperty("segSIList", "widgetDataMap");
                widgetDataMap.lblTransactiondate = "formattedDate";
                widgetDataMap.lblTransactionDesc = "beneficiary_name";
                widgetDataMap.lblAmount = "formattedAmount";
                widgetDataMap.lblInitial = "initial";
                formmodel.setViewAttributeByProperty('segSIList', "widgetDataMap", widgetDataMap);
                this.$class.$superp.bindData.call(this, data);
                frmStandingInstructions.segSIList.isVisible = true;
                frmStandingInstructions.lblNoSI.isVisible = false;
				frmStandingInstructions.lblFilter.setEnabled(true);
                frmStandingInstructions.lblSortSI.setEnabled(true);
            	frmStandingInstructions.tbxSearch.setEnabled(true);
            } else {
                frmStandingInstructions.segSIList.isVisible = false;
                frmStandingInstructions.lblNoSI.isVisible = true;
                frmStandingInstructions.lblFilter.setEnabled(false);
                frmStandingInstructions.lblSortSI.setEnabled(false);
            	frmStandingInstructions.tbxSearch.setEnabled(false);
            }
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        	this.getController().getFormModel().formatUI();
            this.getController().showForm();
        } catch (err) {
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
            kony.sdk.mvvm.log.error("Error in bindData of controllerExtension :: " + err);
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_BINDDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_BINDDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
            customAlertPopup(geti18Value("i18n.Bene.Failed"), geti18nkey("i18n.common.somethingwentwrong"), popupCommonAlertDimiss, "");
        }
    },
    /** 
     * This method is entry point for save flow. Developer can edit.
     * Default implementation saves the entity record from the data of widgets defined in form config 
     * @memberof frmStandingInstructionsControllerExtension#
     */
    saveData: function() {
        try {
            var scopeObj = this;
            this.$class.$superp.saveData.call(this, success, error);
        } catch (err) {
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

        function success(res) {
            //Successfully created record
            kony.sdk.mvvm.log.info("success saving record ", res);
        }

        function error(err) {
            //Handle error case
            kony.sdk.mvvm.log.error("In saveData errorcallback in controller extension ", err);
            var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SAVEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

    },
    /** 
     * This method is entry point for delete/remove flow. Developer can edit.
     * Default implementation deletes the entity record displayed in form (primary keys are needed)
     * @memberof frmStandingInstructionsControllerExtension#
     */
    deleteData: function() {
        try {
            var scopeObj = this;
            this.$class.$superp.deleteData.call(this, success, error);
        } catch (err) {
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }

        function success(res) {
            //Successfully deleting record
            kony.sdk.mvvm.log.info("success deleting record " + JSON.stringify(res));
        }

        function error(err) {
            //Handle error case
            kony.sdk.mvvm.log.error("In deleteData errorcallback in controller extension ", err);
            var exception = scopeObj.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_DELETEDATA_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    },
    /** 
     * This method shows form.
     * @memberof frmStandingInstructionsControllerExtension#
     */
    showForm: function() {
        try {
            var formmodel = this.getController().getFormModel();
            formmodel.showView();
        } catch (e) {
            var exception = this.getController().getApplicationContext().getFactorySharedInstance().createExceptionObject(kony.sdk.mvvm.ExceptionCode.CD_ERROR_SHOWFORM_IN_CONTROLLER_EXTENSION, kony.sdk.mvvm.ExceptionCode.MSG_ERROR_SHOWFORM_IN_CONTROLLER_EXTENSION, err);
            kony.sdk.mvvm.log.error(exception.toString());
        }
    }
});