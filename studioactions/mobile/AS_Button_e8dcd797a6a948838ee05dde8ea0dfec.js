function AS_Button_e8dcd797a6a948838ee05dde8ea0dfec(eventobject) {
    frmFxRate.btnCalculateFx.skin = "slButtonWhiteTab";
    frmFxRate.btnCalculateFx.focusSkin = "slButtonWhiteTabFocus";
    frmFxRate.btnExchangeRates.skin = "slButtonWhiteTabDisabled";
    frmFxRate.flxScrlCalculateFx.setVisibility(true);
    frmFxRate.flxScrlExchangeRates.setVisibility(false);
    frmFxRate.forceLayout();
}