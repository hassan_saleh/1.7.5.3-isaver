function AS_Label_b8f2db1f7d2e4949a366f594878dae7a(eventobject, x, y) {
    var currentForm = kony.application.getCurrentForm();
    if (currentForm.id === "frmLoginKA" || currentForm.id === "frmLanguageChange") {
        currentForm.tbxusernameTextField.text = "";
        currentForm.borderBottom.skin = "skntextFieldDivider";
        animateLabel("DOWN", "lblUserName", currentForm.tbxusernameTextField.text);
        currentForm.lblInvalidCredentialsKA.isVisible = false;
    }
}