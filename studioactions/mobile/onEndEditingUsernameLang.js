function onEndEditingUsernameLang(eventobject, changedtext) {
    return AS_TextField_h3b9357d5bfd46cfb7a1129d35242edb(eventobject, changedtext);
}

function AS_TextField_h3b9357d5bfd46cfb7a1129d35242edb(eventobject, changedtext) {
    if (frmLanguageChange.v.text !== null && frmLanguageChange.tbxusernameTextField.text !== "") {
        frmLanguageChange.borderBottom.skin = "skntextFieldDividerGreen";
    } else {
        frmLanguageChange.borderBottom.skin = "skntextFieldDivider";
    }
    animateLabel("DOWN", "lblUserName", frmLanguageChange.tbxusernameTextField.text);
    animateLabel("UP", "lblPassword", frmLanguageChange.passwordTextField.text);
}