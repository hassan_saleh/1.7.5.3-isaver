function AS_onEndEditingAddress2Android(eventobject, changedtext) {
    return AS_TextField_e048c12c9b484fca987f6be9535e1873(eventobject, changedtext);
}

function AS_TextField_e048c12c9b484fca987f6be9535e1873(eventobject, changedtext) {
    if (frmAddExternalAccountKA.tbxAddress2.text !== "") {
        frmAddExternalAccountKA.flxUnderlineAddress2.skin = "sknFlxGreenLine";
    } else {
        frmAddExternalAccountKA.flxUnderlineAddress2.skin = "sknFlxOrangeLine";
    }
    animateLabel("DOWN", "lblAddress2", frmAddExternalAccountKA.lblAddress2.text);
}