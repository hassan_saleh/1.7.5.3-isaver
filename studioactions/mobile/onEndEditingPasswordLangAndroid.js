function onEndEditingPasswordLangAndroid(eventobject, changedtext) {
    return AS_TextField_h1734a6ca5e245b3b3275a10b135f4f2(eventobject, changedtext);
}

function AS_TextField_h1734a6ca5e245b3b3275a10b135f4f2(eventobject, changedtext) {
    if (frmLanguageChange.passwordTextField.text !== null && frmLanguageChange.passwordTextField.text !== "") {
        frmLanguageChange.flxbdrpwd.skin = "skntextFieldDividerGreen";
    } else {
        frmLanguageChange.flxbdrpwd.skin = "skntextFieldDivider";
    }
    animateLabel("DOWN", "lblPassword", frmLanguageChange.passwordTextField.text);
}