function AS_Segment_c6d599ed963a456cb8415a2275a35bb8(eventobject, x, y) {
    if (Object.keys(swipedIndices).length > 0) {
        animationObj = getTransAnimDefinition("0%");
        coords = [];
        frmChatBots.segBotUser.animateRows({
            rows: [{
                sectionIndex: swipedIndices["secIndex"],
                rowIndex: swipedIndices["rowIndex"]
            }],
            widgets: ["flxBotUserSwipe"],
            animation: animationObj
        });
        swipedIndices = {};
    }
}