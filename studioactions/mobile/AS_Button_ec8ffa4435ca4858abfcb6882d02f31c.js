function AS_Button_ec8ffa4435ca4858abfcb6882d02f31c(eventobject) {
    //kony.timer.schedule("scanCardTimer",scanCardTimerFn, 15, false);
    if (isAndroid()) {
        BojScan.BlinkCardMethodInvoker(microBlinkCallBack, SCANCARDLICENSEANDROID);
    } else {
        //Creates an object of class 'iPhoneMicroBlink'
        var iPhoneMicroBlinkObject = new BojScan.iPhoneMicroBlink(SCANCARDLICENSEIOS);
        //Invokes method 'scanCardWithCallback' on the object
        iPhoneMicroBlinkObject.scanCardWithCallback(
            /**Function*/
            microBlinkCallBack);
    }
}