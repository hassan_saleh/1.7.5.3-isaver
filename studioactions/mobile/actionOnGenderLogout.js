function actionOnGenderLogout(eventobject) {
    return AS_Button_ba8b500ca01f40bfa414b2ecc956d375(eventobject);
}

function AS_Button_ba8b500ca01f40bfa414b2ecc956d375(eventobject) {
    function SHOW_ALERT_ide_onClick_d1ff8abae9be4604beda08ca2c50218f_True() {
        kony.print("Deleted");
    }

    function SHOW_ALERT_ide_onClick_d1ff8abae9be4604beda08ca2c50218f_False() {}

    function SHOW_ALERT_ide_onClick_d1ff8abae9be4604beda08ca2c50218f_Callback(response) {
        if (response === true) {
            SHOW_ALERT_ide_onClick_d1ff8abae9be4604beda08ca2c50218f_True();
        } else {
            SHOW_ALERT_ide_onClick_d1ff8abae9be4604beda08ca2c50218f_False();
        }
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": kony.i18n.getLocalizedString("i18n.common.confirmSignOut"),
        "yesLabel": kony.i18n.getLocalizedString("i18n.common.signOut"),
        "noLabel": kony.i18n.getLocalizedString("i18n.common.cancel"),
        "message": kony.i18n.getLocalizedString("i18n.common.signOutAlert"),
        "alertHandler": SHOW_ALERT_ide_onClick_d1ff8abae9be4604beda08ca2c50218f_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    });
}