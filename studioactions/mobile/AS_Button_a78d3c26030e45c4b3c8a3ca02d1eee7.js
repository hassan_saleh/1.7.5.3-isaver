function AS_Button_a78d3c26030e45c4b3c8a3ca02d1eee7(eventobject) {
    customAlertPopup(geti18Value("i18n.common.AreYouSure"), geti18Value("i18n.transfers.eraseAllAlert"), onClickYesBackIPS, popupCommonAlertDimiss, geti18Value("i18n.common.YES"), geti18Value("i18n.common.NO"));
}