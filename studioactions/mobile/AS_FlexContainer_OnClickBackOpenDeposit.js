function AS_FlexContainer_OnClickBackOpenDeposit(eventobject) {
    return AS_FlexContainer_a3231f1b6ea942e1a635a05ee9b5ee99(eventobject);
}

function AS_FlexContainer_a3231f1b6ea942e1a635a05ee9b5ee99(eventobject) {
    if (frmOpenTermDeposit.flxConfirmDeposite.isVisible === true) {
        frmOpenTermDeposit.flxDepositSelection.setVisibility(true);
        frmOpenTermDeposit.flxConfirmDeposite.setVisibility(false);
        frmOpenTermDeposit.btnNextDeposite.setVisibility(true);
    } else {
        customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.transfers.eraseAllAlert"), userResponseESP, onClickNoBackESP, geti18Value("i18n.common.YES"), geti18Value("i18n.common.NO"));
    }

    function userResponseESP() {
        resetForm();
        frmAccountsLandingKA.show();
        frmOpenTermDeposit.destroy();
        popupCommonAlertDimiss();
    }

    function onClickNoBackESP() {
        popupCommonAlertDimiss();
    }
}