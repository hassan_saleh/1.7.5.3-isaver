function onDoneTbxAmntNewBillKA(eventobject, changedtext) {
    return AS_TextField_cd07318128654403a104f8346064d68d(eventobject, changedtext);
}

function AS_TextField_cd07318128654403a104f8346064d68d(eventobject, changedtext) {
    frmNewBillKA.lblHiddenAmount.text = frmNewBillKA.tbxAmount.text;
    var amount = frmNewBillKA.tbxAmount.text;
    if (amount === null || amount === "" || amount == "." || (Number(amount) < 0.01) || amount === 0.00) {} else {
        if (kony.store.getItem("BillPayfromAcc") !== null && kony.store.getItem("BillPayfromAcc") !== undefined) {
            var currency = "";
            if (frmNewBillKA.btnBillsPayAccounts.text === "t") currency = kony.store.getItem("BillPayfromAcc").currencyCode;
            else {
                if (isNullorEmptyorUndefined(kony.store.getItem("BillPayfromAcc").balance)) // hassan pay from credit card
                    customAlertPopup("", kony.i18n.getLocalizedString("i18n.cards.selectCard"), popupCommonAlertDimiss, "");
                else currency = kony.store.getItem("BillPayfromAcc").balance.split(" ")[1];
            }
            frmNewBillKA.tbxAmount.text = formatamountwithCurrency(frmNewBillKA.tbxAmount.text, currency === undefined ? "JOD" : currency);
        }
    }
}