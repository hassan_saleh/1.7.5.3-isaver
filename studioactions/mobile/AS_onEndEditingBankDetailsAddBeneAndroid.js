function AS_onEndEditingBankDetailsAddBeneAndroid(eventobject, changedtext) {
    return AS_TextField_a5dde8834df14522b55742df90265889(eventobject, changedtext);
}

function AS_TextField_a5dde8834df14522b55742df90265889(eventobject, changedtext) {
    if (frmAddExternalAccountKA.tbxBeneBankDetails.text !== "") {
        frmAddExternalAccountKA.flxUnderlineBankDetails.skin = "sknFlxGreenLine";
    } else {
        frmAddExternalAccountKA.flxUnderlineBankDetails.skin = "sknFlxOrangeLine";
    }
    animateLabel("DOWN", "lblBeneBankDetails", frmAddExternalAccountKA.tbxBeneBankDetails.text);
}