function PInloginCick(eventobject) {
    return AS_Button_ie729bbbcca24784a8c1aa45dfff67f7(eventobject);
}

function AS_Button_ie729bbbcca24784a8c1aa45dfff67f7(eventobject) {
    var isPinSupported = kony.store.getItem("isPinSupported");
    if (isPinSupported !== null && isPinSupported !== undefined && isPinSupported !== "" && isPinSupported == "Y") {
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
        isAlternateLoginEnabled("P");
    } else {
        customAlertPopup("", kony.i18n.getLocalizedString("i18n.Pin.msg"), popupCommonAlertDimiss, "");
    }
}