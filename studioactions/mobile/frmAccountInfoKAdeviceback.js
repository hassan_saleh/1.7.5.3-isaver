function frmAccountInfoKAdeviceback(eventobject) {
    return AS_Form_063db3893d454aacb3114b25c19027d1(eventobject);
}

function AS_Form_063db3893d454aacb3114b25c19027d1(eventobject) {
    //Nart Rawashdeh 09/12/2020 <<
    if (frmAccountInfoKA.flxSSCInformation.isVisible === true) {
        frmAccountInfoKA.flxTabsWrapper.setVisibility = true;
        frmAccountInfoKA.imgDivSett.setVisibility = true;
        frmAccountInfoKA.flxAccountOptions.setVisibility = true;
        frmAccountInfoKA.lblLoanAccountInfo.text = geti18Value("i18n.accounts.accounts");
        frmAccountInfoKA.flxSSCInformation.setVisibility = false;
        //>>
    } else {
        transactionMove();
    }
}