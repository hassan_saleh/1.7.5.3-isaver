function AS_TextField_ab7969a1ba47470c83b2dc29cd982d32(eventobject, changedtext) {
    try {
        animateLabel("UP", "lblEmail1", frmUserSettingsSIRILIMIT.txtEmail1.text);
        var currency = getDefaultTransferCurr();
        kony.print("currency" + currency);
        if (kony.string.equalsIgnoreCase(currency, "JOD") || kony.string.equalsIgnoreCase(currency, "BHD")) {
            limit = 3;
        } else {
            limit = 2;
        }
        var value = fixDecimal(frmUserSettingsSIRILIMIT.txtEmail1.text, limit);
        if (value.indexOf(".") == -1 && value.length > 5) {
            frmUserSettingsSIRILIMIT.txtEmail1.text = value.substring(0, 5);
        } else {
            frmUserSettingsSIRILIMIT.txtEmail1.text = value;
        }
    } catch (err) {
        kony.print("err in pwd::" + err);
    }
}