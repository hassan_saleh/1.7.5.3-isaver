function AS_onClickServiceTypeAddBiller(eventobject) {
    return AS_FlexContainer_g26c512b8d3a441cbc0b00a95a9ea25b(eventobject);
}

function AS_FlexContainer_g26c512b8d3a441cbc0b00a95a9ea25b(eventobject) {
    if (JSON.stringify(kony.boj.Biller[kony.boj.selectedBillerType].selectedBiller) == '{}') {
        frmAddNewPayeeKA.flxUnderlineServiceType.skin = "sknFlxOrangeLine";
        frmAddNewPayeeKA.flxUnderlineBillerName.skin = "sknFlxOrangeLine";
        if (JSON.stringify(kony.boj.Biller[kony.boj.selectedBillerType].selectedBillerCategory) == '{}') frmAddNewPayeeKA.flxUnderlineBillerCategory.skin = "sknFlxOrangeLine";
    } else {
        if (kony.boj.selectedBillerType === "PostPaid") kony.boj.callService("GetEfwServiceType");
        else kony.boj.callService("GetPPServiceType");
    }
}