function AS_FlexContainer_SwitchOffQuickPreview(eventobject) {
    return AS_FlexContainer_i8f4dea6e41041908d7fbd3569c1fe36(eventobject);
}

function AS_FlexContainer_i8f4dea6e41041908d7fbd3569c1fe36(eventobject) {
    if (frmAuthorizationAlternatives.flxSwitchOff.isVisible === true) {
        frmAuthorizationAlternatives.flxSwitchOn.isVisible = true;
        frmAuthorizationAlternatives.flxSwitchOff.isVisible = false;
        var segLength = 0;
        if (frmAuthorizationAlternatives.segAccounts.isVisible) segLength = frmAuthorizationAlternatives.segAccounts.data.length;
        var segArray = [];
        kony.print("da---:" + JSON.stringify(frmAuthorizationAlternatives.segAccounts.data));
        if (!isEmpty(segLength)) {
            for (var i = 0; i < segLength; i++) {
                var segData = frmAuthorizationAlternatives.segAccounts.data[i];
                segData.lblIncommingTick.isVisible = true;
                segArray.push(segData);
            }
            frmAuthorizationAlternatives.segAccounts.setData(segArray);
        }
        customAlertPopup(geti18Value("i18n.maps.Info"), kony.i18n.getLocalizedString("i18n.showSwipeQuick.desc"), popupCommonAlertDimiss, "");
        //frmAuthorizationAlternatives.flxTransactionPreview.isVisible = true;
    } else {
        frmAuthorizationAlternatives.flxSwitchOff.isVisible = true;
        frmAuthorizationAlternatives.flxSwitchOn.isVisible = false;
        //frmAuthorizationAlternatives.flxTransactionPreview.isVisible = false;
    }
}