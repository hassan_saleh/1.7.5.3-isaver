function AS_onClickAddBiller(eventobject) {
    return AS_Button_j99e1c53446b4acb87918593a3a00055(eventobject);
}

function AS_Button_j99e1c53446b4acb87918593a3a00055(eventobject) {
    // frmAddNewPayeeKA.show();
    navigation_STORE_PREVIOUS_FORM(frmPayBillHome);
    frmAddNewPrePayeeKA.destroy();
    kony.boj.BillerResetFlag = true;
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var listController = INSTANCE.getFormController("frmAddNewPayeeKA");
    var navigationObject = new kony.sdk.mvvm.NavigationObject();
    var datamodelflxAddressKA = new kony.sdk.mvvm.DataModel();
    navigationObject.setDataModel(null, kony.sdk.mvvm.OperationType.ADD, "form");
    navigationObject.setRequestOptions("form", {
        "headers": {}
    });
    listController.performAction("navigateTo", ["frmAddNewPayeeKA", navigationObject]);
}