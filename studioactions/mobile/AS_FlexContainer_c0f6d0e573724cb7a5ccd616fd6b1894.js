function AS_FlexContainer_c0f6d0e573724cb7a5ccd616fd6b1894(eventobject) {
    if (frmAuthorizationAlternatives.flxSwitchOff.isVisible === true) {
        frmAuthorizationAlternatives.flxSwitchOn.isVisible = true;
        frmAuthorizationAlternatives.flxSwitchOff.isVisible = false;
        var segLength = frmAuthorizationAlternatives.segAccounts.data.length;
        var segArray = [];
        kony.print("da---:" + JSON.stringify(frmAuthorizationAlternatives.segAccounts.data));
        for (var i = 0; i < segLength; i++) {
            var segData = frmAuthorizationAlternatives.segAccounts.data[i];
            segData.lblIncommingTick.isVisible = true;
            segArray.push(segData);
        }
        frmAuthorizationAlternatives.segAccounts.setData(segArray);
        customAlertPopup(geti18Value("i18n.maps.Info"), kony.i18n.getLocalizedString("i18n.showSwipeQuick.desc"), popupCommonAlertDimiss, "");
        //frmAuthorizationAlternatives.flxTransactionPreview.isVisible = true;
    } else {
        frmAuthorizationAlternatives.flxSwitchOff.isVisible = true;
        frmAuthorizationAlternatives.flxSwitchOn.isVisible = false;
        //frmAuthorizationAlternatives.flxTransactionPreview.isVisible = false;
    }
}