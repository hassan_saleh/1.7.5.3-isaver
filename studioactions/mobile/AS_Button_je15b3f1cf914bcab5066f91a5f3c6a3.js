function AS_Button_je15b3f1cf914bcab5066f91a5f3c6a3(eventobject) {
    var navObject;
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var formController = INSTANCE.getFormController("frmBillPayeeDetailsKA");
    if (!navObject || !(navObject instanceof kony.sdk.mvvm.NavigationObject)) {
        navObject = new kony.sdk.mvvm.NavigationObject();
    }
    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen(kony.retailBanking.globalData.globals.LOADING_SCREEN_MESSAGE);
    kony.retailBanking.globalData.globals.formStack.push("frmBillPayeeDetailsKA");
    formController.loadDataAndShowForm(navObject);
}