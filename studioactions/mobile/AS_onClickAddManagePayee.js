function AS_onClickAddManagePayee(eventobject) {
    return AS_Button_bef34ac826ce4125bb13e070189a5a42(eventobject);
}

function AS_Button_bef34ac826ce4125bb13e070189a5a42(eventobject) {
    // frmAddNewPayeeKA.show();
    frmAddNewPrePayeeKA.destroy();
    navigation_STORE_PREVIOUS_FORM(frmManagePayeeKA);
    kony.store.setItem("denoFlag", {});
    gblQuickFlow = "post";
    if (kony.boj.selectedBillerType === "PrePaid" && frmManagePayeeKA.btnAll.skin !== "slButtonWhiteTab" && frmManagePayeeKA.btnPostPaid.skin !== "slButtonWhiteTab") {
        frmAddNewPrePayeeKA.show();
        gblQuickFlow = "pre";
    } else {
        kony.boj.BillerResetFlag = true;
        kony.boj.selectedBillerType = "PostPaid";
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var listController = INSTANCE.getFormController("frmAddNewPayeeKA");
        var navigationObject = new kony.sdk.mvvm.NavigationObject();
        var datamodelflxAddressKA = new kony.sdk.mvvm.DataModel();
        navigationObject.setDataModel(null, kony.sdk.mvvm.OperationType.ADD, "form");
        navigationObject.setRequestOptions("form", {
            "headers": {}
        });
        listController.performAction("navigateTo", ["frmAddNewPayeeKA", navigationObject]);
    }
}