function IPS_PaymentHistory_OutgoingOnCLick(eventobject) {
    return AS_Button_aa531eb1a81a4f639fb8acfc78046e57(eventobject);
}

function AS_Button_aa531eb1a81a4f639fb8acfc78046e57(eventobject) {
    frmIPSRequests.btnAll.skin = "slButtonWhiteTabDisabled";
    frmIPSRequests.btnIncoming.skin = "slButtonWhiteTabDisabled";
    frmIPSRequests.btnOutgoing.skin = "slButtonWhiteTab";
    frmIPSRequests.btnRequestToPay.skin = "slButtonWhiteTabDisabled";
    kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
    loadPaymentHistory("IPS_OUTWARD");
    kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
}