function AS_Label_b7f1f608484c4e0bb7c855314003e590(eventobject, x, y) {
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.bills.BillPaymentExit"), onClickYesClose, onClickNoClose, geti18Value("i18n.login.continue"), geti18Value("i18n.cashWithdraw.cancel"));

    function onClickYesClose() {
        popupCommonAlertDimiss();
        frmPaymentDashboard.show();;
    }

    function onClickNoClose() {
        popupCommonAlertDimiss();
    }
}