function AS_FlexContainer_OnClickNextEPSScreen(eventobject) {
    return AS_FlexContainer_dc39a67e81024e4daf25ad9a95043fb3(eventobject);
}

function AS_FlexContainer_dc39a67e81024e4daf25ad9a95043fb3(eventobject) {
    if (frmEPS.lblNext.skin === "sknLblNextEnabled") {
        if (validate_SufficientBalance()) {
            kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen("");
            SetupConfirmationScreen();
            //   GetAliasInfo();
            frmEPS.flxMain.setVisibility(false);
            frmEPS.flxConfirmEPS.setVisibility(true);
            kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
        } else {
            customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.error.InsufficientFundInAccount"), popupCommonAlertDimiss, "");
        }
    } else {
        customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.jomopay.fillDetails"), popupCommonAlertDimiss, "");
    }
}