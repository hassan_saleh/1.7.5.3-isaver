function devicebackdisablingcredit(eventobject) {
    return AS_Form_cff8bea3225f4c3b809878f43085a317(eventobject);
}

function AS_Form_cff8bea3225f4c3b809878f43085a317(eventobject) {
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18nkey("i18n.common.sureExit"), function() {
        frmPaymentDashboard.show();
        popupCommonAlertDimiss();
    }, popupCommonAlertDimiss, geti18nkey("i18n.common.YES"), geti18nkey("i18n.common.NO"));
}