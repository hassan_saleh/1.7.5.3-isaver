function AS_animateRegisterConfrmPasswordLabelDownAndroid(eventobject, changedtext) {
    return AS_TextField_c5ac0110dbba405c94d1862a4e8642fa(eventobject, changedtext);
}

function AS_TextField_c5ac0110dbba405c94d1862a4e8642fa(eventobject, changedtext) {
    if (kony.retailBanking.util.validation.isValidPassword(frmEnrolluserLandingKA.tbxPasswordKA.text) && validateConfirmPassword(frmEnrolluserLandingKA.tbxPasswordKA.text, frmEnrolluserLandingKA.tbxConfrmPwdKA.text)) {
        frmEnrolluserLandingKA.flxUnderlineConfirmPassword.skin = "sknFlxGreenLine";
        confrmPasswordFlag = true;
    } else {
        frmEnrolluserLandingKA.flxUnderlineConfirmPassword.skin = "sknFlxOrangeLine";
        confrmPasswordFlag = false;
    }
    animateLabel("DOWN", "lblConfirmPassword", frmEnrolluserLandingKA.tbxConfrmPwdKA.text);
}