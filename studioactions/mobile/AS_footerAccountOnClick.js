function AS_footerAccountOnClick(eventobject) {
    return AS_FlexContainer_hc819bf76a42416c9d326f89589fb4da(eventobject);
}

function AS_FlexContainer_hc819bf76a42416c9d326f89589fb4da(eventobject) {
    var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
    var controller = INSTANCE.getFormController("frmAccountsLandingKA");
    var navObject = new kony.sdk.mvvm.NavigationObject();
    navObject.setRequestOptions("segAccountsKA", {
        "headers": {
            "session_token": kony.retailBanking.globalData.session_token
        }
    });
    controller.loadDataAndShowForm(navObject);
}