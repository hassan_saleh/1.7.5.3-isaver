function onTextChangeApplyCardsMonthlyIncome(eventobject, changedtext) {
    return AS_TextField_c65b51ea088a474ca03ccd78d16ecd64(eventobject, changedtext);
}

function AS_TextField_c65b51ea088a474ca03ccd78d16ecd64(eventobject, changedtext) {
    if (new RegExp(/[^0-9]/g).test(frmApplyNewCardsKA.txtMonthlyIncome.text)) {
        frmApplyNewCardsKA.flxBorderMonthlyIncome.skin = "sknFlxOrangeLine";
    } else {
        frmApplyNewCardsKA.flxBorderMonthlyIncome.skin = "skntextFieldDividerGreen";
    }
    validate_APPLYCARDSDETAILS();
}