function AS_FlexContainer_ifc8d24bbf9347bd8cc8ccd3408498e0(eventobject) {
    //Omar Alnajjar 
    frmNewSubAccountLandingNew.lblFre.text = geti18Value("i18n.common.accountType");
    frmNewSubAccountLandingNew.lblCurr.text = geti18Value("i18n.common.currencyType");
    frmNewSubAccountLandingNew.lblAccDesc.text = "";
    frmNewSubAccountLandingNew.flxAccDesc.setVisibility(false);
    frmNewSubAccountLandingNew.flxISaverSub.setVisibility(false);
    frmSettingsKA.show();
}