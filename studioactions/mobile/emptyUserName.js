function emptyUserName(eventobject, x, y) {
    return AS_Label_c7b957b0c31843eb84b1a1d56f7b282a(eventobject, x, y);
}

function AS_Label_c7b957b0c31843eb84b1a1d56f7b282a(eventobject, x, y) {
    switchToFAQDetailsTab();
    frmInformationKA.flxFAQ.setVisibility(true);
    frmInformationKA.flxFAQSearch.setVisibility(false);
    frmInformationKA.searchField.text = "";
}