function AS_Button_e6b72a617f0344f0aba1a20e0f986e40(eventobject) {
    frmEPS.flxDetailsAliasScroll.setVisibility(false);
    frmEPS.flxDetailsIBAN.setVisibility(false);
    frmEPS.flxDetailsMobileScroll.setVisibility(true);
    if (frmEPS.btnMob.skin === slButtonBlueFocus) {
        frmEPS.btnMob.skin = sknOrangeBGRNDBOJ;
        frmEPS.btnAlias.skin = slButtonBlueFocus;
        frmEPS.btnIBAN.skin = slButtonBlueFocus;
    }
    //reset the other flows 
    // Reset Iban Details //  
    frmEPS.txtAmountIBAN.text = "";
    frmEPS.txtIBANAlias.text = "";
    frmEPS.txtAddressAlias.text = "";
    frmEPS.txtBankAlias.text = "";
    frmEPS.tbxBankName.text = "";
    frmEPS.lblBankNameStat.setVisibility(false);
    frmEPS.tbxBankName.setVisibility(false);
    frmEPS.lblBankName.setVisibility(true);
    // Reset Iban Details // 
    // Reset Alias Details //
    frmEPS.txtAliasName.text = "";
    frmEPS.txtAmountAlias.text = "";
    frmEPS.lblIBANAliastxt.text = "";
    frmEPS.lbAliasAddresstxt.text = "";
    frmEPS.lblAliasBanktxt.txt = "";
    frmEPS.flxAmountAlias.setVisibility(false);
    frmEPS.flxIBANAliass.setVisibility(false);
    frmEPS.flxAliasAddress.setVisibility(false);
    frmEPS.flxAliasBank.setVisibility(false);
    // Reset Alias Details //
    // animateLabel("UP", "lblIBANMob",frmEPS.lblIBANMob.text);
    // animateLabel("UP", "lblAddressMob",frmEPS.lblAddressMob.text);
    // animateLabel("UP", "lblBankMob",frmEPS.lblBankMob.text);
    // GetAliasInfo();
}