function AS_Label_ca15ab7ee5ec46e4933ff822910be070(eventobject, x, y) {
    customAlertPopup(geti18Value("i18n.cards.Confirmation"), geti18Value("i18n.Transfer.cancelTra"), onClickYesClose, onClickNoClose, geti18Value("i18n.login.continue"), geti18Value("i18n.common.cancel"));

    function onClickYesClose() {
        popupCommonAlertDimiss();
        removeDatafrmTransfersform();
        makeInlineErrorsTransfersInvisible();
        frmPaymentDashboard.show();;
    }

    function onClickNoClose() {
        popupCommonAlertDimiss();
    }
}