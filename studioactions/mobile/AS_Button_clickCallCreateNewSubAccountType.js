function AS_Button_clickCallCreateNewSubAccountType(eventobject) {
    return AS_Button_hc140283e13d49269ba4347b31663091(eventobject);
}

function AS_Button_hc140283e13d49269ba4347b31663091(eventobject) {
    if (frmNewSubAccountConfirm.lblCheck1.text === "p") {
        callConfirmCreateNewSubAccount();
    } else {
        customAlertPopup(geti18Value("i18n.maps.Info"), geti18Value("i18n.common.pleaseSelectCheckBox"), popupCommonAlertDimiss, "");
    }
}