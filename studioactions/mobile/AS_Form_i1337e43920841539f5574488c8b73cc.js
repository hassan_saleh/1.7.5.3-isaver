function AS_Form_i1337e43920841539f5574488c8b73cc(eventobject) {
    var langFlag = kony.store.getItem("langFlagSettingObj");
    if (langFlag) {
        var langSelected = kony.store.getItem("langPrefObj");
        //alert("langSelected::"+langSelected);
        if (langSelected === "en") {
            onClickEnglish();
        } else {
            onClickArabic();
        }
        frmLoginKA.show();
    } else {
        frmLanguageChange.show();
    }
}