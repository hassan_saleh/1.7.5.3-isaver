function onclickbackfromrecentransios(eventobject) {
    return AS_Button_3eb39c555faf423f8cf411d4f63de98b(eventobject);
}

function AS_Button_3eb39c555faf423f8cf411d4f63de98b(eventobject) {
    var previousForm = kony.application.getPreviousForm().id;
    if (previousForm == "frmAccountDetailKA") {
        kony.sdk.mvvm.KonyApplicationContext.showLoadingScreen();
        var INSTANCE = kony.sdk.mvvm.KonyApplicationContext.getAppInstance();
        var controller = INSTANCE.getFormController("frmAccountDetailKA");
        controller.getFormModel().showView();
        kony.sdk.mvvm.KonyApplicationContext.dismissLoadingScreen();
    } else {
        backToTransferPayLandingPage("frmRecentTransactionDetailsKA");
    }
}