function ontouchEndPassClearLogin(eventobject, x, y) {
    return AS_Label_ida4b61186bc4cc1b3007b80924f7d27(eventobject, x, y);
}

function AS_Label_ida4b61186bc4cc1b3007b80924f7d27(eventobject, x, y) {
    var currentForm = kony.application.getCurrentForm();
    if (currentForm.id === "frmLoginKA" || currentForm.id === "frmLanguageChange") {
        currentForm.passwordTextField.text = "";
        currentForm.flxbdrpwd.skin = "skntextFieldDivider";
        currentForm.lblInvalidCredentialsKA.isVisible = false;
        currentForm.lblShowPass.setVisibility(false);
        animateLabel("DOWN", "lblPassword", currentForm.tbxusernameTextField.text);
    }
}