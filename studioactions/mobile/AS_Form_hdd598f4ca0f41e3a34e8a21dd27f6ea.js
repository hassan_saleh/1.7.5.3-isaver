function AS_Form_hdd598f4ca0f41e3a34e8a21dd27f6ea(eventobject) {
    //Mai 13/12/2020
    if (gblTModule === "CLIQAddAlias") {
        flxAliasType.visible = false;
        lblIPSRegistrationTitle.text = "i18n.CLIQ.addAlias";
        onSelect_REGISTRATION_TYPES("ALIAS", frmIPSRegistration.txtAlias);
        frmIPSRegistration.LblCountryCodeHint.setVisibility(false);
    } else {
        flxAliasType.visible = true;
        lblIPSRegistrationTitle.text = "i18n.IPS.registerforIPS";
        onSelect_REGISTRATION_TYPES("MOBILE", frmIPSRegistration.txtAlias);
        frmIPSRegistration.LblCountryCodeHint.setVisibility(true);
    }
}