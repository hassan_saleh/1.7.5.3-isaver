function initializetmpMonths() {
    flxMonthsTemplate = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "10%",
        "id": "flxMonthsTemplate",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "slFbox"
    }, {}, {});
    flxMonthsTemplate.setDefaultUnit(kony.flex.DP);
    var lblCheck = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblCheck",
        "isVisible": false,
        "left": "5%",
        "skin": "sknBOJttfwhitee150",
        "text": "q",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblMonth = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblMonth",
        "isVisible": true,
        "left": "5%",
        "skin": "sknTransferType",
        "text": "Label",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    flxMonthsTemplate.add(lblCheck, lblMonth);
}