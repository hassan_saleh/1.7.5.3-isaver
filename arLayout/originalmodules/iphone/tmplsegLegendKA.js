function initializetmplsegLegendKA() {
    CopyFlexContainer07124cb64fc4f45 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "id": "CopyFlexContainer07124cb64fc4f45",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "sknCopyslFbox07d05709853a74d"
    }, {}, {});
    CopyFlexContainer07124cb64fc4f45.setDefaultUnit(kony.flex.DP);
    var lblBoxKA = new kony.ui.Label({
        "centerY": "50%",
        "height": "20dp",
        "id": "lblBoxKA",
        "isVisible": true,
        "left": "8dp",
        "skin": "slLabel",
        "width": "20dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var CategoryName = new kony.ui.Label({
        "centerY": "50%",
        "height": "20dp",
        "id": "CategoryName",
        "isVisible": true,
        "left": "14.030000000000001%",
        "skin": "skn383838LatoRegular107KA",
        "top": "30dp",
        "width": "65%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    var lblSepKA = new kony.ui.Label({
        "bottom": "0dp",
        "height": "1dp",
        "id": "lblSepKA",
        "isVisible": true,
        "right": "0dp",
        "skin": "sknLineEDEDEDKA",
        "text": "Label",
        "width": "95%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    CopyFlexContainer07124cb64fc4f45.add(lblBoxKA, CategoryName, lblSepKA);
}